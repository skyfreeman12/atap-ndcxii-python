from __future__ import unicode_literals
import numpy as np
import pandas as pd
import time
import sys
import os
import zmq

import matplotlib
matplotlib.use('Qt5Agg')    #   Make sure that we are using QT5
import matplotlib.pyplot as plt
from PyQt5 import QtCore, QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from sm import all_checks
import NDCXII
from NDCXII import data
import scopeavg

NDCXII.plt.figure = NDCXII.oldfigure    #   remove command line information
matplotlib.rcParams.update({'figure.max_open_warning': 0})  #   stops terminal from showing 'too many figues' warning

progname = os.path.basename(sys.argv[0])
progversion = "0.1"

# listen to announcements of shots
c = zmq.Context()
s = c.socket(zmq.SUB)
s.connect('tcp://128.3.58.16:6001')
s.setsockopt_string(zmq.SUBSCRIBE, '')

poller = zmq.Poller()
poller.register(s, zmq.POLLIN)

def check_scope(scope):
    '''flags unusual shots'''
    '''calculates the differences between expected values and actual values'''
    #   these values are displayed on the large graph
    max_value = np.max(scope.x)
    arrival = get_loc(scope)
    average_shot = aw.average_shot # data frame of a plot of an average_shot and sum of distances squared information

    max_difference = max_value - average_shot[scope.name]['max']    #   difference between average max and actual max
    loc_difference = arrival - average_shot[scope.name]['arrival time'] #   difference between average arrival time and actual arrival time

    error_string = ''   #   the string at the top of the big graph
    bgcolor = 'w'   #   the background color of the small graphs

    #   scaling the axis
    if scope.name.startswith('SRK'):    #   SRK scope scale differently
        tscale = 1e6
        vscale = 1e3
        tunit = '$\mu$s'
        vunit = 'V'
    else:
        tscale = 1e9
        vscale = 1e-3
        tunit = 'ns'
        vunit = 'kV'

    if max_difference > 0:
        error_string += ('max is ' +
        str(round(abs(max_difference) * vscale, 1)) +
        ' ' + vunit + ' high, ')
    else:
        error_string += ('max is ' +
        str(round(abs(max_difference) * vscale, 1)) +
        ' ' + vunit + ' low, ')

    if loc_difference > 0:
        error_string += ('arrival time is ' + str(round(np.abs(loc_difference)*tscale, 1)) + ' ' + tunit + ' late')
    elif loc_difference < 0:
        error_string += ('arrival time is ' + str(round(np.abs(loc_difference)*tscale, 1)) + ' ' + tunit + ' early')

    '''how well the the scope fits the dotted line determines if it is flaged'''
    mini_scope = MiniScope(average_shot[scope.name]['x'], average_shot[scope.name]['t'])
    if sds(mini_scope, scope) > average_shot[scope.name]['sds'] * average_shot[scope.name]['tolerence']:
        bgcolor = 'yellow'
    if sds(mini_scope, scope) > average_shot[scope.name]['sds'] * 2 * average_shot[scope.name]['tolerence']:
        bgcolor = 'red'

    return bgcolor, error_string

def get_loc(scope):
    '''Finds the location halfway up the rising slope of the voltage pulse'''
    '''does a linear interpolation'''
    max_peak = np.max(scope.x)
    hm = max_peak/2
    max_arg = np.argmax(scope.x)
    left1 = max_arg
    while scope.x[left1] > hm:
        left1 -= 1
    left2 = left1 + 1
    left_m = (scope.x[left2] - scope.x[left1]) / (scope.t[left2] - scope.t[left1])
    if left_m != 0.0:
        left_t = (hm - scope.x[left1]) / left_m + scope.t[left1]
    else:
        left_t = scope.t[left1]
    try:
        return left_t
    except:
        print('Could not obtain fwhm/peak')
        return None

def waveform_analysis(scope):
    ''' Returns FWHM and peak of the waveform'''
    '''does a linear interpolation beacuse the scope resolution is 2 nanoseconds'''
    max_peak = np.max(scope.x)
    hm = max_peak/2
    max_arg = np.argmax(scope.x)
    left1 = max_arg
    right2 = max_arg
    while scope.x[left1] > hm:
        left1 -= 1
    while scope.x[right2] > hm:
        right2 += 1
    left2 = left1 + 1
    right1 = right2 - 1
    left_m = (scope.x[left2] - scope.x[left1]) / (scope.t[left2] - scope.t[left1])
    right_m = (scope.x[right2] - scope.x[right1]) / (scope.t[right2] - scope.t[right1])
    if left_m != 0.0:
        left_t = (hm - scope.x[left1]) / left_m + scope.t[left1]
    else:
        left_t = scope.t[left1]
    if right_m != 0.0:
        right_t = (hm - scope.x[right1]) / right_m + scope.t[right1]
    else:
        right_t = scope.t[right1]
    fwhm = right_t - left_t
    try:
        return fwhm, max_peak
    except:
        print('Could not obtain fwhm/peak')
        return None

def sds(scope1, scope2):
    '''finds the sum of the distances quared of two waveforms on a regin of interest'''
    '''the regin of interest is the largest peak, above zero'''
    if scope2.name.startswith('SRK'):
        max_loc = np.argmax(scope1.x) # define regin of interest around maximum of scope1
        left = max_loc
        while scope1.t[left] > .9 * np.max(scope1.x):
            left -= 1
        right = max_loc
        while scope1.x[right] > .9 * np.max(scope1.x):
            right += 1
        roi1 = scope1.x[ left : right ]
        roi2 = scope2.x[ left: right ]
        distance = sum([(roi1[i] - roi2[i]) ** 2 for i in range(len(roi1))]) # take the sum of distances squared
        return distance
    else:
        max_loc = np.argmax(scope1.x) # define regin of interest around maximum of scope1
        left = max_loc
        while scope1.t[left] > .05 * np.max(scope1.x):
            left -= 1
        right = max_loc
        while scope1.x[right] > .05 * np.max(scope1.x):
            right += 1
        roi1 = scope1.x[ left : right ]
        roi2 = scope2.x[ left: right ]
        distance = sum([(roi1[i] - roi2[i]) ** 2 for i in range(len(roi1))]) # take the sum of distances squared
        return distance

def make_average_shot(shots):
    '''creates an average shot data frame'''
    cell_list = ['BL'+str(i) for i in range(1,6)] + ['Injector'] + ['Cell'+str(i) for i in range (1,8)] + ['SRK0'+str(i) for i in range(1, 10)] + ['SRK'+str(i) for i in range(10, 29)]
    value_list = ['sds', 'stdev', 'arrival time', 'max', 'tolerence', 'x', 't']
    avg = pd.DataFrame(index = value_list, columns = cell_list)
    for cell in cell_list:

        avg_scope = AvgScope(shots, cell)
        avg[cell]['sds'] = avg_scope.avg_sds    #   the average sum of distances squared over a comment
        avg[cell]['stdev'] = avg_scope.sds_stdev    #   the standard deviation of sums of distances squared over a comment
        avg[cell]['arrival time'] = scopeavg.comment_arrival_time(shots, cell)[0]   #   the average half max arrival time over a comment
        avg[cell]['max'] = scopeavg.comment_max(shots, cell)[0] #   the aveage max over a comment
        avg[cell]['x'] = avg_scope.x    #   the aveage x component of a waveform over a comment
        avg[cell]['t'] = avg_scope.t    #   the t component of a waveform over a comment

        #   how many times the average sds the shot sds can be before the shot is flagged
        #   these automatic tolerences can be changed in the drop-down menu
        if cell.startswith('BL'):
            avg[cell]['tolerence'] = 6
        elif cell.startswith('Injector'):
            avg[cell]['tolerence'] = 5
        elif cell.startswith('Cell'):
            avg[cell]['tolerence'] = 15
        elif cell.startswith('SRK'):
            avg[cell]['tolerence'] = 10
    return avg  #   average shot data frame

class MiniScope(object):
    '''a simple object that can be treated like a scope with just x and t values'''
    def __init__(self, x, t):
        self.x = x
        self.t = t

class AvgScope(object):
    '''creates an average scope waveform of many scope waveforms, removeing outlier'''
    def __init__(self, shots, scopename):

        def good_list(shots, scopename):
            '''removes the outlier when defineing an average shot'''
            wave1 = MiniScope(avg_wave(shots, scopename)[0], avg_wave(shots, scopename)[1])
            sds1, sigma1 = sds_tuple(shots, wave1, scopename)
            good_list = [shot for shot in shots if sds_tuple([shot], wave1, scopename)[0] < sds1 * 3]   #   cut off at 3 standard deviations
            return good_list

        def avg_wave(shots, scopename):
            '''creates an average scope waveform'''
            for shot in shots:
                '''make array of right length'''
                try:
                    shotnumber = shot.shotnumber
                    scope = NDCXII.data.Scope(scopename, shotnumber)
                    scope_avg = [0] * len(scope.t)
                    scope_time = scope.t
                    break
                except:
                    print('could not find valid scope')
            '''fill array with sum of points'''
            count = 0   #   count of valid shots
            for shot in shots:
                try:
                    shotnumber = shot.shotnumber
                    scope = NDCXII.data.Scope(scopename, shotnumber)
                    scope_avg = [scope_avg[i] + scope.x[i] for i in range(len(scope.t))]
                    count += 1
                except:
                    pass
            try:
                scope_avg = [scope_avg[i]/count for i in range(len(scope_avg))]
                return scope_avg, scope_time
            except:
                return [], []

        def sds_tuple(shots, average_scope, scopename):
            '''finds the average and standard deviation of a comment's distance from a single wave'''
            sds_list = []
            count = 0
            for shot in shots:
                try:
                    shotnumber = shot.shotnumber
                    scope = NDCXII.data.Scope(scopename, shotnumber)
                    sds_list.append(sds(average_scope, scope))
                    count += 1
                except:
                    pass
            if count != 0:
                return sum(sds_list)/count, np.std(sds_list)
            else:
                return 0, 0

        self.scopename = scopename
        good_list = good_list(shots, scopename)
        self.x, self.t = avg_wave(good_list, scopename)
        self.avg_sds, self.sds_stdev = sds_tuple(good_list, self, scopename) # average sum of distances squared and standard deviation of sum of distances squared

class SingleFigure(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None):
        self.fig, self.axes = plt.subplots(1, 1)

        self.compute_initial_figure()

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass

class SingleMonitor(SingleFigure):
    """A canvas that updates itself every second with a new plot."""

    def __init__(self, connect=None, *args, **kwargs):
        self.main = kwargs.pop('main')
        super().__init__(*args, **kwargs)
        self.connect = connect
        self.setToolTip(connect)

    def compute_initial_figure(self):
        plt.sca(self.axes)
        plt.cla()
        plt.tick_params(
            axis='both',
            which='both',      # both major and minor ticks are affected
            bottom='off',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            left='off',
            right='off',
            labelleft='off',
            labelbottom='off')  # labels along the bottom edge are off
        self.axes.plot([], [])

    def update_figure(self, shotdata):

        self.axes.cla() #   clears the small plot

        try:
            Scope = getattr(shotdata, self.connect)
            self.Scope = Scope
        except:
            print("couldn't access scope", sys.exc_info()[0])
            return

        try:
            Scope.get_data()
        except:
            print("couldn't get data for ", self.connect)
            return

        try:
            bgcolor, self.error_string = check_scope(self.Scope)
        except:
            bgcolor = 'silver'
            self.error_string = 'could not check scope'

        if Scope.name.startswith('Cell1'):  #   ignoor the noise on cell 1, which is not active
            bgcolor = 'silver'
            self.error_string = 'this cell is not on'

        self.x = Scope.x
        self.t = Scope.t * 1e6  #   scale the time

        try:    #   try to load the average red dotted line unless a comparison comment is not defined
            self.avg_x = aw.average_shot[Scope.name]['x']
            self.avg_t = aw.average_shot[Scope.name]['t'] * 1e6
        except:
            pass

        self.mint = round(len(self.t) * .1) # start plotting one tenth of the way down the signal
        self.maxt = round(len(self.t) * .4) # stop plotting four tenths down the signal

            #   an SRK plots its whole signal on its small plot, other scopes only plot the spiked part
        if Scope.name.startswith('SRK'):
            try:
                self.axes.plot(self.avg_t, self.avg_x, 'r--') # plot whole signal
                self.draw()
            except:
                pass
            self.axes.plot(self.t, self.x, color = 'blue') # plot whole signal
            self.axes.set_facecolor(bgcolor)
            self.draw()
        else:
            try:
                self.axes.plot(self.avg_t[self.mint : self.maxt], self.avg_x[self.mint : self.maxt], 'r--')     #   plot only the spike of the signal
                self.draw()
            except:
                pass
            self.axes.plot(self.t[self.mint : self.maxt], self.x[self.mint : self.maxt], color = 'blue')    #   plot only the spike of the signal
            self.axes.set_facecolor(bgcolor)
            self.draw()

        self.axes.relim()
        self.axes.autoscale_view(True, True, True)

        if self.Scope.name.startswith('Injector'): #plots new injector to big plot first
            self.main.bigplot.axes.cla()
            try:    #   try to plot the dotted line, except if it does not exist yet
                self.main.bigplot.axes.plot(self.avg_t[self.mint : self.maxt], [i * 1e-3 for i in self.avg_x[self.mint : self.maxt]], 'r--')#turn V into kV
                self.main.bigplot.draw()
            except:
                pass
            self.main.bigplot.axes.plot(self.t[self.mint : self.maxt], [i * 1e-3 for i in self.x[self.mint : self.maxt]], color = 'blue')#turn V into kV
            self.main.bigplot.axes.set_title("shot {} for {}\ncomparison comment {}".format(self.Scope.shotnumber, self.Scope.name, aw.comment))
            self.main.bigplot.axes.set_xlabel('time [$\mu$s]')
            self.main.bigplot.axes.set_ylabel('voltage [kV]')
            self.main.bigplot.axes.text(0.02, 0.98, self.error_string, fontsize=12, transform=self.main.bigplot.axes.transAxes, verticalalignment='top')
            self.main.bigplot.axes.set_ylim([(np.min(self.x) - (np.max(self.x) - np.min(self.x)) * .1) * 1e-3, (np.max(self.x) + (np.max(self.x) - np.min(self.x)) * .25) * 1e-3]) # allow room for error string
            self.main.bigplot.draw()

    def mousePressEvent(self, event):
        if self.main:
            # Fail nicely if you try to click on a plot without a shot loaded
            try:
                if self.Scope.name.startswith('SRK'):

                    self.main.bigplot.axes.cla()
                    try:    #   try to plot the dotted line, except if it does not exist yet
                        self.main.bigplot.axes.plot([i * 1e-3 for i in self.avg_t], [i * 1e3 for i in self.avg_x], 'r--') #us to ms, kV to V
                        self.main.bigplot.draw()
                    except:
                        pass
                    self.main.bigplot.axes.plot(self.t * 1e-3, self.x * 1e3, color = 'blue') #us to ms, kV to V
                    self.main.bigplot.axes.set_xlabel('time [ms]')
                    self.main.bigplot.axes.set_ylabel('voltage [V]')
                    self.main.bigplot.axes.set_ylim([(np.min(self.x) - (np.max(self.x) - np.min(self.x)) * .1) * 1e3, (np.max(self.x) + (np.max(self.x) - np.min(self.x)) * .25) * 1e3]) # allow room for error string at top
                else:

                    self.main.bigplot.axes.cla()
                    try:    #   try to plot the dotted line, except if it does not exist yet
                        self.main.bigplot.axes.plot(self.avg_t[self.mint : self.maxt], [i * 1e-3 for i in self.avg_x[self.mint : self.maxt]], 'r--') #turn V into kV
                        self.main.bigplot.draw()
                    except:
                        pass
                    self.main.bigplot.axes.plot(self.t[self.mint : self.maxt], [i * 1e-3 for i in self.x[self.mint : self.maxt]], color = 'blue') #turn V into kV
                    self.main.bigplot.axes.set_xlabel('time [$\mu$s]')
                    self.main.bigplot.axes.set_ylabel('voltage [kV]')
                    self.main.bigplot.axes.set_ylim([(np.min(self.x) - (np.max(self.x) - np.min(self.x)) * .1) * 1e-3, (np.max(self.x) + (np.max(self.x) - np.min(self.x)) * .25) * 1e-3]) # allow room for error string at top
                self.main.bigplot.axes.set_title("shot {} for {}\ncomparison comment {}".format(self.Scope.shotnumber, self.Scope.name, aw.comment))
                self.main.bigplot.axes.text(0.02, 0.98, self.error_string, fontsize=12, transform=self.main.bigplot.axes.transAxes,
                    verticalalignment='top')
                self.main.bigplot.draw()
            except AttributeError:
                pass

class ApplicationWindow(QtWidgets.QMainWindow):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        self.average_shot = None
        self.average_shot_is_good = False
        self.old_comment = 'not selected'
        self.comment = 'not selected'
        self.oldshot = None
        self.newshot = None
        self.standards_file = 'no standards file selected'
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("application main window")

        '''file menue'''
        self.file_menu = QtWidgets.QMenu('&File', self)
        self.file_menu.addAction(
            '& Load shot', self.loadshot)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.file_menu.addAction(   #   pick the comment to make an average shot out of
            '& Generate dotted line', self.new_dotted)
        self.menuBar().addMenu(self.file_menu)

        self.file_menu.addAction(   #   pick the comment to make an average shot out of
            '& Change tolerences', self.change_tolerences)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtWidgets.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtWidgets.QWidget(self)

        l = QtWidgets.QVBoxLayout()
        self.dc = []

        self.bigplot = SingleFigure() #whichever figure is clicked on goes to the big plot
        l.addWidget(self.bigplot, stretch=5)

        box = QtWidgets.QHBoxLayout() #adds BL scopes in fist row
        for i in ['BL1', 'BL2', 'BL3', 'BL4', 'BL5']:
            A = SingleMonitor(parent=self.main_widget, connect=i, main=self)
            self.dc.append(A)
            box.addWidget(A)
        l.addLayout(box, stretch=1)

        box = QtWidgets.QHBoxLayout() #adds Injector and Cell scopes in second row
        for i in ['Injector', 'Cell1', 'Cell2', 'Cell3', 'Cell4',
                  'Cell5', 'Cell6', 'Cell7']:
            A = SingleMonitor(parent=self.main_widget, connect=i, main=self)
            self.dc.append(A)
            box.addWidget(A)
        l.addLayout(box, stretch=1)

        box = QtWidgets.QHBoxLayout()#adds SRK scopes in third row
        for i in range(28):
            A = SingleMonitor(parent=self.main_widget,
                              connect="SRK{:02d}".format(i + 1), main=self)
            self.dc.append(A)
            box.addWidget(A)
        l.addLayout(box, stretch=1)

        self.main_widget.setLayout(l)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.fwhm={}
        self.peaks={}

        # try to update figures every second
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figures)
        timer.start(1000)

    def update_figures(self):
        socks = dict(poller.poll(0))
        if s in socks and socks[s] == zmq.POLLIN:
            self.newshot = int(s.recv_string())
            # need to wait three seconds for data to be written
            # this should be more robust in the future
            time.sleep(3)
        if self.newshot != self.oldshot or self.comment != self.old_comment:
            if self.newshot:
                print('updating figures from shot: {} to shot: {}'.format(
                    self.oldshot, self.newshot))
                start_time = time.time()
                try:
                    all_checks(self.newshot)    #   does the checks on the data writeing from sm.py
                except:
                    pass
                self.shotdata = data.Shotdata(self.newshot)
                for i in self.dc:
                    i.update_figure(self.shotdata)
                self.oldshot = self.newshot
                print('shot check time for shot ' + str(self.newshot) + ': ' + str(time.time() - start_time))
            self.old_comment = self.comment
        sys.stdout.flush()

    def loadshot(self): #enter a shot number manualy
        text, ok = QtWidgets.QInputDialog.getText(
            self, 'Load Shot', 'Enter a shot number:')
        if text.isdigit() and ok:
            self.newshot = int(text)

    def new_dotted(self): #enter a comment number
        text, ok = QtWidgets.QInputDialog.getText(self, 'Comment to define dotted line', 'Enter a comment number: (YYYYMM, DDTTTT)')
        if ',' in text and ok:
            YYYYMM = text.split(',')[0]
            DDTTTT = text.split(',')[1]
            start_time = time.time()
            try:
                shots = NDCXII.data.ShotsFromComment(YYYYMM, DDTTTT)
                self.average_shot = make_average_shot(shots)
                self.comment = text
                self.average_shot_is_good = True
            except:
                print('could not create new file')
            print('dottted line generation time for comment ' + str(YYYYMM) + ' ' + str(DDTTTT) + '(' + str(len(shots)) + ' shots)' ': ' + str(time.time() - start_time))

    def change_tolerences(self):
        '''lets the user change tolerences is the average shot data frame through the drop-down menu'''
        if self.average_shot_is_good:
            cell_list = ['BL'+str(i) for i in range(1,6)] + ['Injector'] + ['Cell'+str(i) for i in range (1,8)] + ['SRK0'+str(i) for i in range(1, 10)] + ['SRK'+str(i) for i in range(10, 29)]
            update = False
            text, ok = QtWidgets.QInputDialog.getText(self, 'Injector tolerence ', 'enter injector tolerence ' + '(currently ' + str(self.average_shot['Injector']['tolerence']) + 'stdev) ')
            if text.isdigit() and ok:
                self.average_shot['Injector']['tolerence'] = float(text)
                update = True
            text, ok = QtWidgets.QInputDialog.getText(self, 'Cell tolerence ', 'enter cell tolerence ' + '(currently ' + str(self.average_shot['Cell1']['tolerence']) + 'stdev) ')
            if text.isdigit() and ok:
                for cell in cell_list:
                    if cell.startswith('Cell'):
                        self.average_shot[cell]['tolerence'] = float(text)
                        update = True
            text, ok = QtWidgets.QInputDialog.getText(self, 'Blumlein tolerence ', 'enter Blumlein tolerence ' + '(currently ' + str(self.average_shot['BL1']['tolerence']) + 'stdev) ')
            if text.isdigit() and ok:
                for cell in cell_list:
                    if cell.startswith('BL'):
                        self.average_shot[cell]['tolerence'] = float(text)
                        update = True
            text, ok = QtWidgets.QInputDialog.getText(self, 'Solenoid tolerence ', 'enter solenoid tolerence ' + '(currently ' + str(self.average_shot['SRK01']['tolerence']) + 'stdev) ')
            if text.isdigit() and ok:
                for cell in cell_list:
                    if cell.startswith('SRK'):
                        self.average_shot[cell]['tolerence'] = float(text)
                        update = True
            # updates the figures
            if self.newshot and update:
                print('updating figures from shot: {} to shot: {}'.format(
                    self.oldshot, self.newshot))
                self.shotdata = data.Shotdata(self.newshot)
                for i in self.dc:
                    i.update_figure(self.shotdata)
        else:
            print('no dotted line yet')

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtWidgets.QMessageBox.about(self, "About",
                                    "Shotmonitor GUI for NDCXII\n\n"+
                                    "python-version: {}\n".format(".".join([str(i) for i in sys.version_info]))+
                                    "NDCXII-version: {}".format(NDCXII.__version__))


if __name__ == '__main__':
    qApp = QtWidgets.QApplication(sys.argv)

    aw = ApplicationWindow()
    aw.setWindowTitle('NDCX-II Shot Monitor')
    iconpath = os.path.join(os.curdir, 'Icon', 'NDCXIILogo.jpeg')

    aw.setWindowIcon(QtGui.QIcon(iconpath))
    aw.show()
    sys.exit(qApp.exec_())
