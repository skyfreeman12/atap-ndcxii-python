import NDCXII
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from report import waveform_analysis
from report import get_loc
import datetime

'''this module contains functions that find averages values of all waveforms for a scope during a series of shots'''
'''works well to use NDCXII.data.ShotsFromComment(yyyymm, ddtttt) to load many shots'''

def comment_plot(shots, scopename):
    '''plots all the scope waveforms'''
    for shot in shots:
        try:
            shotnumber = shot.shotnumber
            scope = NDCXII.data.Scope(scopename, shotnumber)
            x = scope.x
            t = scope.t
            plt.plot(t, x)
        except:
            print("could not plot")
    plt.show()

def comment_fwhm(shots, scopename):
    '''finds the average fwhm and standard deviation'''
    fwhm_list = []
    for shot in shots:
        try:
            shotnumber = shot.shotnumber
            scope = NDCXII.data.Scope(scopename, shotnumber)
            fwhm_list.append(waveform_analysis(scope)[0])
        except:
            print("could not get fwhm for shot " + str(shot.shotnumber))
    mean = np.mean(fwhm_list)
    stdev = np.std(fwhm_list)
    fwhm_list = [ i for i in fwhm_list if i > (mean - 4 * stdev) and i < (mean + 4 * stdev) ] #removes outliers
    return np.mean(fwhm_list), np.std(fwhm_list)

def comment_max(shots, scopename):
    '''finds the average global max and standard deviation'''
    max_list = []
    for shot in shots:
        try:
            shotnumber = shot.shotnumber
            scope = NDCXII.data.Scope(scopename, shotnumber)
            max_list.append(np.max(scope.x))
        except:
            print("could not get max for shot " + str(shot.shotnumber))
    mean = np.mean(max_list)
    stdev = np.std(max_list)
    max_list = [ i for i in max_list if i > (mean - 4 * stdev) and i < (mean + 4 * stdev) ] #removes outliers
    return np.mean(max_list), np.std(max_list)

def comment_min(shots, scopename):
    '''finds the average global min and standard deviation'''
    min_list = []
    for shot in shots:
        try:
            shotnumber = shot.shotnumber
            scope = NDCXII.data.Scope(scopename, shotnumber)
            min_list.append(np.min(scope.x))
        except:
            print("could not get min for shot " + str(shot.shotnumber))
    mean = np.mean(min_list)
    stdev = np.std(min_list)
    min_list = [ i for i in min_list if i > (mean - 4 * stdev) and i < (mean + 4 * stdev) ] #removes outliers
    return np.mean(min_list), np.std(min_list)


def comment_arrival_time(shots, scopename):
    '''find the half max arrival time of a waveform and its standrad deviaton'''
    atime_list = []
    for shot in shots:
        try:
            shotnumber = shot.shotnumber
            scope = NDCXII.data.Scope(scopename, shotnumber)
            atime_list.append(get_loc(scope))
        except:
            print("could not get arrival time for shot " + str(shot.shotnumber))
    mean = np.mean(atime_list)
    stdev = np.std(atime_list)
    atime_list = [ i for i in atime_list if i > (mean - 4 * stdev) and i < (mean + 4 * stdev) ] #removes outliers
    return np.mean(atime_list), np.std(atime_list)

def average_shot_file(shots):
    '''creates an average shot data frame and saves it to desktop as a excel doc'''
    print('starting')
    scope_list = ['BL'+str(i) for i in range(1,6)] + ['Injector'] + ['Cell'+str(i) for i in range (1,8)] + ['SRK0'+str(i) for i in range(1, 10)] + ['SRK'+str(i) for i in range(10, 29)]
    value_list = ['fwhm', 'fwhm stdev', 'fwhm tolerence', 'max', 'max stdev', 'max tolerence', 'min', 'min stdev', 'min tolerence', 'arrival time', 'arrival time stdev', 'arrival time tolerence']
    avg = pd.DataFrame(index = value_list, columns = scope_list)
    for scope in scope_list:

        print('doing fwhm')
        avg[scope]['fwhm'], avg[scope]['fwhm stdev'] = str(comment_fwhm(shots, scope)[0]), str(comment_fwhm(shots, scope)[1])
        print('doing max')
        avg[scope]['max'], avg[scope]['max stdev'] = str(comment_max(shots, scope)[0]), str(comment_max(shots, scope)[1])
        print('doing min')
        avg[scope]['min'], avg[scope]['min stdev']= str(comment_min(shots, scope)[0]), str(comment_min(shots, scope)[1])
        print('doing arrival time')
        avg[scope]['arrival time'], avg[scope]['arrival time stdev'] = str(comment_arrival_time(shots, scope)[0]), str(comment_arrival_time(shots, scope)[1])

        if scope.startswith('BL'):
            avg[scope]['fwhm tolerence'] = 3
            avg[scope]['min tolerence'] = 4
            avg[scope]['max tolerence'] = 4
            avg[scope]['arrival time tolerence'] = 3
        elif scope.startswith('Injector'):
            avg[scope]['fwhm tolerence'] = 3
            avg[scope]['min tolerence'] = 4
            avg[scope]['max tolerence'] = 3
            avg[scope]['arrival time tolerence'] = 3
        elif scope.startswith('Cell'):
            avg[scope]['fwhm tolerence'] = 3
            avg[scope]['min tolerence'] = 4
            avg[scope]['max tolerence'] = 4
            avg[scope]['arrival time tolerence'] = 3
        elif scope.startswith('SRK'):
            avg[scope]['fwhm tolerence'] = 4
            avg[scope]['min tolerence'] = 4
            avg[scope]['max tolerence'] = 4
            avg[scope]['arrival time tolerence'] = 4

    print('saving')
    homedir = os.environ['HOME']
    now = datetime.datetime.now()
    time_string = str(now.year) + str(now.month) + '-' + str(now.day) + str(now.hour) + str(now.second)

    writer = pd.ExcelWriter(homedir + '/Desktop/shot_standards' + time_string + '.xlsx', engine='xlsxwriter')
    avg.to_excel(writer, sheet_name = 'Sheet1')
    writer.save()
