# Kieran Murphy, Steve Lidia
# Created on Feb 6, 2013
#
# 2013-10-11 updated to python3,
#     imported into git. See git history for changes. Arun Persaud

"""pepperpot

Usage: pepperpot [options] [-h|--help] [--] <images>...

Options:
  -h --help               Show this screen
  -p --pdf                Output all plots into a pdf
  -o FILE --output=FILE   Name of the pdf file [default: plots.pdf]
  --roi (<lx>,<ly>,<ux>,<uy>)
                          Upper/Lower x/y coordinates of
                          the ROI [default: 0,511,0,511]
  --hideROI               Don't plot the ROI [default: false]
  --noflip                do not flip images horizontally
  -v --verbose            Print out some more information

Use the keyword `bkgrd` to start the list of background images. The
`images` can be listed in short from, that is only the first one needs
to have the full path and file name. Consecitive ones can only have
the numbers that differ, e.g. \\path\\to\\file\\12345678.spe 00 would
load two files \\path\\to\\file\\12345678.spe and
\\path\\to\\file\\12345600.spe.

Example:
  pepperpot ../NDCX-II-data/131016/images/1310160029.SPE 30-33 bkgrd 39-43

"""
import os.path
import readSPE
from docopt import docopt
from functools import partial
import NDCXII
import NDCXII.helper

import numpy as np
from scipy import ndimage
from scipy.ndimage.filters import maximum_filter

from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

N = 2.00  # num std dev's above mean for pixel to be considered

hole_size = 0.010*25.4  # hole diameter [mm]
pixel_per_mm = 4.52  # pixels/mm
mm_per_pixel = 1./pixel_per_mm  # number of millimeters in a pixel
hole_spacing = 3.81  # hole center to hole center spacing [mm]
# pixels between holes in the pepperpot
gridSpacing = hole_spacing * pixel_per_mm
pix_per_grid = int(gridSpacing)**2
print(pix_per_grid, 'pixels per grid')

sizeMinimum = max(5, pix_per_grid/100)  # minimum number of pixels per blob
sizeMaximum = pix_per_grid

# distance in mm from pPot to scintillator
drift_distance = 17.5
# degrees to rotate image due to mask misalignment (CW direction) [rad]
img_rotation = 1.0*np.pi/180.

# measurement variances
sig_x_meas = np.sqrt(hole_spacing**2/12. + mm_per_pixel**2/12.)  # [mm] 1.1 mm
sig_xp_meas = np.arctan(mm_per_pixel /
                        drift_distance)/np.sqrt(12.)*1e3  # [mr] 3.5 mr

# Display toggles
show_img = 1
show_phase_space = 1

# scaling factor for bkgd images = xx/yy, for yy duration bkgd with xx
# duration signal images
bkgdImgCal = 1

# Sobel filter parameter
sobel_fac = 0.01


# =========================================================================
def emit2D_variance(phase_space2, meas_err):
    """Calculates variance on the emittance (sigma_emit**2)

    Parameters
    ----------
    phase_space2 : is a numpy array of values [rho, x, x_prime]
                   where rho is the normalized density, and (x,xprime)
                   are either (x,x') or (y,y')
    meas_err : is the list of measure errors to be used

    Result
    ------
    output is the variances on the calculated emittance value

    """
    sig_x_meas, sig_xp_meas, bkgd_avg_sigma, sigma_mass = meas_err

    rho, x, xp = phase_space2[:, [0, 1, 2]].transpose()

    norm = rho.sum()

    x = x - (rho*x).sum()/norm
    xp = xp - (rho*xp).sum()/norm

    x2 = (rho*x*x).sum()/norm
    xp2 = (rho*xp*xp).sum()/norm
    xxp = (rho*x*xp).sum()/norm

    emitsqr = x2*xp2 - xxp**2
    emit = np.sqrt(emitsqr)

    # intensity variance
    var_rho = bkgd_avg_sigma**2 + sigma_mass**2

    # intensity error ~var_rho
    var_emit2_rho = (var_rho*((x*x*xp2 + xp*xp*x2 -
                               2*x*xp*xxp - 2*emitsqr)/norm)**2).sum()

    # position error ~sig_x_meas^2
    var_x = sig_x_meas**2
    var_emit2_x = (var_x*((4*rho/norm)*(x*xp2-xp*xxp))**2).sum()

    # divergence error ~sig_xp_meas^2
    var_xp = sig_xp_meas**2
    var_emit2_xp = (var_xp*((4*rho/norm)*(xp*x2-x*xxp))**2).sum()

    var_emit2 = var_emit2_rho + var_emit2_x + var_emit2_xp

    var_emit = 0.
    var_emit_rho = 0.
    var_emit_x = 0.
    var_emit_xp = 0.
    if emitsqr != 0:
        var_emit = var_emit2/(4*emitsqr)
        var_emit_rho = var_emit2_rho/(4*emitsqr)
        var_emit_x = var_emit2_x/(4*emitsqr)
        var_emit_xp = var_emit2_xp/(4*emitsqr)

    return emit, [var_emit, var_emit_rho, var_emit_x, var_emit_xp]


def sigma4(phase_space4, meas_err):
    rho, x, xp, y, yp = phase_space4[:, [0, 2, 3, 4, 5]].T

    norm = rho.sum()
    norm2 = norm**2

    x = x - (rho*x).sum()/norm
    xp = xp - (rho*xp).sum()/norm
    y = y - (rho*y).sum()/norm
    yp = yp - (rho*yp).sum()/norm

    sig_x_meas, sig_xp_meas, bkgd_avg_sigma, sigma_mass = meas_err

    var_rho = bkgd_avg_sigma**2 + sigma_mass**2
    var_rho_sum = var_rho.sum()

    var_x = sig_x_meas**2
    var_xp = sig_xp_meas**2
    var_y = var_x
    var_yp = var_xp

    xx = (rho*x*x).sum()/norm
    xxp = (rho*x*xp).sum()/norm
    xpxp = (rho*xp*xp).sum()/norm
    xy = (rho*x*y).sum()/norm
    xpy = (rho*xp*y).sum()/norm
    xyp = (rho*x*yp).sum()/norm
    xpyp = (rho*xp*yp).sum()/norm
    yy = (rho*y*y).sum()/norm
    yyp = (rho*y*yp).sum()/norm
    ypyp = (rho*yp*yp).sum()/norm

    covar = [xx, xxp, xpxp, xy, xpy, xyp, xpyp, yy, yyp, ypyp]

    var_xx = ((x**2*x**2*var_rho + 2*rho**2*x**2*var_x).sum()/norm2 +
              xx**2*var_rho_sum/norm2)
    var_xxp = (x**2*xp**2*var_rho + rho**2*x**2*var_xp +
               rho**2*xp**2*var_x).sum()/norm2 + xxp**2*var_rho_sum/norm2
    var_xpxp = ((xp**2*xp**2*var_rho + 2*rho**2*xp**2*var_xp).sum()/norm2 +
                xpxp**2*var_rho_sum/norm2)
    var_xy = (x**2*y**2*var_rho + rho**2*x**2*var_y +
              rho**2*y**2*var_x).sum()/norm2 + xy**2*var_rho_sum/norm2
    var_xpy = (xp**2*y**2*var_rho + rho**2*xp**2*var_y +
               rho**2*y**2*var_xp).sum()/norm2 + xpy**2*var_rho_sum/norm2
    var_xyp = (x**2*yp**2*var_rho + rho**2*x**2*var_yp +
               rho**2*yp**2*var_x).sum()/norm2 + xyp**2*var_rho_sum/norm2
    var_xpyp = (xp**2*yp**2*var_rho + rho**2*xp**2*var_yp +
                rho**2*yp**2*var_xp).sum()/norm2 + xpyp**2*var_rho_sum/norm2
    var_yy = ((y**2*y**2*var_rho + 2*rho**2*y**2*var_y).sum()/norm2 +
              yy**2*var_rho_sum/norm2)
    var_yyp = (y**2*yp**2*var_rho + rho**2*y**2*var_yp +
               rho**2*yp**2*var_y).sum()/norm2 + yyp**2*var_rho_sum/norm2
    var_ypyp = ((yp**2*yp**2*var_rho + 2*rho**2*yp**2*var_yp).sum()/norm2 +
                ypyp**2*var_rho_sum/norm2)

    vsigma4 = [var_xx, var_xxp, var_xpxp, var_xy, var_xpy,
               var_xyp, var_xpyp, var_yy, var_yyp, var_ypyp]

    return covar, vsigma4


def emit4D(phase_space4, meas_err):
    rho, x, xp, y, yp = phase_space4[:, [0, 2, 3, 4, 5]].T
    norm = rho.sum()

    x = x - (rho*x).sum()/norm
    xp = xp - (rho*xp).sum()/norm
    y = y - (rho*y).sum()/norm
    yp = yp - (rho*yp).sum()/norm

    sig_x_meas, sig_xp_meas, bkgd_avg_sigma, sigma_mass = meas_err

    var_rho = bkgd_avg_sigma**2 + sigma_mass**2

    covar, sigma4_var = sigma4(phase_space4, meas_err)

    xx, xxp, xpxp, xy, xpy, xyp, xpyp, yy, yyp, ypyp = covar
    vxx, vxxp, vxpxp, vxy, vxpy, vxyp, vxpyp, vyy, vyyp, vypyp = sigma4_var

    # products
    nu1 = xy**2*xpxp - 2*xy*xxp*xpy + xx*xpy**2
    nu2 = xy*xpxp*xyp - xy*xxp*xpyp + xpy*xx*xpyp - xpy*xxp*xyp
    nu4 = xyp**2*xpxp - 2*xyp*xxp*xpyp + xpyp**2*xx

    # variances
    vnu1 = (4*(xy*xpxp - xxp*xpy)**2*vxy + xy**4*vxpxp + xpy**4*vxx +
            4*xy**2*xpy**2*vxxp + 4*(xpy*xx - xy*xxp)**2*vxpy)

    vnu2 = ((xpxp*xyp - xxp*xpyp)**2*vxy + xy**2*xyp**2*vxpxp +
            xpy**2*xpyp**2*vxx + (xy*xpxp - xpy*xxp)**2*vxyp +
            (xy*xpyp + xpy*xyp)**2*vxxp + (xx*xpy - xxp*xy)**2*vxpyp +
            (xx*xpyp - xxp*xyp)**2*vxpy)

    vnu4 = (4*(xyp*xpxp - xxp*xpyp)**2*vxyp +
            4*(xpyp*xx - xyp*xxp)**2*vxpyp +
            xyp**4*vxpxp + 4*xyp**2*xpyp**2*vxxp + xpyp**4*vxx)

    # sigma_x sector
    detx = xx*xpxp - xxp**2
    vdetx = xpxp**2*vxx + xx**2*vxpxp + 4*xxp**2*vxxp

    #print 'detx, vdetx, emitx, sig_emitx =',\
    #       detx, vdetx, detx**0.5, np.sqrt(vdetx/(4*detx))
    #print

    c1 = detx*yy - nu1
    c2 = detx*ypyp - nu4
    c3 = detx*yyp - nu2

    vc1 = yy**2*vdetx + detx**2*vyy + vnu1
    vc2 = ypyp**2*vdetx + detx**2*vypyp + vnu4
    vc3 = yyp**2*vdetx + detx**2*vyyp + vnu2

    #print c1,c2,c3
    #print vc1, vc2, vc3
    #print

    a = c1*c2 - c3**2
    va = c2**2*vc1 + c1**2*vc2 + 4*c3**2*vc3

    det4 = a/detx
    vdet4 = va/detx**2 + a**2/detx**4*vdetx

    emit2 = det4**0.25
    vemit2 = (0.25/det4**0.75)**2*vdet4

    return det4, vdet4, emit2, np.sqrt(vemit2)


def display_img(img, title=""):
    plt.figure()
    plt.title(title)
    plt.imshow(img)
    ShowOrSave()
    return


def load_averaged_image(filelist, median_radius=1, ROI=None):
    """Calculate average and variance of multiple images

    Apply a median_filter optionally.

    Clip to region of interest (ROI) if specified.

    """
    if ROI is not None:
        row_lo, row_hi, col_lo, col_hi = ROI
        img = np.zeros(shape=(row_hi-row_lo, col_hi-col_lo), dtype=np.float)
        var_img = np.zeros_like(img, dtype=np.float)
    else:
        img = np.zeros_like(readSPE.load(filelist[0]), dtype=np.float)
        var_img = np.zeros_like(img, dtype=np.float)

    for fl in filelist:
        # convert to float, so we don't get overflow during **2
        tmpimage = readSPE.load(fl).astype(np.float)
        if ROI is not None:
            tmpimage = tmpimage[row_lo:row_hi, col_lo:col_hi]
        if median_radius > 1:
            img += ndimage.median_filter(tmpimage, median_radius)
            var_img += ndimage.median_filter(tmpimage, median_radius)**2
        else:
            img += tmpimage
            var_img += tmpimage**2

    img /= len(filelist)
    var_img = var_img/len(filelist) - img**2

    return img, var_img


# =========================================================================
# =========================================================================
#
#                       Main routine begins here
#
# =========================================================================
# =========================================================================
def crunch(filelist, bkgdfilelist):
    """
    This program takes an image of the ion beam after passing through
    the pepperpot.  It finds the 'blobs' which are the mini beamlets
    that passed through each hole in the Ppot by 1) finding all pixel
    values N standard deviations above the image mean, 2) joining
    contiguous groups of high pixels together, and 3) only keeping those
    blobs bigger than or equal to sizeMinimum pixels.

    After this, various processing is done:

    1) Center of Mass for each blob is found and stored in the
    blobCenters array

    2) Gaussian fitting - each blob is fit to a 2D gaussian with the
    mean at the center of mass and the covariance matrix is found from
    the distribution.  The eigenvalues and eigenvectors of the
    covariance matrix are found and used to plot the semimajor and
    semiminor axes of the ellipses that are the isocontours of the
    best-fit gaussians.  This could be used later in analysis of each
    beamlet distribution after traversing the distance from the Ppot to
    the scintillator.

    3) Grid fitting - a grid of modeled after the Ppot with a width of
    gridSpacing pixels is laid down and the closest grid point to each
    blob is found.  The grid is then shifted by the average displacement
    from grid point to blob, ostensibly finding the best fit grid

    4) NOT DONE - Phase space projections - the distributions of
    beamlets wrt grid holes are projected together for each x and y
    value and plotted to show a rough sketch of the x-x' and y-y' phase
    spaces

    5) NOT DONE - the blocked beamlet distributions in the upper left
    diagonal of the Ppot are interpolated by averaging

    """
    assert len(filelist) > 0, "Need at least one image to work on"
    assert len(bkgdfilelist) > 0, "Need at least one background image"

    ### load images
    img, var_img = load_averaged_image(filelist)
    bkgdImg, var_bkgd = load_averaged_image(bkgdfilelist)

    # apply calibration factor if exposure time of bkgd image is
    # different from data images
    bkgdImg *= bkgdImgCal
    var_bkgd *= bkgdImgCal**2

    if flip_horizontal:
        img = np.fliplr(img)
        var_img = np.fliplr(var_img)
        bkgdImg = np.fliplr(bkgdImg)
        var_bkgd = np.fliplr(var_bkgd)

    if show_img:
        display_img(img, "Averaged Images")

    print('Image peak, max std. dev. =', img.max(), np.sqrt(var_img.max()))
    print('Bkgd mean and max std. dev. =',
          np.mean(bkgdImg), np.sqrt(var_bkgd.max()))

    ### plot ROI
    ROI_msk = np.zeros_like(img, dtype=np.bool)
    ROI_msk[ROI_rlo:ROI_rhi, ROI_clo:ROI_chi] = True

    if not commands['--hideROI']:
        display_img(ROI_msk, "ROI mask")
        display_img(img*ROI_msk, "Applied ROI mask")

    ### apply ROI to image
    if commands['--verbose']:
        print("applying ROI")
    img = img[ROI_rlo:ROI_rhi, ROI_clo:ROI_chi]
    var_img = var_img[ROI_rlo:ROI_rhi, ROI_clo:ROI_chi]
    bkgdImg = bkgdImg[ROI_rlo:ROI_rhi, ROI_clo:ROI_chi]
    var_bkgd = var_bkgd[ROI_rlo:ROI_rhi, ROI_clo:ROI_chi]

    bkgd_avg_sigma = np.mean(np.sqrt(var_bkgd))
    print('Bkgd mean and avg. sigma =', np.mean(bkgdImg), bkgd_avg_sigma)

    img_sigma_to_mean = np.sqrt(var_img) / img

    if 1 < 0:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.scatter(img, img_sigma_to_mean, s=img/500.)
        ax.set_title('Image sigma to mean')
        ax.set_xlabel('Mean value')
        ax.set_ylabel('Sigma:mean value')
        ShowOrSave()

    print('Image mean and avg. sigma/mean =',
          np.mean(img), np.mean(img_sigma_to_mean))
    print()

    ### subtract bkgd img
    mean = np.mean(bkgdImg)
    std = np.std(bkgdImg)

    img = (img - bkgdImg) * ((img - bkgdImg) > N*std)

    print('### Bkgd subtracted image min, max, bkgd mean and std =',
          img.min(), img.max(), mean, std)
    print()

    ### calculate average pixel value along ROI edge
    edge_mean_left = np.mean(img[:, 0])
    edge_mean_right = np.mean(img[:, -1])
    edge_mean_top = np.mean(img[0, :])
    edge_mean_bottom = np.mean(img[-1, :])

    print('### Image edge mean values =', edge_mean_left, edge_mean_right,
          edge_mean_top, edge_mean_bottom)
    print()

    # =========================================================================
    # filtering and peak detection

    ### Sobel filter and peak detection
    sx = ndimage.sobel(img, 0, mode='constant')
    sy = ndimage.sobel(img, 1, mode='constant')
    smag = np.hypot(sx, sy)
    smag *= 65535.0/np.max(smag)
    img_sobel = smag

    peak_filter = maximum_filter(img_sobel, size=(int(gridSpacing)/2,
                                                  int(gridSpacing)/2))
    img_peak = img * (peak_filter > 0)

    if commands['--verbose']:
        display_img(img, "Background subtracted data")
        display_img(img_sobel, "Sobel Filter")
        display_img(peak_filter, "Peak Filter")

    ### peak labeling, aggregation, and separation
    peak_label, num_peaks = ndimage.measurements.label(peak_filter)

    # construct new image built around solo peak aggregation
    # array to aggregate individual peaks
    img_keep = np.zeros_like(img)
    num_keep = 0

    peak_data = []

    for ii in range(num_peaks):
        # get individual peaks from labelled peak inventory
        solo_peak = img_peak * (peak_label == ii+1)
        num_pixels = (peak_label == ii+1).sum()

        yind, xind = np.ndarray.nonzero(solo_peak)  # y:row, x:col
        xyind = list(zip(xind, yind))

        # not too few pixels nor too many
        if sizeMinimum <= num_pixels <= pix_per_grid:
            num_keep += 1
            img_keep += solo_peak  # layer solo peaks into new composite image

            # 2nd order moments
            ymean, xmean = ndimage.measurements.center_of_mass(solo_peak)
            x2mean = np.array([(i-xmean)**2*solo_peak[i, j]
                               for (i, j) in xyind]).sum()/solo_peak.sum()
            y2mean = np.array([(j-ymean)**2*solo_peak[i, j]
                               for (i, j) in xyind]).sum()/solo_peak.sum()
            xymean = np.array([(i-xmean)*(j-ymean)*solo_peak[i, j]
                               for (i, j) in xyind]).sum()/solo_peak.sum()

            peak_moments = [xmean, ymean, xymean, x2mean, y2mean]

            solo_peak_max = ndimage.measurements.maximum(solo_peak)
            solo_mass = ndimage.measurements.sum(solo_peak)
            solo_var = (var_img*solo_peak*(peak_label == ii+1)).sum()/solo_mass
            solo_sigma = np.sqrt(solo_var)

            print('{:3.0f} {:4.0f} {:5.2f} {:7.2f} {:8.2f} {:6.2f}'.
                  format(num_keep, num_pixels,
                         num_pixels/(0.5**2*pix_per_grid),
                         solo_peak_max, solo_mass, solo_sigma))

            peak_data.append((solo_peak, solo_mass, solo_sigma, peak_moments))
        elif num_pixels < sizeMinimum:
            print(ii+1, len(xind), 'pixels. Not accepted.')
        elif num_pixels > pix_per_grid:
            # iterate to break up clumps
            print('De-clumpifying . . .')

            old_clump = solo_peak

            clump_max = old_clump.max()
            clump_min = old_clump.min()
            clump_diff = clump_max-clump_min

            floor_increment = clump_diff/100

            floor = clump_min+floor_increment

            """
            Declumping proceeds by slowly raising the background level
            ('floor') until peaks are individually resolved, at which
            point they are removed from the clump mass. This strives
            to protect the outlying, low intensity peaks by extracting
            them first with the smallest degree of background erosion.

            """
            while floor < clump_max:
                #print 'clump max, floor =',clump_max, floor

                new_clump = old_clump*(old_clump > floor)

                new_peak_filter = maximum_filter(new_clump,
                                                 size=(int(gridSpacing)/2,
                                                       int(gridSpacing)/2))
                new_peak = new_clump * (new_peak_filter > 0)
                new_peak_label, new_peak_num \
                    = ndimage.measurements.label(new_peak_filter)

                for jj in range(new_peak_num):
                    new_solo_peak = new_peak*(new_peak_label == jj+1)
                    new_num_pixels = (new_peak_label == jj+1).sum()

                    # y:row, x:col
                    yind, xind = np.ndarray.nonzero(new_solo_peak)
                    xyind = list(zip(xind, yind))

                    if sizeMinimum <= new_num_pixels <= pix_per_grid:
                        num_keep += 1
                        # layer solo peaks into new composite image
                        img_keep += new_solo_peak

                        # 2nd order moments
                        ymean, xmean = ndimage.measurements.center_of_mass(new_solo_peak)
                        x2mean = np.array([(i-xmean)**2*new_solo_peak[i, j]
                                           for (i, j) in xyind]).sum()/new_solo_peak.sum()
                        y2mean = np.array([(j-ymean)**2*new_solo_peak[i, j]
                                           for (i, j) in xyind]).sum()/new_solo_peak.sum()
                        xymean = np.array([(i-xmean)*(j-ymean)*new_solo_peak[i, j]
                                           for (i, j) in xyind]).sum()/new_solo_peak.sum()

                        peak_moments = [xmean, ymean, xymean, x2mean, y2mean]

                        solo_peak_max = ndimage.measurements.maximum(new_solo_peak)
                        solo_mass = ndimage.measurements.sum(new_solo_peak)
                        solo_var = (var_img*new_solo_peak*(new_peak_label == jj+1)).sum()/solo_mass
                        solo_sigma = np.sqrt(solo_var)

                        print('{:3.0f} {:4.0f} {:5.2f} {:7.2f} {:8.2f} {:6.2f}'.format(
                            num_keep, new_num_pixels, new_num_pixels/(0.5**2*pix_per_grid),
                            solo_peak_max, solo_mass, solo_sigma))

                        peak_data.append((new_solo_peak, solo_mass, solo_sigma, peak_moments))

                        old_clump = old_clump - new_solo_peak

                floor = floor+floor_increment

    total_mass = img_keep.sum()
    img = img_keep

    # =========================================================================
    # sort through peaks
    """Constructs the phase space around the accumulated peaks."""

    peak_masses = [peak_data[i][1] for i in range(num_keep)]
    peak_images = [peak_data[i][0] for i in range(num_keep)]
    peak_sigmas = [peak_data[i][2] for i in range(num_keep)]
    peak_moments = [peak_data[i][3] for i in range(num_keep)]

    peak_sigma_dict = dict(list(zip(peak_masses, peak_sigmas)))
    peak_moment_dict = dict(list(zip(peak_masses, peak_moments)))

    max_peak_mass = np.array(peak_masses).max()
    x0_max = peak_moment_dict[max_peak_mass][0]
    y0_max = peak_moment_dict[max_peak_mass][1]

    phase_space4d = []

    # xprime limit, mrad
    xprime_max = min(200., np.arctan(hole_spacing/2./drift_distance)*1e3)
    # yprime limit, mrad
    yprime_max = min(200., np.arctan(hole_spacing/2./drift_distance)*1e3)

    print('Maximum x-prime, y-prime:', xprime_max, yprime_max, 'mrad')

    for pm in list(peak_moment_dict.keys()):
        sigma_pm = peak_sigma_dict[pm]

        x0 = peak_moment_dict[pm][0]
        y0 = peak_moment_dict[pm][1]

        xy = peak_moment_dict[pm][2]

        x2 = peak_moment_dict[pm][3]
        y2 = peak_moment_dict[pm][4]

        sigx = np.sqrt(x2)/gridSpacing
        sigy = np.sqrt(y2)/gridSpacing

        yoffset = (y0-y0_max)/gridSpacing
        ygrid = np.sign(yoffset)*int(abs(yoffset)+0.5)
        ydiff = yoffset - ygrid
        xoffset = (x0-x0_max)/gridSpacing
        xgrid = np.sign(xoffset)*int(abs(xoffset)+0.5)
        xdiff = xoffset - xgrid

        xblob = xgrid*hole_spacing  # [mm]
        xpblob = np.arctan(xdiff*hole_spacing/drift_distance)*1e3  # [mrad]
        sigxpblob = np.arctan(sigx*hole_spacing/drift_distance)*1e3

        # negate vertical offsets and drifts to account for image indexing
        yblob = -ygrid*hole_spacing
        ypblob = np.arctan(-ydiff*hole_spacing/drift_distance)*1e3
        sigypblob = np.arctan(sigy*hole_spacing/drift_distance)*1e3

        if (abs(xpblob) <= xprime_max) * (abs(ypblob) <= yprime_max):
            # distribute blobs about center location
            phase_space4d.append([pm/4., sigma_pm/4., xblob, xpblob-sigxpblob, yblob, ypblob])
            phase_space4d.append([pm/4., sigma_pm/4., xblob, xpblob+sigxpblob, yblob, ypblob])
            phase_space4d.append([pm/4., sigma_pm/4., xblob, xpblob, yblob, ypblob-sigypblob])
            phase_space4d.append([pm/4., sigma_pm/4., xblob, xpblob, yblob, ypblob+sigypblob])

    phase_space4d = np.array(phase_space4d)
    # =========================================================================
    # calculate geometric emittances

    print(' #### Calculating emittances ####')

    PM, X, Xp, Y, Yp = phase_space4d[:, [0, 2, 3, 4, 5]].T

    total_mass = PM.sum()
    xavg = (PM * X).sum()/total_mass
    xpavg = (PM * Xp).sum()/total_mass
    yavg = (PM * Y).sum()/total_mass
    ypavg = (PM * Yp).sum()/total_mass

    # beam moments
    xx = (PM * (X-xavg)**2).sum()/total_mass
    xxp = (PM * (X-xavg) * (Xp-xpavg)).sum()/total_mass
    xy = (PM * (X-xavg) * (Y-yavg)).sum()/total_mass
    xyp = (PM * (X-xavg) * (Yp-ypavg)).sum()/total_mass
    xpxp = (PM * (Xp-xpavg)**2).sum()/total_mass
    xpy = (PM * (Xp-xpavg) * (Y-yavg)).sum()/total_mass
    xpyp = (PM * (Xp-xpavg) * (Yp-ypavg)).sum()/total_mass
    yy = (PM * (Y-yavg)**2).sum()/total_mass
    yyp = (PM * (Y-yavg) * (Yp-ypavg)).sum()/total_mass
    ypyp = (PM * (Yp-ypavg)**2).sum()/total_mass

    # emittances
    emitx = np.sqrt(xx*xpxp-xxp**2)
    emity = np.sqrt(yy*ypyp-yyp**2)

    # =========================================================================
    # use Courant-Snyder invariant as a tool for culling phase space population
    alphax = -xxp/emitx
    betax = xx/emitx
    gammax = xpxp/emitx
    print('Courant-Snyder X: {:5.2f} {:5.2f} {:5.2f} {:5.2f} {:5.2f}'.
          format(alphax, betax, gammax, emitx, betax*gammax-alphax**2))

    alphay = -yyp/emity
    betay = yy/emity
    gammay = ypyp/emity
    print('Courant-Snyder Y: {:5.2f} {:5.2f} {:5.2f} {:5.2f} {:5.2f}'.
          format(alphay, betay, gammay, emity, betay*gammay-alphay**2))

    total_mass_uncut = total_mass
    phase_space4d_uncut = np.copy(phase_space4d)
    emitx_uncut = emitx
    emity_uncut = emity

    total_mass_old = 0
    emitx_old = 0
    emity_old = 0

    mass_diff = 100.
    emitx_diff = 100.
    emity_diff = 100.

    CScut = 2
    diffcount = 0

    print('CScut, Mass_total, Mass_ratio, emit_x, emit_y :')

    while ((mass_diff > 0.1) or (emitx_diff > 0.1) or
           (emity_diff > 0.1) or (diffcount < 2)):
        CScut = CScut + 1

        psx, psxp, psy, psyp = phase_space4d_uncut[:, [2, 3, 4, 5]].T
        xinvariant = gammax*psx**2 + 2.*alphax*psx*psxp + betax*psxp**2
        yinvariant = gammay*psy**2 + 2.*alphay*psy*psyp + betay*psyp**2

        msk = ((xinvariant <= CScut*emitx_uncut) *
               (yinvariant <= CScut*emity_uncut))

        # mask array
        phase_space4d = phase_space4d_uncut[msk, :]

        # recalculate
        PM, sigma_mass, X, Xp, Y, Yp = phase_space4d[:, [0, 1, 2, 3, 4, 5]].T

        total_mass = PM.sum()
        xavg = (PM * X).sum()/total_mass
        xpavg = (PM * Xp).sum()/total_mass
        yavg = (PM * Y).sum()/total_mass
        ypavg = (PM * Yp).sum()/total_mass

        # beam moments
        xx = (PM * (X-xavg)**2).sum()/total_mass
        xxp = (PM * (X-xavg) * (Xp-xpavg)).sum()/total_mass
        xy = (PM * (X-xavg) * (Y-yavg)).sum()/total_mass
        xyp = (PM * (X-xavg) * (Yp-ypavg)).sum()/total_mass
        xpxp = (PM * (Xp-xpavg)**2).sum()/total_mass
        xpy = (PM * (Xp-xpavg) * (Y-yavg)).sum()/total_mass
        xpyp = (PM * (Xp-xpavg) * (Yp-ypavg)).sum()/total_mass
        yy = (PM * (Y-yavg)**2).sum()/total_mass
        yyp = (PM * (Y-yavg) * (Yp-ypavg)).sum()/total_mass
        ypyp = (PM * (Yp-ypavg)**2).sum()/total_mass

        # emittances
        emitx = np.sqrt(xx*xpxp-xxp**2)
        emity = np.sqrt(yy*ypyp-yyp**2)

        if total_mass_old == 0:
            mass_ratio = total_mass/total_mass_uncut*100.
            print('{:3.0f} {:7.0f} {:5.1f}% {:12.1f} {:12.1f}'.
                  format(CScut, total_mass, mass_ratio, emitx, emity))
        else:
            mass_ratio = total_mass/total_mass_uncut*100.
            mass_diff = (total_mass-total_mass_old)/total_mass_old*100.
            emitx_diff = (emitx-emitx_old)/emitx_old*100.
            emity_diff = (emity-emity_old)/emity_old*100.
            print('{:3.0f} {:7.0f} {:5.1f}% {:5.1f}% {:5.1f} {:5.1f}% {:5.1f} {:5.1f}%'.
                  format(CScut, total_mass, mass_ratio, mass_diff, emitx,
                         emitx_diff, emity, emity_diff))

        if (mass_diff > 0.1) or (emitx_diff > 0.1) or (emity_diff > 0.1):
            diffcount = 0
        else:
            diffcount += 1

        total_mass_old = total_mass
        emitx_old = emitx
        emity_old = emity

    # =========================================================================
    if show_phase_space:
        ps_m, ps_sig, ps_x, ps_xp, ps_y, ps_yp = phase_space4d[:, [0, 1, 2, 3, 4, 5]].T

        ps_r = np.sqrt(ps_x**2 + ps_y**2)  # radius
        ps_j = ps_x*ps_yp - ps_y*ps_xp

        ### Plot x-y
        fig = plt.figure()
        ax = fig.add_subplot(111)
        # plots the dots' areas as proportional to the blob mass
        ax.scatter(ps_x, ps_y, s=ps_m/100)
        ax.set_title('x-y phase space projection')
        ax.set_xlabel('x (mm)')
        ax.set_ylabel('y (mm)')
        ShowOrSave()

        ### Plot x-x'
        fig = plt.figure()
        ax = fig.add_subplot(111)
        # plots the dots' areas as proportional to the blob mass
        ax.scatter(ps_x, ps_xp, s=ps_m/100)
        ax.set_title('x-x\' phase space projection')
        ax.set_xlabel('x (mm)')
        ax.set_ylabel('x\' (mrad)')
        ShowOrSave()

        ### Plot y-y'
        fig = plt.figure()
        ax = fig.add_subplot(111)
        # plots the dots' areas as proportional to the blob mass
        ax.scatter(ps_y, ps_yp, s=ps_m/100)
        ax.set_title('y-y\' phase space projection')
        ax.set_xlabel('y (mm)')
        ax.set_ylabel('y\' (mrad)')
        ShowOrSave()

        ### Plot j vs. r
        fig = plt.figure()
        ax = fig.add_subplot(111)
        # plots the dots' areas as proportional to the blob mass
        ax.scatter(ps_r, ps_j, s=ps_m/100)
        ax.set_title('r-j phase space projection')
        ax.set_xlabel('radius (mm)')
        ax.set_ylabel('j (mm-mrad)')
        ShowOrSave()

        ### Plot x'-y'
        fig = plt.figure()
        ax = fig.add_subplot(111)
        # plots the dots' areas as proportional to the blob mass
        ax.scatter(ps_xp, ps_yp, s=ps_m/100)
        ax.set_title('x\'-y\' phase space projection')
        ax.set_xlabel('x\' (mrad)')
        ax.set_ylabel('y\' (mrad)')
        ShowOrSave()

    # =========================================================================
    # package phase spaces

    xphase_space = phase_space4d[:, [0, 2, 3]] - np.array([0, xavg, xpavg])
    yphase_space = phase_space4d[:, [0, 4, 5]] - np.array([0, yavg, ypavg])

    # =========================================================================
    # display 4D beam sigma matrix

    print('Sigma matrix')
    print()
    matrixformat = "|" + " {:7.2f} "*4 + " |"
    print(matrixformat.format(xx, xxp, xy, xyp))
    print(matrixformat.format(xxp, xpxp, xpy, xpyp))
    print(matrixformat.format(xy, xpy, yy, yyp))
    print(matrixformat.format(xyp, xpyp, yyp, ypyp))
    print()

    # ========================================================================
    # measurement errors to be utilized in variance calculations

    meas_err = [sig_x_meas, sig_xp_meas, bkgd_avg_sigma, sigma_mass]

    emitx, emitx_var = emit2D_variance(xphase_space, meas_err)
    emitx_sig = np.sqrt(np.abs(emitx_var))
    print('X: {:.2f}, {:.2f} : {:5.2f} {:5.2f} {:5.2f}'.
          format(emitx, *emitx_sig))

    emity, emity_var = emit2D_variance(yphase_space, meas_err)
    emity_sig = np.sqrt(np.abs(emity_var))
    print('Y: {:.2f}, {:.2f} : {:5.2f} {:5.2f} {:5.2f}'.
          format(emity, *emity_sig))
    print()

    sigma4 = np.array([[xx, xxp, xy, xyp],
                       [xxp, xpxp, xpy, xpyp],
                       [xy, xpy, yy, yyp],
                       [xyp, xpyp, yyp, ypyp]])
    eig_val, eig_vec = np.linalg.eig(sigma4)

    print('2D emit = {:4.2f} mm-mrad'.format(eig_val.prod()**0.25))
    print('Sigma4 eigenvalues : {:4.2f} {:4.2f} {:4.2f} {:4.2f}'.
          format(*eig_val))
    print()

    # =========================================================================
    # Cholesky decomposition

    #det4, vdet4, emit2, sigemit2 = emit4D(phase_space4d, meas_err)
    #print det4, vdet4, emit2, sigemit2
    #print

    # =========================================================================
    # 2D Cartesian correlations

    print('RMS emit x = {0:.1f} +- {1:.1f} mm-mrad'.
          format(emitx, np.sqrt(emitx_var[0])))
    print('2*sigma_x, 2*sigma_xprime = {0:5.2f} [mm], {1:5.2f} [mrad]'.
          format(2.*np.sqrt(xx), 2*np.sqrt(xpxp)))
    print('x-xp regression: {0:.1f} mrad/mm'.format(xxp/xx))
    print()
    print('RMS emit y = {0:.1f} +- {1:.1f} mm-mrad'.
          format(emity, np.sqrt(emity_var[0])))
    print('2*sigma_y, 2*sigma_yprime = {0:5.2f} [mm], {1:5.2f} [mrad]'.
          format(2.*np.sqrt(yy), 2*np.sqrt(ypyp)))
    print('y-yp regression: {0:.1f} mrad/mm'.format(yyp/yy))
    print()

    if commands['--pdf']:
        PDF.close()
    else:
        print("Hit enter to exit")
        input()

# =========================================================================

if __name__ == '__main__':
    ### parse command line
    commands = docopt(__doc__, version=NDCXII.__version__)

    print(commands)

    if commands['--pdf']:
        PDF = PdfPages(commands['--output'])
        ShowOrSave = PDF.savefig
    else:
        ShowOrSave = partial(plt.show, block=False)

    images = commands['<images>']
    if not 'bkgrd' in images:
        raise SystemExit("Need to define background images using the bkgrd" +
                         " key word, see --help")

    idx = images.index('bkgrd')
    bkgrdimages = images[idx+1:]
    images = images[:idx]

    if len(images) == 0:
        raise SystemExit("Need to define some images.")
    if len(bkgrdimages) == 0:
        raise SystemExit("Need to define some background images, see --help.")

    # get path
    img = images[0]
    path, name = os.path.split(img)
    ext = name.split(".")[-1]

    if ext not in ["spe", "SPE"]:
        raise SystemExit("Please provide SPE images, see --help")

    bkgdFileLocs = NDCXII.helper.GetImageFileNames(bkgrdimages, path,
                                                   name, ext)
    imgFileLocs = NDCXII.helper.GetImageFileNames(images, path, name, ext)

    # flip images horizontally if True img[row,col]
    flip_horizontal = not commands['--noflip']

    # define ROI
    ROI_rlo, ROI_clo, ROI_rhi, ROI_chi\
        = [int(x) for x in commands['--roi'].split(",")]

    if commands['--verbose']:
        print("Pepperpot analysis program")
        print("Flipping images: ", flip_horizontal)
        print("using ROI: {}, {}, {}, {} ".
              format(ROI_rlo, ROI_clo, ROI_rhi, ROI_chi))
        print("Loading images:")
        for f in imgFileLocs:
            print("  ", f)
        print("Loading bkgrd images:")
        for f in bkgdFileLocs:
            print("  ", f)
    #### end command line parsing

    crunch(imgFileLocs, bkgdFileLocs)
