function autohighlight_ndcxii_logbook_folder() {
  var ndcxiifolder = DriveApp.getFolderById('19lWlQOONJ4WSw30p_FHmvLMLzfFGyNhd');
  var files = ndcxiifolder.getFiles();
  while(files.hasNext()) {
    var file = files.next();
    var ID = file.getId();

    var update_time = check_date(ID);
    // we will run this script every N minutes, so no need to check files
    // that haven't been updated for a while (they should already be OK)
    if (update_time < 3600) {
      highlightShotnumbers(ID);
      highlightComment(ID);
    }
    else
    {
      // might as well save the filename in the log file, just in case we want to check
      // and run them by hand
      Logger.log(file.getName());
      Logger.log(ID);
      Logger.log('---------------------');
    }
  }
}

function check_date(ID){
  var file = DriveApp.getFileById(ID);
  var update = file.getLastUpdated();
  var d = new Date();
  var n = d.getDate();

  return (d-update)/1000; // in seconds
}

function test_main(){
  var ID = '1FPQQTRfEv2ybDV2bpCFmj2hAwptS9f6DEaNcaDIsR2o';
  highlightShotnumbers(ID);
  highlightComment(ID);
}

function highlightShotnumbers(ID) {
  var REGEXshot='(\\D|^)3\\d{9}(\\D|$)';
  highlightText(ID, REGEXshot, "#c9daf8");
}

function highlightComment(ID) {
  var REGEXcomment='(\\D|^)20\\d{4}\\s+\\d{5,6}(\\D|$)';
  highlightText(ID, REGEXcomment, "#ffe482");
}

function highlightText(ID, findMe, color) {
  /*  Find a regexp and highlight it in the whole file
  in case the match has non numbers in front or end, we remove them.
  This works well for shotnumbers and comments.
  */
  try {
    var doc = DocumentApp.openById(ID);
  }
  catch(err) {
    return;
  }
  //var doc = DocumentApp.getActiveDocument();
  var body = doc.getBody();
  var foundElement = body.findText(findMe);

  while (foundElement != null) {
    // Get the text object from the element
    var foundText = foundElement.getElement().asText();

    // Where in the Element is the found text?
    // Correct for possible \D at front and end
    var start = foundElement.getStartOffset();
    if(foundText.getText()[start].match(/\D/)) {
      start = start+1;
    };
    var end = foundElement.getEndOffsetInclusive();
    if(foundText.getText()[end].match(/\D/)) {
      end = end-1;
    };

    // Change the background color
    foundText.setBackgroundColor(start, end, color);

    // Find the next match
    foundElement = body.findText(findMe, foundElement);
  }

  doc.saveAndClose();
}