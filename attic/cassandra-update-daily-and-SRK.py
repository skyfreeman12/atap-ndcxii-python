from NDCXII.db import Shot, Comments, Data, Setting
import NDCXII.db as DB
import NDCXII.data as ND
import NDCXII.helper as NDH

import cqlengine 
import sys

from table_iterator import TableIterator
import json

DB.init()

j=0
fixedSRKs=0
added = 0

oldTS = 0
for i in TableIterator(Shot, blocksize=100):
    j += 1
    # keep some progress report
    if j % 100 == 0:
        print("x ",j, i.lvtimestamp, i.devicename, added, fixedSRKs)

    sn = i.lvtimestamp

    # add TS to daily comment
    if sn != oldTS:
        month, daytime = NDH.LVtime2allshots(sn)
        com = Comments.objects(month=month, daytime=daytime, species='').update(shots__add={sn})
        oldTS = sn
        added += 1

        # check for SRK values, fix missing } and convert to int
        s = Shot.objects(lvtimestamp=sn, devicename="SRKs").first()
        if s:
            h = s.settinghash
            
            setting = Setting.objects(Setting.hash==h).first()
            data = setting.data
            
            # fix missing }
            if not data.endswith("}"):
                data = data+"}"
            
            # convert to int
            new = json.loads(data)
            for k in new:
                new[k] = int(new[k])
            new = json.dumps(new)

            Setting.objects(hash=h).update(data=new)                
            fixedSRKs += 1