from NDCXII.db import Shot, Comments, Data, Setting
import NDCXII.db as DB
import NDCXII.data as ND
import NDCXII.helper as NDH

import json
import hashlib

DB.init()

# fix two changes in BPM attenuator settings on Oct 14th and Oct 21th
# fixed on Nov 8th 2016

# oct 14th after shot 3559324205
# change bpm 6 PXI4-9 PXI4-10 from [1,1] to [2,2]

# oct 21th after shot 3559938417
# change bpm 5 PXI4-6 PXI4-8 from [0.5,0.5] to [1,1]

# get all shots that need fixing
A = DB.list_comments(201610, daytime=None, skip4200=False)
B = DB.list_comments(201611, daytime=None, skip4200=False)

C = list(A) + list(B)

# create NEW setting for BPM5
# old settings seem to be all: c4e766b02d41cfbfc1f1daf9c4a4b004
# ca9434ba49f5897ff0af4156907ed48a

def update_setting(oldhash, newvalue):
    oldsetting = Setting.objects(Setting.hash==oldhash).first()

    if not oldsetting:
        return None

    data = json.loads(oldsetting.data)
    # fix problem
    data['external attenuator/gain'] = newvalue

    jdata = json.dumps(data, sort_keys=True)

    m = hashlib.md5()
    m.update(jdata.encode('ascii'))
    newhash = m.hexdigest()
    Setting.create(hash=newhash, data=jdata, version=2)
    return newhash

# create NEW settinsg for BPM 6
#257f7f3f33a6273518b652d1032d43bf
#c12b5552d3f8123a96c13ff0caa015de


# update all setting hashes for BPM5 in old shots
for i in C:
    if str(i[1]).endswith('4200'):
        shots = DB.shots_from_comment(i[0], i[1])
        for s in shots:
            if s <= 3559938417 or s > 3561483711:
                continue
            else:
                S = Shot.objects(lvtimestamp=s, devicename="PXI4Slot6").first()
                if S:
                    r = update_setting(S.settinghash, [1, 1])
                    if r is not None:
                        T = Shot.objects(lvtimestamp=s, devicename="PXI4Slot6").update(settinghash=r)

                S = Shot.objects(lvtimestamp=s, devicename="PXI4Slot8").first()
                if S:
                    r = update_setting(S.settinghash, [1, 1])
                    if r is not None:
                        T = Shot.objects(lvtimestamp=s, devicename="PXI4Slot8").update(settinghash=r)
                
            if s <= 3559324205 or s > 3561483711:
                continue
            else:
                S = Shot.objects(lvtimestamp=s, devicename="PXI4Slot9").first()
                if S:
                    r = update_setting(S.settinghash, [2, 2])
                    if r is not None:
                        T = Shot.objects(lvtimestamp=s, devicename="PXI4Slot9").update(settinghash=r)

                S = Shot.objects(lvtimestamp=s, devicename="PXI4Slot10").first()
                if S:
                    r = update_setting(S.settinghash, [2, 2])
                    if r is not None:
                        T = Shot.objects(lvtimestamp=s, devicename="PXI4Slot10").update(settinghash=r)
