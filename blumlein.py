"""blumlein

Usage:
   blumlein [options] <month> <date>


Options:
   -h --help      this page
   -l --list      list shots from this month

"""

import sys
import cassandra
import json
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.ioff()
from docopt import docopt
from cassandra.cluster import Cluster

import NDCXII

commands = docopt(__doc__, version=NDCXII.__version__)

cluster = Cluster(['128.3.58.20'])
session = cluster.connect('test')

if commands["--list"]:
    print("available list of month and daytime values")
    rows = session.execute('select month, daytime from comments;')
    for r in rows:
        print(r.month, r.daytime)
    sys.exit()

month = int(commands['<month>'])
daytime = int(commands['<date>'])

print("getting shot list")

rows = session.execute("select shots from comments where month={} and daytime={};".
                        format(month, daytime))

devicelist8 = ['PXI3Slot4']
devicelist2 = ['PXI0Slot9', 'PXI0Slot10', 'PXI0Slot11', 'PXI0Slot12', 'PXI4Slot12', 'PXI4Slot14', 'PXI4Slot15']

print("getting data")

waves = []
upstairs = []
for s in sorted(rows[0].shots):
    tmp = []
    for scope in devicelist2:
        data = session.execute("select data from shot where lvtimestamp={} and devicename='{}';".
                                format(s, scope))
        if scope in ['PXI4Slot12', 'PXI4Slot14', 'PXI4Slot15']:
            scaling = -350000.0
        elif scope in ['PXI0Slot9', 'PXI0Slot10', 'PXI0Slot11', 'PXI0Slot12']:
            scaling = -20000.0
            scaling_inj = 100000.0
        try:
            t = json.loads(data[0].data)
            if scope == 'PXI0Slot9':
                tmp.append(np.array(t['wave0'])*scaling_inj)
            else:
                tmp.append(np.array(t['wave0'])*scaling)
            if scope != 'PXI4Slot15':
                tmp.append(np.array(t['wave1'])*scaling)
        except:
            pass
    if len(tmp)==13:
        waves.append(tmp)
    else:
        waves.append([np.NaN]*13)
        print("problem with shot", s, "   only", len(tmp), "channels")
    #tmp = []
    #for dev in devicelist8:
    #    data = session.execute("select data from shot where lvtimestamp={} and devicename='{}';".
    #                            format(s, dev))
    #    try:
    #        t = json.loads(data[0].data)
    #        tmp.append(t['wave3'])
    #        tmp.append(t['wave4'])
    #        tmp.append(t['wave5'])
    #        tmp.append(t['wave6'])
    #        tmp.append(t['wave7'])
    #        upstairs.append(tmp)
    #    except:
    #        pass

print("converting data")

waves = np.array(waves)
upstairs = np.array(upstairs)

print("find maximum")

# find maximum
mymax = []
for i, w in enumerate(waves):
    tmp = []
    for d in w:
        j = np.argmax(d)
        if type(d)!=type(1.0) and d[j] > 10000:
            # find first datapoint where we are above MAX/2
            m = d[j]
            j = np.argmin(np.abs(d[:j]-m/2))
            tmp.append(j)
        else:   
            tmp.append(np.NaN)
    mymax.append(tmp)

mymax = np.array(mymax)

# same for upstairs
#mymax2 = []
#for i, w in enumerate(upstairs):
#    tmp = []
#    for d in w:
#        mydiff = d[:-1]-d[1:]
        

print("plotting", mymax.shape)

fig, ax1 = plt.subplots(1,1)
for i in range(mymax.shape[1]):
    ax1.plot(mymax[:, i]*2e-3)
    try:
        plt.text(len(mymax[:, i])-4-15*(i%2), 0.1+np.ma.masked_invalid(mymax[:, i]*2e-3).mean(),
                "$\sigma = {:.2f} ns$".format(np.ma.masked_invalid(mymax[:, i]).std()))
    except:
        pass
plt.ylabel("time [us]")
plt.xlabel("shotnumber")
plt.title("2014-10-8 18:20 statistics from ~100 shots")
plt.ylim([0,11])
plt.show()

#fig, ax1 = plt.subplots(1,1)
#for i in range(mymax.shape[1]):
#    plt.hist(mymax[:, i]*2e-3, 20, edgecolor="none")
#plt.xlabel("time [us]")
#
#plt.show()

    
    
    
    




