"""fluence

Usage:
    fluence.py [options] <filepath>

Options:
    -h --help                       Show this screen
    -p --plot                       Show all of the available plots
    -f --fit                        Use fitting routines in calculations
    --b=<bkgd>                      Specify background subtraction
    --side=<sidelength>             Specify side length of square in mm for averaged fluence in active area [default: 0.5]
    --radius=<radius>               Specify radius in mm [default: 1]
    -q --Q=<charge>                 Specify the charge (in nC) of the beam for plotting
    -k --E=<energy>                 Specify the energy (in MeV) of the beam for plotting
    -o --offset=<off>               Plot zoomed in <off> indices
    --epc=<cal>                     Guess the energy per count, should always be the same
    --threshold=<thr>               Specify a threshold in J/cm^2 to color red in the plot
    -c <pxmm> --calibration=<pxmm>  How many pixel per mm [default: 11]
    --mask                          mask hot pixels automatically
    --masklevel <level>             mask hot pixel threshold [default: 150]

If <filepath> points to a folder, the program analyzes all .SPE
files in that folder. If <filepath> points to an image, the
program only analyzes that image.

The program expects the files to have the format
YYYY-MM-DD-imageNNNN.SPE.  It will generate two or more files to store
the results: YYYY-MM-DD.txt and analyzed_images.ini.

"""

import configparser
from docopt import docopt
from json import JSONDecoder, JSONEncoder
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, Ellipse
from matplotlib import cm
from NDCXII.readSPE import load
import numpy as np
import os
import scipy.ndimage
from scipy.interpolate import UnivariateSpline
from scipy.optimize import curve_fit
from scipy.ndimage import median_filter
from collections import namedtuple
from math import ceil

# track the X and Y offsets that happen when zooming into the ROI
trackX = 0
trackY = 0


Cauchy = namedtuple('Cauchy', ['Amplitude', 'Xc', 'Yc', 'offset', 'gammaX', 'gammaY'])


def cauchy(domain, A, x0, y0, offset, gammax, gammay):
    (x, y) = domain
    g = offset + A * (1 / (1 + ((x - x0) / gammax)
                           ** 2 + ((y - y0) / gammay)**2))
    return g.ravel()


Gaussian = namedtuple('Gaussian', ['Amplitude1', 'Xc', 'Yc', 'offset',
                                   'sigmaX1', 'sigmaY1',
                                   'Amplitude2', 'sigmaX2', 'sigmaY2'])


def gaussian(domain, A, x0, y0, offset, sigx, sigy, B, sigx2, sigy2):
    (x, y) = domain
    a = offset
    b = A * np.exp(-0.5 * (((x - x0) / sigx) ** 2 + ((y - y0) / sigy)**2))
    c = B * np.exp(-0.5 * (((x - x0) / sigx2) ** 2 + ((y - y0) / sigy2)**2))
    g = a + b + c
    return g.ravel()


def center_image(img, Xc, Yc, minvalue=100000000):
    """return the largest image portion with the given coordinates at the center"""
    global trackX, trackY
    h, w = img.shape
    r = int(min([Yc, h - Yc,
                 Xc, w - Xc, minvalue]))
    img = img[int(Xc - r):int(Xc + r),
              int(Yc - r): int(Yc + r)]
    trackX += int(Xc - r)
    trackY += int(Yc - r)
    return img, r


def fit(a, guess, plot=True):
    ah, aw = a.shape
    x = np.linspace(0, ah - 1, ah)
    y = np.linspace(0, aw - 1, aw)
    x, y = np.meshgrid(x, y)
    cpopt, cpcov = curve_fit(cauchy, (x, y), a.ravel(), p0=guess)
    cpopt = Cauchy(*cpopt)
    #cdata_fitted = cauchy((x, y), *cpopt).reshape(ah, aw)

    initial_guess = Gaussian(Amplitude1=cpopt.Amplitude/2,
                             Xc=cpopt.Xc,
                             Yc=cpopt.Yc,
                             offset=cpopt.offset,
                             sigmaX1=cpopt.gammaX,
                             sigmaY1=cpopt.gammaY,
                             Amplitude2=cpopt.Amplitude/2,
                             sigmaX2=cpopt.gammaX,
                             sigmaY2=cpopt.gammaY)

    popt, pcov = curve_fit(gaussian, (x, y), a.ravel(), p0=initial_guess)
    popt = Gaussian(*popt)
    data_fitted = gaussian((x, y), *popt).reshape(ah, aw)

    print("\nGaussian fit parameters:")
    for arg, val in zip(popt._fields, popt):
        print('{:>11s}: {}'.format(arg, val))

    if plot:
        fig, ax = plt.subplots(1, 1)
        ax.set_xlabel(r'Horizontal Pos. [pixel]')
        ax.set_ylabel(r'Vertical Pos. [pixel]')
        ax.set_title(r'Gaussian fit (Contour)')
        im = ax.imshow(a, cmap=cm.plasma)
        ax.contour(x, y, scipy.ndimage.rotate(data_fitted, 90), 5, colors='k')
        cbar = fig.colorbar(im)
        cbar.ax.get_yaxis().labelpad = 15
        cbar.ax.set_ylabel(r'Pixel Brightness')

        # find FWHM in x and y
        x = np.linspace(0, ah - 1, ah)
        y = np.linspace(0, aw - 1, aw)
        Xs = data_fitted[:, int(popt.Xc)]
        Xspline = UnivariateSpline(x, Xs - max(Xs)/2, s=0)
        Xroots = Xspline.roots()
        Xr1 = min(Xroots)
        Xr2 = max(Xroots)
        Ys = data_fitted[int(popt.Yc), :]
        Yspline = UnivariateSpline(y, Ys - max(Ys)/2, s=0)
        Yroots = Yspline.roots()
        Yr1 = min(Yroots)
        Yr2 = max(Yroots)
        FWHMx = abs(Xr1 - Xr2)
        FWHMy = abs(Yr1 - Yr2)

        fig, (ax1, ax2) = plt.subplots(2, 1)
        ax1.plot(a[int(popt.Xc), :], '.', label="Data")
        ax1.plot(data_fitted[int(popt.Xc), :], '--', label="Gaussian fit")
        ax1.axvspan(Xr1, Xr2, ls='--', fill=False, label="FWHM")

        ax2.plot(a[:, int(popt.Yc)], '.')
        ax2.plot(data_fitted[:, int(popt.Yc)], '--')
        ax2.axvspan(Yr1, Yr2, ls='--', fill=False)
        ax2.set_xlabel('Position [pixel]')
        ax2.set_ylabel('Intensity (counts)')
        ax1.set_ylabel('Intensity (counts)')
        ax1.set_title('X lineout through center')
        ax2.set_title('Y lineout through center')
        fig.suptitle('Fitted data')
        ax1.legend()
        plt.show(block=False)
    return popt


Guess = namedtuple('Guess', ['Amplitude', 'Yc', 'Xc', 'offset',
                             'Xsigma', 'Ysigma'])


def get_guess(a, plot, background):
    ROI = a  # ROI is region of interest with spot centered
    h, w = ROI.shape

    bkgdsum = 0

    if plot:
        fig1 = plt.figure(1)
        ax11 = fig1.add_subplot(111)
        ax11.set_xlabel(r'Horizontal Pos. [pixel]')
        ax11.set_ylabel(r'Vertical Pos. [pixel]')
        ax11.imshow(ROI)
        ax11.set_title('Raw image')

    mask = ROI > 0.75 * ROI.max()
    label_im, nb_labels = scipy.ndimage.label(mask)
    com = scipy.ndimage.center_of_mass(ROI, label_im)
    for i in range(3):
        ROI, _ = center_image(ROI, com[0], com[1])
        h, w = ROI.shape
        com = scipy.ndimage.center_of_mass(ROI)
    if background is None:
        bkgd = np.mean(np.concatenate((ROI[0: 10, 0: 10],
                                       ROI[0: 10, w - 11: w - 1],
                                       ROI[h - 11: h - 1, 0: 10],
                                       ROI[h - 11: h - 1, w - 11: w - 1])))
    else:
        bkgd = float(background)

    bkgdsum = bkgdsum + bkgd
    print('Background: %g' % bkgdsum)

    ROI = ROI - bkgd
    mask = ROI < 0
    ROI[mask] = 0

    Xc = int(com[1])
    Yc = int(com[0])

    # cut of image through centroid
    Ys = ROI[Xc, :]
    Xs = ROI[:, Yc]

    # find the moments and use to find 2* sqrt(rms)
    lx, ly = ROI.shape
    peak = np.amax(ROI)

    M00 = ROI.sum()
    xind, yind = np.indices(ROI.shape)
    dx = xind - np.ones((lx, ly)) * Xc
    dxsq = np.square(dx)
    M20 = np.multiply(dxsq, ROI).sum()

    dy = yind - np.ones((lx, ly)) * Yc
    dysq = np.square(dy)
    M02 = np.multiply(dysq, ROI).sum()
    Xrms = np.sqrt(M20 / M00)
    a = 2 * Xrms
    Yrms = np.sqrt(M02 / M00)
    b = 2 * Yrms

    # find FWHM in x and y
    Xspace = np.linspace(0, h, h)
    Yspace = np.linspace(0, w, w)
    Xspline = UnivariateSpline(Xspace, Xs - max(Xs)/2, s=0)
    Xroots = Xspline.roots()
    Xr1 = min(Xroots)
    Xr2 = max(Xroots)
    Yspline = UnivariateSpline(Yspace, Ys - max(Ys)/2, s=0)
    Yroots = Yspline.roots()
    Yr1 = min(Yroots)
    Yr2 = max(Yroots)
    FWHMx = abs(Xr1 - Xr2)
    FWHMy = abs(Yr1 - Yr2)

    # A, x0, y0, offset, gammax, gammay
    guess = Guess(Amplitude=np.max(ROI) - bkgd, Yc=float(Yc), Xc=float(Xc),
                  offset=bkgd, Xsigma=FWHMx/2, Ysigma=FWHMy/2)

    return guess, ROI


def analyze(ROI,
            guess,
            imnumber,
            date,
            Q,
            E,
            foldername,
            epc,
            thr,
            side,
            radius,
            dofit=False,
            plot=False,
            write=False):


    global px_per_mm
    global trackX, trackY

    lx, ly = ROI.shape
    ROI, offset = center_image(ROI, guess.Xc, guess.Yc, minvalue=100)
    guess = Guess(Amplitude=guess.Amplitude,
                  Xc=offset, Yc=offset,
                  offset=guess.offset,
                  Xsigma=guess.Xsigma, Ysigma=guess.Ysigma)

    # Constants and Callibration
    MeVtoJ = 1.60218e-13  # J/MeV
    e = 1.60217e-19  # C

    # guess = [A, y0, x0, offset, gammax, gammay]

    if dofit:
        acoeffs = fit(ROI, guess, plot=plot)
        guess = acoeffs  # A, y0, x0, offset, sigx, sigy, B, sigx2, sigy2

        Xc = int(guess.Xc)
        Yc = int(guess.Yc)
        ROI, offset = center_image(ROI, guess.Xc, guess.Yc)
        Xc = int(offset)
        Yc = int(offset)
        guess = Gaussian(Amplitude1=guess.Amplitude1,
                         Xc=Xc, Yc=Yc,
                         offset=guess.offset,
                         Amplitude2=guess.Amplitude2,
                         sigmaX1=guess.sigmaX1,
                         sigmaY1=guess.sigmaY1,
                         sigmaX2=guess.sigmaX2,
                         sigmaY2=guess.sigmaY2)

    # find the moments and use to find 2* sqrt(rms)
    M00 = ROI.sum()
    lx, ly = ROI.shape
    peak = np.amax(ROI)

    xind, yind = np.indices(ROI.shape)
    dx = xind - np.ones((lx, ly)) * Xc
    dxsq = np.square(dx)
    M20 = np.multiply(dxsq, ROI).sum()

    dy = yind - np.ones((lx, ly)) * Yc
    dysq = np.square(dy)
    M02 = np.multiply(dysq, ROI).sum()
    Xrms = np.sqrt(M20 / M00)
    a = 2 * Xrms
    Yrms = np.sqrt(M02 / M00)
    b = 2 * Yrms

    # calculate scaling factors to go from pixel intensity to mJ/cm^2
    Eperct = None
    scaling = None
    if not (Q is None or E is None) or epc is not None:
        if epc is None:
            Q = float(Q) * 1e-9  # C
            E = float(E)  # MeV
            Eperct = Q / e * E / M00
        else:
            Eperct = float(epc)
        print('\nEnergy per count = ' + str(Eperct))

        # change pixel brightness to mJ/cm^2
        # assume 100 pixels = 1 cm
        scaling = Eperct * MeVtoJ * 1000

    # find FWHM in x and y
    h, w = ROI.shape
    # cut of image through centroid
    Xspace = np.linspace(0, h-1, h)
    Yspace = np.linspace(0, w-1, w)
    if dofit:
        print("Use fitted data for FWHM")
        data_fitted = gaussian(np.meshgrid(Xspace, Yspace), *guess).reshape(h, w)
        Xs = data_fitted[:, int(guess.Xc)]
        Ys = data_fitted[int(guess.Yc), :]
    else:
        print("Using raw data for FWHM (will over estimate the FWHM if the data is noisy)")
        Ys = ROI[Xc, :]
        Xs = ROI[:, Yc]
    # find FWHM in x and y
    Xspline = UnivariateSpline(Xspace, Xs - max(Xs) / 2, s=0)
    Xroots = Xspline.roots()
    Xr1 = min(Xroots)
    Xr2 = max(Xroots)
    Yspline = UnivariateSpline(Yspace, Ys - max(Ys) / 2, s=0)
    Yroots = Yspline.roots()
    Yr1 = min(Yroots)
    Yr2 = max(Yroots)
    FWHMx = abs(Xr1 - Xr2)
    FWHMy = abs(Yr1 - Yr2)

    print('Width using rms:  {0:.3f} pix, using FWHM: {1:.3f} pix'.format(
        4*Yrms, FWHMy))
    print('Height using rms: {0:.3f} pix, using FWHM: {1:.3f} pix'.format(
        4*Xrms, FWHMx))

    # plot a favorite shot from July 1, 2016
    x, y = np.meshgrid(Xspace, Yspace)
    tinstandard = Gaussian(Amplitude1=12307.8, Yc=Yc, Xc=Xc, offset=0,
                           sigmaX1=8.7, sigmaY1=17.6,
                           Amplitude2=10590, sigmaX2=20.8, sigmaY2=17.15)
    tsd = gaussian((x, y), *tinstandard).reshape(h, w)

    if plot:
        comcent = Circle((Yc, Xc), 1, color='k')
        fig, ax = plt.subplots(1, 1)
        ax.set_xlabel('Horizontal Pos. [pixel]')
        ax.set_ylabel('Vertical Pos. [pixel]')
        ax.set_title('Beam center X={} Y={}'.format(Yc+trackY, Xc+trackX))
        print("Beam center:      X={} Y={}".format(Yc+trackY, Xc+trackX))
        im = ax.imshow(ROI, cmap=cm.plasma)
        ax.add_artist(comcent)

        fig, (ax1, ax2) = plt.subplots(2, 1)
        fig.suptitle('Vertical and horizontal fluence')

        ax1.plot(Yspace, Ys, '.', label='Image through centroid')
        ax1.plot(Yspace, tsd[Yc, :], 'r--', label='"Favorite" shot from July 2016')
        ax1.axvspan(Yr1, Yr2, ls='--', fill=False)
        ax1.axvspan((Yc - b), (Yc + b), ls='-', fill=False)
        ax1.set_xlabel('vertical pos. [pixel]')
        ax1.set_ylabel('Fluence (counts/pixel)')
        ax1.legend()

        ax2.plot(Xspace, Xs, '.', Xspace, tsd[:, Xc], 'r--')
        ax2.axvspan(Xr1, Xr2, ls='--', fill=False)
        ax2.axvspan((Xc - a), (Xc + a), ls='-', fill=False)
        ax2.set_xlabel('Horizontal Pos. [pixel]')
        ax2.set_ylabel('Fluence (counts/pixel)')

        # plot the image with x and y axes scaled to physical values
        A = Ellipse((0, 0), width=4*Xrms/px_per_mm,
                    height=4*Yrms/px_per_mm, fill=False, ls='-', color='k', label='4*RMS')
        F = Ellipse((0, 0), width=FWHMx/px_per_mm, height=FWHMy/px_per_mm,
                    fill=False, ls='--', color='k')

        fig, ax = plt.subplots(1, 1)
        ax.set_xlabel('Horizontal Pos. [mm]')
        ax.set_ylabel('Vertical Pos. [mm]')
        ax.set_title(r'Fluence in count/cm$^2$')
        im = ax.imshow(ROI * px_per_mm**2 * 100, cmap=cm.plasma,
                       extent=[-Yc / px_per_mm,
                               (ly - Yc) / px_per_mm,
                               -(lx - Xc) / px_per_mm,
                               Xc / px_per_mm])
        cbar = fig.colorbar(im)
        cbar.ax.get_yaxis().labelpad = 20
        cbar.ax.set_ylabel(r'Fluence [counts/cm$^{2}$]', rotation=270)
        ax.add_artist(A)
        ax.add_artist(F)
        ax.text(0, -4*Xrms/2/px_per_mm + .7, 'RMS', horizontalalignment='center')
        ax.text(0, -FWHMx/2/px_per_mm + .7, 'FWHM', horizontalalignment='center')

#        from mpl_toolkits.axes_grid.inset_locator import inset_axes
#
#        axtmp = inset_axes(ax, width="100%", height="20%",
#                           loc=6, borderpad=0,
#                           axes_kwargs=dict(frameon=False))
#
#        axtmp.plot((Xspace-Xc)/px_per_mm, Xs, '.')
#        axtmp.axvspan((Xr1-Xc)/px_per_mm, (Xr2-Xc)/px_per_mm, ls='--', fill=False)
#        axtmp.set_xlabel('Horizontal Pos. [pixel]')
#        axtmp.set_ylabel('Fluence (counts/pixel)')
#        axtmp.set_xlim(ax.get_xlim())

        # if the charge and energy of the beam is specified, estimate the
        # fluence for each pixel
        if Eperct is not None and scaling is not None:
            tmp = ROI * scaling * px_per_mm**2 * 100

            # set extent for image and overlay
            # must be the same
            extent = -Yc/px_per_mm, (ly - Yc)/px_per_mm, -(lx - Xc)/px_per_mm, Xc/px_per_mm

            fig, ax = plt.subplots(1, 1)
            ax.set_xlabel('Horizontal Pos. [mm]')
            ax.set_ylabel('Vertical Pos. [mm]')
            ax.set_title("Fluence")
            im = ax.imshow(tmp, cmap=cm.plasma,
                           interpolation='nearest',
                           extent=extent)
            cbar = fig.colorbar(im)
            cbar.ax.get_yaxis().labelpad = 20
            cbar.ax.set_ylabel('Fluence [mJ/cm$^{2}$]', rotation=270)

            # highlight area above threshold
            if thr is not None:
                thr = float(thr)
                plt.hold(True)
                tmpplot = tmp.copy()
                tmpplot[tmpplot >= thr] = 100
                tmpplot[tmpplot < thr] = np.nan
                # overlay = ax.imshow(tmpplot,
                #                     alpha=.5,
                #                     cmap=plt.cm.jet,
                #                     extent=extent,
                #                     interpolation='bilinear')

            # Draw FWHM ellipse
            F = Ellipse((0, 0),
                        width=FWHMx/px_per_mm, height=FWHMy/px_per_mm,
                        fill=False, ls='--', color='k')
            ax.add_artist(F)

    # Area_rms = np.pi * Xrms * Yrms * 4  #
    Area_FWHM = np.pi * FWHMx/2 / px_per_mm * FWHMy/2 / px_per_mm / 100  # cm^2

    print('FWHM:             {:.3f}mm {:.3f}mm'.format(FWHMx / px_per_mm,
                                                       FWHMy / px_per_mm))
    # Counts within 2*rms ellipse
    # tmp = np.copy(ROI)
    # X, Y = np.ogrid[0: lx, 0: ly]
    # mask2 = ((X - Xc) / a)**2 + ((Y - Yc) / b)**2 > 1
    # tmp[mask2] = 0
    # tot = sum(sum(tmp))
    # print('Counts within 2*rms Ellipse {0}'.format(tot))

    # Counts within FWHM ellipse
    tmp = ROI.copy()
    X, Y = np.ogrid[0: lx, 0: ly]
    mask = ((X - Xc) * 2 / FWHMx)**2 + ((Y - Yc) * 2 / FWHMy)**2 < 1
    tot2 = np.sum(tmp[mask])
    print('Counts within FWHM ellipse {}'.format(tot2))

    # Counts within N pixel ellipse
    # David Raftrey 11.28.2017
    M = int(round(radius * px_per_mm)) # Converts from mm to pixels. Rounds to nearest pixel.
    X, Y = np.ogrid[0: lx, 0: ly] # gets image into easy to manipulate form
    mask = ((X - Xc) / M)**2 + ((Y - Yc) / M)**2 < 1 # A single mask selects vaues within elipse. If Xc=Yc the elipse is a circle.
    tot3 = np.sum(tmp[mask]) # finds the number of pixels in elipse
    tot3_pxcount = mask.sum() # adds all the counts in the elipse
    Area3 = np.pi * M / px_per_mm * M / px_per_mm / 100  # area of elipse in cm^2
    print('tot3 counts, pxcounts',tot3, ' counts ', tot3_pxcount, ' pixels', 'area3 ', Area3) # output

    # Counts within N x N pixel square
    # David Raftrey 11.28.2017
    N = int(ceil(side * px_per_mm / 2 - 1)) # Converts from mm to pixels. Rounds to nearest pixel.
    X, Y = np.ogrid[0: lx, 0: ly] # gets image into easy to manipulate form
    mask1 = abs((X - Xc) / N) <= 1 # first mask selects pixels with x values within square (includes border)
    mask2 = abs((Y - Yc) / N) <= 1 # second mask selects pixels with y values within square (includes border)
    mask = np.logical_and(mask1, mask2) # combine the two masks to select a square of pixels
    tot4 = np.sum(tmp[mask]) # finds the number of pixels in square
    tot4_pxcount = mask.sum() # adds all the counts in the square
    Area4 = tot4_pxcount * 1 / (px_per_mm**2) * 1 / 100  #   mm**2/11pix^2 * 1cm**2/100mm**2 = area of square in cm^2
    print('tot4 counts, pxcounts',tot4, ' counts ', tot4_pxcount, ' pixels', 'area4 ', Area4) # output

    # Fluence at the centroid

    # print('Fluence at the centroid: {0:.3g} J/cm^2'.format(
    #    Centroid_Fluence))

    # Peak Fluence

    # Avg Fluence using RMS Ellipse

    # Avg_Fluence_rms = tot * EperCt / Area_rms * MeVtoJ  # J/cm^2
    # print(
    #    'Fluence for 2*rms ellipse region {0:.3g} J/cm^2'.format(Avg_Fluence_rms))

    # Avg Fluence using FWHM Ellipse

    # * EperCt / Area_FWHM * MeVtoJ  # counts/pixel^2
    Avg_Fluence_FWHM = tot2 / Area_FWHM

    # Print
    # print('\nTotal Counts after background subtraction: {0:g}'.format(M00))
    # print('2*rms elipse area:                        {0:g}'.format(Area_rms))
    # print('Fluence:                                  {0:g}'.format(
    #     M00 / Area_rms))
    print('Area of ellipse:               {:.3g} cm^2'.format(Area_FWHM))

    print('Peak:                          {:.3g}'.format(peak))
    print('Peak (mJ/cm2):                 {:.3g}'.format(peak*px_per_mm* px_per_mm*100*scaling))
    if scaling is not None:
        print('number ions (total):           {0:.3g} ions'.format(Q/e))
    print('\nAverages over FWHM:')
    print('Counts within FWHM Ellipse     {:.3g} counts'.format(tot2))
    if scaling is not None:
        print('Avg Fluence:                   {:8.3g} pi/cm^2  {:5.3g} mJ/cm^2  {:8.3g} ions/cm^2'.
              format(Avg_Fluence_FWHM, Avg_Fluence_FWHM*scaling, Avg_Fluence_FWHM*Q/e/M00))
    else:
        print('Avg Fluence:                   {:8.3g} pi/cm^2'.format(Avg_Fluence_FWHM))

    print('\nAverages over ',M,' px radius:')
    print('Peak averaged ',M,' px:            {:.3g}'.format(tot3/tot3_pxcount))
    if scaling is not None:
    	print('Avg Fluence in ',M,' pixel radius: {:8.3g} pi/cm^2  {:5.3g} mJ/cm^2  {:8.3g} ions/cm^2'.format(tot3/Area3, tot3/Area3*scaling, tot3*Q/e/Area3/M00))
        # the peaking factor of the elipse itself
        # given by (fluence of maximum count pixel)/(fluence of entire elipse)
    	print('Elipse peaking factor: ', peak*px_per_mm* px_per_mm*100/(tot3/Area3))

    else:
        print('Avg Fluence in 5 pixel radius: {:.3g} pi/cm^2'.format(tot3/Area3))

    #square area calculations
    print('Avg Fluence in {:1.3g} pixel per side square: {:8.3g} /cm^2  {:5.3g} mJ/cm^2  {:8.3g} ions/cm^2'.format(2 * N + 1, tot4/Area4, tot4/Area4*scaling, tot4*Q/e/Area4/M00))
    # the peaking factor of the square itself
    # given by (fluence of maximum count pixel)/(fluence of entire square)
    print('Square peaking factor: ', peak*px_per_mm* px_per_mm*100/(tot4/Area4))

    # outputs the peaking factor with the square as the peak of circle
    # the peaking factor is (counts_square/area_square)/(counts_circle/area_circle)
    print('Peaking factor = Fluence_square/Fluence_circle: {:1.3g}'.format((tot4/Area4*scaling)/(tot3/Area3*scaling)))

    if write:
        header = ['img',
                  'area_FWHM (cm^2)',
                  'peak (counts)',
                  'total in FWHM (counts)',
                  'Avg_Fluence_FWHM (counts/cm^2)',
                  'Avg_Fluence_5p (counts/cm^2)',
                  'Mass (counts)']
        forfile = [imnumber,
                   Area_FWHM,
                   peak,
                   tot2,
                   Avg_Fluence_FWHM,
                   tot3 / Area3,
                   M00]

        # format string
        s = '\t'.join(['{}'] * len(forfile)) + '\n'

        filename = foldername + '/' + date + '.txt'

        print(filename)

        # write header if needed
        if not os.path.isfile(filename):
            with open(filename, 'w') as newfile:
                newfile.write(s.format(*header))

        # write data
        with open(filename, 'a') as f:
            f.write(s.format(*forfile))


def main():
    global px_per_mm
    commands = docopt(__doc__, version=1)

    plot = commands['--plot']
    dofit = commands['--fit']
    p = commands['<filepath>']
    bkgd = commands['--b']
    side = float(commands['--side'])
    radius = float(commands['--radius'])
    pathls = p.split('/')
    Q = commands['--Q']
    E = commands['--E']
    epc = commands['--epc']
    thr = commands['--threshold']


    px_per_mm = int(commands['--calibration'])
    mask = commands['--mask']
    masklevel = int(commands['--masklevel'])

    #radius = int(commands['--r'])

    configin = configparser.ConfigParser()

    if not pathls[-1].endswith('.SPE'):
        folder_contents = os.listdir(path=p)
        foldername = p
    else:
        folder_contents = [pathls[-1]]
        if '/' in p:
            foldername = p.replace('/' + folder_contents[0], '')
        else:
            foldername = '.'

    if os.path.isfile(foldername + '/' + 'analyzed_images.ini'):
        configin.read(foldername + '/analyzed_images.ini')
        analyzed = set(JSONDecoder().decode(configin['ANALYZED']['images']))
    else:
        analyzed = set()

    for ele in folder_contents:
        if ele.endswith('.SPE'):
            write = ele not in analyzed
            imfile = ele
            a = load(foldername + '/' + imfile)
            if len(folder_contents) == 1:
                a = load(p)
                print('total without background subtraction', a.sum())
            imnumber = imfile[-6:-4]
            date = imfile[:10]
            print('\n' + '*' * 50 + '\n' + imfile + '\n' + '*' * 50 + '\n')
            if mask:
                c = scipy.ndimage.filters.median_filter(a, size=3)
                b = scipy.ndimage.filters.median_filter(a, footprint=np.array(
                    [[True, True, True], [True, False, True], [True, True, True]]))
                diff = np.abs(c.astype(np.float) - a.astype(np.float))
                # overwrite hot pixels with average of their neighbors
                a[diff > masklevel] = b[diff > masklevel]
                print("# hot pixels: ", (diff > masklevel).sum())
            guess, ROI = get_guess(a, plot, bkgd)
            analyze(ROI, guess, imnumber, date,
                    Q, E, foldername, epc, thr, side, radius, dofit, plot, write)
            analyzed.add(ele)

    plt.show()
    config = configparser.ConfigParser()
    config['ANALYZED'] = {'images': JSONEncoder().encode(tuple(analyzed))}
    with open(foldername + '/analyzed_images.ini', 'w') as f:
        config.write(f)

main()
