import NDCXII.helper as helper
from datetime import datetime

import pytest


def test_lvtime2unix():
    shot = 3451231234
    with pytest.raises(TypeError):
        helper.lvtime2unix('3211231234')
    with pytest.raises(TypeError):
        helper.lvtime2unix(1.0)
    with pytest.raises(ValueError):
        helper.lvtime2unix(0)
    unixtime = int(datetime(2015, 1, 1).strftime("%s"))
    assert helper.lvtime2unix(3502944000) == unixtime


def test_unix2lvtime():
    unixtime = datetime(2015, 1, 1)
    assert helper.unix2lvtime(unixtime) == 3502944000
