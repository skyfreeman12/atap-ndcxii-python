Average period = 0.279407407407 [m]

=====================================
Vmon measure  Mon Dec  2 15:16:24 2013
=====================================

Reading Vmon data
Collecting 26 device channels
Opening 3464613780.pkl
# Peak voltages (kV):
#  Injector: 139.06
#  Cell 1: 0.58
#  Cell 2: 0.66
#  Cell 3: 0.65
#  Cell 4: 0.79
#  Cell 5: 0.48
#  Cell 6: 0.68
#  Cell 7: 0.55
Injector delay t= 0.466 usec
Perv = 4.09031929997e-05
Peak current = 55.0 mA
Bunch charge = 46.1585908306 nC
Peak exit time = 1.0315958639 usec
Current waveform FWHM = 1010.0 ns
Leading edge exit time = 0.12
Average KE [keV], beta = 130.393345633 0.00635099336483
tedge, tpeak, tau_exit = 0.12 1.0315958639 0.44370795362552806 usec
tref1 = 0.346291056349 usec
=====================================
Imon measure  Mon Dec  2 15:16:24 2013
=====================================

Reading Imon data
Collecting 26 device channels
Opening 3464613780.pkl
Collecting 26 device channels
Opening 3464614002.pkl
Calibration (atten.): [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 5.0, 5.0, 5.0, 5.0]
Imon signal mask: [[ 2.2  5. ]
 [ 2.4  5. ]
 [ 2.5  5.5]
 [ 2.8  5.7]
 [ 3.   6.5]
 [ 3.   6.5]
 [ 3.9  6.5]
 [ 4.   6.8]
 [ 4.7  7.5]
 [ 5.1  8. ]]
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
Monitor #, L/R (usec), Peak current [mA], total charge [nC]:
  1   1.84   54.4   44.4
  2   1.63   56.7   40.8
  3   2.05   56.8   44.0
  4   1.96   56.1   41.0
  5   1.91   43.1   35.8
  6   1.77   41.6   33.1
  7   1.95   40.7   32.9
  8   2.08   40.4   33.0
  9   2.21   39.2   33.7
 10   1.82   38.1   31.1
=====================================
BPMmeasure  Mon Dec  2 15:16:24 2013
=====================================

Signal,bkgd shot: 3464613780 3464614002
Collecting 26 device channels
Opening 3464613780.pkl
Collecting 26 device channels
Opening 3464614002.pkl
Reading BPM data. Max voltages (uncalibrated):
Ch. 1D is non-functional. Averaging over channels A,B,C
  BPM # 1 1.095703 1.203125 1.117187 1.11718766667
 BPM bkgd # 1 0.107422 0.021484 0.021484 0.0286456666667
  BPM # 2 0.644531 0.923828 0.661719 0.790625
 BPM bkgd # 2 0.021484 0.279297 0.017188 0.008594
  BPM # 3 0.75625 0.764844 0.816406 0.799219
 BPM bkgd # 3 0.042969 0.042969 0.008594 0.025781
  BPM # 4 0.120313 0.105273 0.113867 0.113867
 BPM bkgd # 4 0.012891 0.021484 0.023633 0.015039
  BPM # 5 0.005586 0.002578 0.004297 0.005156
 BPM bkgd # 5 0.004297 0.002578 0.003008 0.005156
  BPM # 6 0.003867 0.003867 0.000859 0.006875
 BPM bkgd # 6 0.001719 0.001719 -0.00043 0.005586

Align: [30, [5, -12, 7, 1, -3, -7]]
Calibration (atten.): [0.1, 0.1, 0.1, 0.5, 10.0, 10.0]
Signal calc domain: [[ 1.9  3.6]
 [ 2.4  4.8]
 [ 2.7  5.2]
 [ 3.6  6.2]
 [ 4.2  6.5]
 [ 5.   7. ]]
Plotting region with lambda > 0.4 *lambda_max

BPM # 1 ----------------
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
percentfit = 15.8359726621
[ 1.9  3.6] 11.998 1.9 3.6 851
Time align shift by -6.0 ns
Peak voltage [mV], current [mA], charge [nC] on buttons
  A: 109.982239659 2.20133269459 0.142590765844
  B: 120.276619197 2.40958656743 0.169027463957
  C: 110.259207679 2.20632455653 0.144269209505
  D: 111.644982295 2.23529394716 0.149906640444
2.55 3.606 529
centroid (mm), aperture (mm): ( 1.71, -0.04 ), 0.77

BPM # 2 ----------------
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
percentfit = 20.003333889
[ 2.4  4.8] 11.998 2.4 4.8 1200
Time align shift by 4.0 ns
Peak voltage [mV], current [mA], charge [nC] on buttons
  A: 65.3836906192 1.3100036913 0.0689352066539
  B: 81.4028666947 1.63081172475 0.265286553141
  C: 65.9346380687 1.32120912216 0.158431933687
  D: 77.4065547573 1.55087211981 0.16784469477
3.06 3.796 369
centroid (mm), aperture (mm): ( -3.45, -16.85 ), 9.83

BPM # 3 ----------------
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
percentfit = 22.5037506251
[ 2.7  5.2] 11.998 2.7 5.2 1251
Time align shift by 24.0 ns
Peak voltage [mV], current [mA], charge [nC] on buttons
  A: 70.8902804925 1.42765750347 0.104231361966
  B: 71.5756191133 1.43355089526 0.125984286922
  C: 80.0991556565 1.60533254417 0.139778089856
  D: 77.8915139004 1.55825571311 0.146350294625
3.688 4.322 318
centroid (mm), aperture (mm): ( 0.10, 1.90 ), 4.24

BPM # 4 ----------------
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
percentfit = 30.0050008335
[ 3.6  6.2] 11.998 3.6 6.2 1300
Time align shift by 16.0 ns
Peak voltage [mV], current [mA], charge [nC] on buttons
  A: 53.6584190917 1.0768051696 0.127568810401
  B: 49.530638706 0.993548273316 0.117797612958
  C: 49.4994604597 0.99212093082 0.125697477161
  D: 51.9124425423 1.03939616691 0.135170285462
4.38 5.176 399
centroid (mm), aperture (mm): ( -1.00, 3.27 ), 2.61

BPM # 5 ----------------
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
percentfit = 35.0058343057
[ 4.2  6.5] 11.998 4.2 6.5 1150
Time align shift by -14.0 ns
Peak voltage [mV], current [mA], charge [nC] on buttons
  A: 35.5062850723 0.711685355734 0.106787429117
  B: 26.4599035563 0.532105481572 0.0933567075333
  C: 35.8017029949 0.719262812204 0.0822669093741
  D: 32.6846721664 0.654963318047 0.101222045954
4.952 5.502 276
centroid (mm), aperture (mm): ( 4.24, 5.83 ), 5.18

BPM # 6 ----------------
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
percentfit = 41.6736122687
[ 5.  7.] 11.998 5.0 7.0 1000
Time align shift by 14.0 ns
Peak voltage [mV], current [mA], charge [nC] on buttons
  A: 40.3575196781 0.810855601343 0.119846378376
  B: 39.7067647729 0.797154765151 0.110876302377
  C: 37.8581636747 0.759878885334 0.119446658032
  D: 36.8110798861 0.737475564216 0.124130264077
5.482 6.264 392
centroid (mm), aperture (mm): ( -3.42, 1.65 ), 2.64

------------------
Dynamic aperture centroid (x,y) and radius [mm]: ( -0.52, -1.70 ), 9.44
BPM position [m], line charge densities [nC/m], FWHM charge [nC]:
 1.276      27.14      67.50
 2.393      25.72      48.91
 3.511      21.70      31.43
 4.908      24.12      41.37
 6.026      18.59      20.93
 7.143      22.57      37.36
=====================================
Isol measure  Mon Dec  2 15:16:34 2013
=====================================

Reading Isol data
Collecting 26 device channels
Opening 3464613780.pkl
=====================================
Faraday cup measure  Mon Dec  2 15:16:34 2013
=====================================

Reading Faraday cup data
Collecting 26 device channels
Opening 3464613780.pkl
Sampling time interval : 2.0 ns
Sampling critical frequency : 250.0 MHz
Convolution window cutoff 50.0 MHz
Charge in FWHM (nC) = 20.9055371798
Average beam velocity, beta =  0.00626143152796
Equivalent KE =  126.741657368  keV
TOF data: z[m], t[usec], FWHM[usec]
0.272  0.35 +- 1e-02  1.010 +- 6e-04
0.716  0.58 +- 6e-04  0.978 +- 6e-04
1.276  0.88 +- 6e-04  0.946 +- 8e-04
1.554  1.00 +- 6e-04  0.762 +- 8e-04
1.833  1.14 +- 6e-04  0.710 +- 8e-04
2.393  1.41 +- 6e-04  0.688 +- 8e-04
2.672  1.59 +- 6e-04  0.710 +- 8e-04
2.951  1.74 +- 6e-04  0.658 +- 8e-04
3.511  2.06 +- 6e-04  0.562 +- 8e-04
3.789  2.18 +- 6e-04  0.712 +- 8e-04
4.348  2.47 +- 6e-04  0.678 +- 8e-04
4.908  2.74 +- 6e-04  0.702 +- 8e-04
5.466  2.98 +- 6e-04  0.680 +- 8e-04
6.026  3.29 +- 6e-04  0.486 +- 8e-04
6.304  3.41 +- 6e-04  0.680 +- 8e-04
6.863  3.76 +- 6e-04  0.678 +- 8e-04
7.143  3.86 +- 6e-04  0.670 +- 8e-04
7.701  4.21 +- 6e-04  0.652 +- 8e-04
8.725  4.65 +- 6e-04  0.642 +- 8e-04
TOF interpolation stitched at z = 6.03 m

Timing residual errors:
  z [m]    tmon-fit [ns]
  0.272          0
  0.716         -0
  1.276          9
  1.554        -17
  1.833        -24
  2.393        -42
  2.672         -1
  2.951          4
  3.511         38
  3.789         13
  4.348         20
  4.908         12
  5.466        -33
  6.026        -13
  6.304        -28
  6.863         34
  7.143         -8
  7.701         50
  8.725        -35


Monitor#  Position [m]  time [us]  beta  KE [kV] from spline:
    0        0.272        0.346       6.35e-03 +- 3.7e-04      130.4 +- 15.2
    1        0.716        0.580       6.36e-03 +- 1.6e-04      130.7 +-  6.6
    2        1.276        0.873       6.28e-03 +- 5.7e-05      127.5 +-  2.3
    3        1.554        1.017       6.20e-03 +- 9.4e-05      124.3 +-  3.8
    4        1.833        1.162       6.22e-03 +- 5.9e-05      125.0 +-  2.4
    5        2.393        1.452       6.25e-03 +- 5.3e-05      126.5 +-  2.1
    6        2.672        1.595       6.27e-03 +- 7.3e-05      127.2 +-  2.9
    7        2.951        1.738       6.29e-03 +- 5.2e-05      128.0 +-  2.1
    8        3.511        2.024       6.33e-03 +- 5.5e-05      129.5 +-  2.3
    9        3.789        2.165       6.35e-03 +- 5.9e-05      130.3 +-  2.4
   10        4.348        2.448       6.39e-03 +- 4.3e-05      131.8 +-  1.8
   11        4.908        2.730       6.42e-03 +- 4.8e-05      133.4 +-  2.0
   12        5.466        3.009       6.46e-03 +- 4.4e-05      135.0 +-  1.9
   13        6.026        3.299       6.50e-03 +- 5.5e-05      136.7 +-  2.3
   14        6.304        3.442       6.50e-03 +- 5.1e-05      136.7 +-  2.1
   15        6.863        3.728       6.50e-03 +- 5.4e-05      136.7 +-  2.3
   16        7.143        3.872       6.50e-03 +- 5.4e-05      136.7 +-  2.3
   17        7.701        4.158       6.50e-03 +- 3.1e-05      136.7 +-  1.3
   18        8.725        4.683       6.50e-03 +- 5.5e-05      136.7 +-  2.3


Position [m]    Ipk [mA]    Qfwhm [nC]    FWHM [us]    Qfwhm/FWHM [mA]:
   0.27           55.00        42.89         0.98          43.86
   1.28           47.00        38.99         0.95          41.22
   1.55           46.93        32.47         0.76          42.61
   1.83           46.89        30.30         0.71          42.68
   2.39           44.37        28.14         0.69          40.91
   2.67           49.68        31.31         0.71          44.10
   2.95           46.64        27.57         0.66          41.90
   3.51           37.87        18.30         0.56          32.56
   3.79           39.71        24.14         0.71          33.90
   4.35           39.26        22.65         0.68          33.41
   4.91           42.75        24.45         0.70          34.83
   5.47           40.42        22.96         0.68          33.77
   6.03           33.34        12.52         0.49          25.76
   6.30           40.38        22.23         0.68          32.69
   6.86           39.01        21.36         0.68          31.51
   7.14           40.47        22.35         0.67          33.36
   7.70           37.47        19.76         0.65          30.30
   8.72           40.13        20.91         0.64          32.56


Gap voltage over beam FWHM:
Cell  1 Voltage: [  -0.2   -0.3   -0.3] kV
Cell  2 Voltage: [  -0.2    0.1   -0.2] kV
Cell  6 Voltage: [   0.2    0.1    0.2] kV
Cell 10 Voltage: [   0.2   -0.1   -0.1] kV
Cell 15 Voltage: [   0.1    0.1    0.1] kV
Cell 17 Voltage: [  -0.2   -0.2    0.2] kV
Cell 19 Voltage: [  -0.0    0.1    0.2] kV
 1.28  0.46  6.28e-03   47.16  -1.67  -0.03
 1.55  0.47  6.20e-03   46.94  -0.13  -0.00
 1.83  0.48  6.22e-03   46.99   0.35   0.01
 2.39  0.50  6.25e-03   46.75  -1.92  -0.04
 2.67  0.51  6.27e-03   45.86  -4.66  -0.10
 2.95  0.52  6.29e-03   44.21  -6.81  -0.15
 3.51  0.53  6.33e-03   40.65  -4.38  -0.11
 3.79  0.54  6.35e-03   39.86  -1.53  -0.04
 4.35  0.56  6.39e-03   39.80   0.52   0.01
 4.91  0.57  6.42e-03   39.87  -0.48  -0.01
 5.47  0.59  6.46e-03   39.05  -2.65  -0.08
 6.03  0.60  6.50e-03   37.54  -1.44  -0.04
 6.30  0.61  6.50e-03   37.48   0.95   0.03
 6.86  0.62  6.50e-03   38.66   2.23   0.07
 7.14  0.62  6.50e-03   39.08   0.51   0.02
 7.70  0.63  6.50e-03   38.47  -1.82  -0.06
 8.72  0.64  6.50e-03   39.98   7.76   0.24

Sol#  Bpk[T]  I[mA]    beta      B@const-R   kcycl[/m]:
  0    1.14    47.7  6.14e-03      0.75        6.32
  1    0.96    45.4  6.16e-03      0.73        5.47
  2    0.73    45.3  6.17e-03      0.73        4.17
  3    0.73    46.4  6.19e-03      0.74        4.12
  4    0.72    47.7  6.21e-03      0.74        4.09
  5    0.73    48.6  6.23e-03      0.75        4.09
  6    0.73    48.7  6.25e-03      0.75        4.08
  7    0.72    48.0  6.26e-03      0.74        4.01
  8    0.70    46.7  6.28e-03      0.73        3.90
  9    0.68    45.1  6.30e-03      0.72        3.78
 10    0.68    43.4  6.32e-03      0.70        3.75
 11    0.66    42.0  6.34e-03      0.69        3.66
 12    0.66    40.9  6.36e-03      0.68        3.61
 13    0.64    40.2  6.38e-03      0.67        3.53
 14    0.62    39.9  6.40e-03      0.67        3.42
 15    0.62    39.7  6.41e-03      0.67        3.38
 16    0.62    39.8  6.43e-03      0.67        3.36
 17    0.61    39.8  6.45e-03      0.67        3.29
 18    0.62    39.9  6.47e-03      0.67        3.33
 19    0.61    39.9  6.49e-03      0.67        3.31
 20    0.61    39.7  6.50e-03      0.66        3.27
 21    0.60    39.5  6.50e-03      0.66        3.23
 22    0.59    39.2  6.50e-03      0.66        3.18
 23    0.59    38.9  6.50e-03      0.66        3.14
 24    0.48    38.5  6.50e-03      0.65        2.58
 25    0.47    38.2  6.50e-03      0.65        2.52
 26    0.47    38.0  6.50e-03      0.65        2.61
 27    1.16    38.2  6.50e-03      0.65        5.98

Solenoid field at emitter = 21.62 G
Solenoid field at target (z = 8.72 m) = 17.34 G

Saving plots in plots.pdf

 ---- fin ----

=====================================
      Mon Dec  2 15:17:04 2013
=====================================
