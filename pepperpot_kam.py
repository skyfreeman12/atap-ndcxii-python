'''
Kieran Murphy, Steve Lidia
Created on Feb 6, 2013


To execute this:
Use FileCruncher.py and call the crunch routine

2013-10-11 updated to python3, imported into git. See git history for changes. Arun Persaud

'''
import readSPE
import numpy as np
import scipy as sp
from scipy import ndimage
from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion
from matplotlib import pyplot as plt

# If running from command line, and all you want is one file, uncomment next line and put in it the file location
cmd_line = 1
fileLoc = ["./130508/images/130508035.SPE", \
           "./130508/images/130508036.SPE", \
           "./130508/images/130508037.SPE"]

bkgdFileLocs = ["./130508/images/130508050.SPE",\
                "./130508/images/130508051.SPE",\
                "./130508/images/130508052.SPE"]

def display_img(img):
    img = np.transpose(img)
    plt.imshow(img)
    plt.show()
    return 0
    # We don't need to tranpose back since the method takes the value only, so changing it won't change the variable
    # at the level above, where we're calling the method

def crunch(filelist, bkgdfilelist):
    # Open the file
    # 1302040095
    # 1304010065
    # ../../../Desktop/1304010065.SPE

    #filename = sys.argv[1]

    filename = filelist[0]
    img = readSPE.load(filename)

    if len(filelist)>1:
        for fl in filelist[1:]:img = img + readSPE.load(fl)
    img = img/len(filelist)
    display_img(img)

    bkgdfilename = bkgdfilelist[0]
    bkgdImg = readSPE.load(bkgdfilename)

    if len(bkgdfilelist)>1:
        for fl in bkgdfilelist[1:]:bkgdImg = bkgdImg + readSPE.load(fl)
    bkgdImg = bkgdImg/len(bkgdfilelist)
    display_img(bkgdImg)

#    bkgdImg1 = readSPE.load(bkgdfilelist[0])
#    bkgdImg2 = readSPE.load(bkgdfilelist[1])
#    bkgdImg = (bkgdImg1+bkgdImg2)/2.0
#    bkgdImg = bkgdImg/len(bkgdfilelist)

    # subtract bkgd img, apply median filter to image
    img = (img - bkgdImg)*((img - bkgdImg)>0)
    img = ndimage.median_filter(img,1)
    display_img(img)

    print('### Bkgd mean and st dev. =',np.mean(bkgdImg), np.std(bkgdImg))
    print('### Image mean and std =',np.mean(img), np.std(img))
    print()

    '''
    IMPORTANT
    Python (or numpy, or whatever we are using) counts, in its arrays, with the row as the first coordinate.
    To get around accessing arrays with [y,x] always, we'll instead transpose the image at the beginning of processing

    To display, call the display_img routine, which tranposes the array to display, calls the plt display methods,
    and tranposes again.
    '''
    img = np.transpose(img);

    '''
    This program takes an image of the ion beam after passing through the pepperpot.
    It finds the 'blobs' which are the mini beamlets that passed through each hole in the Ppot
    by 1) finding all pixel values N standard deviations above the image mean, 2) joining
    contiguous groups of high pixels together, and 3) only keeping those blobs bigger than or equal to
    sizeMinimum pixels.

    After this, various processing is done:
    1) Center of Mass for each blob is found and stored in the blobCenters array
    2) Gaussian fitting - each blob is fit to a 2D gaussian with the mean at the center of mass
    and the covariance matrix is found from the distribution.  The eigenvalues and eigenvectors of the
    covariance matrix are found and used to plot the semimajor and semiminor axes of the ellipses that are
    the isocontours of the best-fit gaussians.  This could be used later in analysis of each beamlet distribution
    after traversing the distance from the Ppot to the scintillator.
    3) Grid fitting - a grid of modeled after the Ppot with a width of gridSpacing pixels is laid down and the closest
    grid point to each blob is found.  The grid is then shifted by the average displacement from grid point to blob,
    ostensibly finding the best fit grid
    4) NOT DONE - Phase space projections - the distributions of beamlets wrt grid holes are projected together for each
    x and y value and plotted to show a rough sketch of the x-x' and y-y' phase spaces
    5) NOT DONE - the blocked beamlet distributions in the upper left diagonal of the Ppot are interpolated by averaging

    TTD
    Take a background image and subtract it before anything else.
    Find net rotation of blobs and compare with 1.5 degree estimate.
    Calculate emittance with formula on half sheet of paper, weighting blob centers by their masses.
    Vary threshold used to cut off blobs to include more of the less intense blobs.
    Use long exposure image to place grid on image, so that then we don't have to use an arbitrary grid and fit it.
    Median Filter because Steve likes it.
    '''
    N = 3.0 #num stds above mean for pixel to be considered

    pix_to_mm = 1/4.67 # number of millimeters in a pixel
    hole_spacing = 3.81 # mm
    gridSpacing = hole_spacing/pix_to_mm # pixels between holes in the pepperpot, 3.81 mm btwn holes
    pix_per_grid = int(gridSpacing)**2

    sizeMinimum = max(9,pix_per_grid/30) # minimum number of pixels per blob
    sizeMaximum = pix_per_grid
    sizex = np.size(img[0])
    sizey = np.size(img)/sizex

    drift_distance = 17.5 # distance in mm from pPot to scintillator
    img_rotation = 0.0 # number of degrees to rotate image due to mask misalignment (in CW direction)
    img_rotation = img_rotation*np.pi/180 # convert it to radians so we can use np.cos()

    # Display toggles
    show_blobs = 1
    show_gaussfit = 0
    show_dispVec = 1
    show_phase_space = 1

    mean = np.mean(img)
    std = np.std(img)
    img = (img-mean)*((img-mean)>(N*std))

    print('### Bkgd subtracted image min, max, mean and std =',img.min(),img.max(),mean,std)
    print()

    # peak detection
    peak_filter = maximum_filter(img,size=(int(gridSpacing)/2,int(gridSpacing)/2))
    img_peak = img * (peak_filter>0)
    if (show_blobs):
        display_img(peak_filter)

    # Sobel filter
    img_sobel = (np.abs(ndimage.sobel(img))>0)*img
    img_sobel = img_sobel*(img_sobel>0.10*img_sobel.max())

    if (show_blobs):display_img(img_sobel)

    peak_filter = maximum_filter(img_sobel,size=(int(0.5*gridSpacing),int(0.5*gridSpacing)))
    img_peak = img * (peak_filter>0)

    if (show_blobs):display_img(img_peak)

    # peak labeling
    peak_label, num_peaks = ndimage.measurements.label(peak_filter)

    max_peak = img_peak.max()
    max_peak_mass = np.array([(img_peak*(peak_label == ii+1)).sum() for ii in range(num_peaks)]).max()
    print('### Peak maximum, max peak mass =',max_peak,max_peak_mass)
    print()

    img_keep = np.ndarray(shape = (sizex, sizey))
    for ii in range(num_peaks):
        solo_peak = img_peak*(peak_label == ii+1)
        num_pixels = (peak_label == ii+1).sum()

        xind,yind = np.ndarray.nonzero(solo_peak)
        xyind = list(zip(xind,yind))

        accept = (solo_peak.max()>0.05*max_peak)*(solo_peak.sum()>0.05*max_peak_mass)* \
                 (len(xind)>=sizeMinimum)
        if accept:
            img_keep += solo_peak  # layer solo peaks into new composite image
            # 2nd order moments
            xmean, ymean = ndimage.measurements.center_of_mass(solo_peak)
            x2mean = np.array([(i-xmean)**2*solo_peak[i,j] for (i,j) in xyind]).sum()/solo_peak.sum()
            y2mean = np.array([(j-ymean)**2*solo_peak[i,j] for (i,j) in xyind]).sum()/solo_peak.sum()
            xymean = np.array([(i-xmean)*(j-ymean)*solo_peak[i,j] for (i,j) in xyind]).sum()/solo_peak.sum()

            solo_mass = ndimage.measurements.sum(img_peak,labels=peak_label,index=ii+1)
#            print ii+1, num_pixels, num_pixels/(0.5**2*pix_per_grid),\
#                  ndimage.measurements.maximum(img_peak,labels=peak_label,index=ii+1),\
#                  ndimage.measurements.sum(img_peak,labels=peak_label,index=ii+1),\
#                  ndimage.measurements.maximum_position(img_peak,labels=peak_label,index=ii+1),\
#                  ndimage.measurements.center_of_mass(img_peak,labels=peak_label,index=ii+1)
    print()
    #
    total_mass = img_keep.sum()
#    display_img(img_keep)

    img = img_keep

    peak_filter = maximum_filter(img,size=(int(0.5*gridSpacing),int(0.5*gridSpacing)))
    peak_label, num_peaks = ndimage.measurements.label(peak_filter)

    print('### Num peaks =',num_peaks)
    print()

    # Then find all points that are N standard deviations above the mean
    highpts = np.ndarray(shape = (sizex, sizey))
    threshold = N*std
    for x in range(sizex):
        for y in range(sizey):
            pixVal = img[x,y]
            if (pixVal > threshold):
                highpts[x,y] = pixVal
            else:
                highpts[x,y] = 0

    '''
     Use blob connection to collect touching points into blobs
    '''
    blobList = [[0,0]]
    blobLocs = np.ndarray(shape = (sizex, sizey))
    blobNum = 1

    for i in range(sizex-2): # to skip the edges
        x = i+1
        for j in range(sizey-2):
            y = j+1
            bright = (highpts[x,y]>0) #if it's colored
            if bright:
                if (blobLocs[x-1,y]>0 and blobLocs[x, y-1]>0): # both prev adj pixels are bright
                    if (blobLocs[x-1,y] == blobLocs[x,y-1]): # they are the same blob, so this pix is too
                        tempBlobNum = int(blobLocs[x-1,y])
                        blobLocs[x,y] = tempBlobNum
                        blobList[tempBlobNum].append((x, y))
                    else: # they're marked as different blobs but they should be the same
                        # make both the current pixel and the upper one part of the left pixel's blob, and
                        # run a recursive routine to correct any blob pixels the upper pixel is touching
                        tempBlobNum = int(blobLocs[x-1,y])
                        blobLocs[x,y] = tempBlobNum
                        blobList[tempBlobNum].append((x, y))
                        erroneousBlob = int(blobLocs[x,y-1])
                        for (x, y) in blobList[erroneousBlob]:
                            blobList[tempBlobNum].append((x, y))
                            blobLocs[x,y] = tempBlobNum
                        blobList[erroneousBlob] = []
                        #findAndFix(blobLocs, x, y-1)

                elif (blobLocs[x-1,y]>0):
                    tempBlobNum = int(blobLocs[x-1,y])
                    blobLocs[x,y] = tempBlobNum
                    blobList[tempBlobNum].append((x, y))
                elif (blobLocs[x,y-1]>0):
                    tempBlobNum = int(blobLocs[x,y-1])
                    blobLocs[x,y] = tempBlobNum
                    blobList[tempBlobNum].append((x, y))
                else: # neither adjacent pix is yored, so assign it to the next open blobNum
                    blobLocs[x,y] = blobNum
                    blobList.append([(x, y)])
                    blobNum += 1
    blobList[0] = []

    '''
    Throw out blobs composed of fewer than sizeMinimum pixels
    '''
    finalBlobList = []
    finalBlobLocs = np.ndarray(shape = (sizex, sizey))

    currentBlobNum = 1
    for blob in blobList:
        if (len(blob) >= sizeMinimum):
            # transfer it to the new bloblist and blobLocs
            finalBlobList.append(blob)
            for (x, y) in blob:
                finalBlobLocs[x,y] = currentBlobNum
            currentBlobNum += 1

    numBlobs = len(finalBlobList)

    if show_blobs:display_img(finalBlobLocs)

    print("Number of blobs detected: " + str(numBlobs))

    '''
    Find the center of mass of each blob and of the beam as a whole
    '''
    blobCenters = []
    blobMasses = []
    beamCenter = [0,0]
    beamMass = 0

    for blob in finalBlobList:
        centerX = centerY = 0
        totalMass = 0
        for (x,y) in blob:
            mass = img[x,y]
            centerX += x*mass
            centerY += y*mass
            totalMass += mass
        beamCenter[0] += centerX
        beamCenter[1] += centerY
        beamMass += totalMass
        centerX = centerX/totalMass
        centerY = centerY/totalMass
        blobCenters.append((centerX, centerY))
        blobMasses.append(totalMass)
    beamCenter = [x/beamMass for x in beamCenter]
    print("Beam is centered at pixel ("+str(beamCenter[0])+","+str(beamCenter[1])+").")
    print("Total mass = " + str(beamMass))
    # Draw a block over each blob to check
    centers = np.ndarray(shape = (sizex, sizey))
    for blobCent in blobCenters:
        x = int(blobCent[0])-3
        y = int(blobCent[1])-3
        for i in range(7):
            for j in range(7):
                if (x+i>0 and x+i<512 and y+j>0 and y+j<512):
                    centers[x+i,y+j] = 7
    if (show_blobs):display_img(centers)
    """
    ######## GAUSSIAN FIT ###########
    # For each blob, fit a 2D gaussian, and return its mu and covariance matrix
    """
    covarianceMats = np.ndarray(shape = (2,2,numBlobs)) # each cov matrix is 2x2, and there are numBlobs gaussians
    eigvecs = np.ndarray(shape = (2, 2, numBlobs))
    eigvals = np.ndarray(shape = (2, numBlobs))
    # we already have the centers
    covmat = np.ndarray(shape = (2,2))
    for blobNum in range(len(finalBlobList)):
        covmat[0,0] = covmat[0,1] = covmat[1,0] = covmat[1,1] = 0
        blobMass = 0
        for (x,y) in finalBlobList[blobNum]:
            intensity = img[x,y]
            blobMass += intensity
            covmat[0,0] += intensity*x*x
            covmat[0,1] += intensity*x*y
            covmat[1,1] += intensity*y*y
        covmat = covmat/blobMass
        centX = blobCenters[blobNum][0]
        centY = blobCenters[blobNum][1]
        covmat[0,0] -= centX**2
        covmat[1,1] -= centY**2
        covmat[0,1] -= centX*centY
        covmat[1,0] = covmat[0,1]
        covarianceMats[:,:,blobNum] = covmat
        eigvals[:,blobNum], eigvecs[:,:,blobNum] = np.linalg.eig(covmat)
        direction0 = eigvecs[:,0,blobNum]
        direction1 = eigvecs[:,1,blobNum]
        eigval0 = eigvals[0,blobNum]
        eigval1 = eigvals[1,blobNum]
        for i in range(15):
            #Divide by sqrt(eigvals) to make axes their proper lengths
            x0 = centX+direction0[0]*i*(abs(eigval0)**0.5)
            y0 = centY+direction0[1]*i*(abs(eigval0)**0.5)
            x1 = centX+direction1[0]*i*(abs(eigval1)**0.5)
            y1 = centY+direction1[1]*i*(abs(eigval1)**0.5)
            if (x0>0 and x0<512 and y0>0 and y0<512):
                finalBlobLocs[x0, y0] = 3
            if (x1>0 and x1<512 and y1>0 and y1<512):
                finalBlobLocs[x1, y1] = 3
    if show_gaussfit:display_img(finalBlobLocs)

    """
    ########## GRID FITTING ################
    """
#    Create the grid
    numGridRows = int(np.ceil(sizey/(gridSpacing)))

    blobsLeft = list(range(numBlobs)) # To keep track of what's been taken care of
    gridUsage = np.ndarray(shape = (numGridRows, numGridRows))
    gridUsage[:,:] = -1 # -1 means we haven't used the grid pt yet
    gridLocsByBlob = np.ndarray(shape = (numBlobs,2)) # Grid locations indexed by blob number
    displacements = np.ndarray(shape = (numBlobs,2))
    DIRECTION_LIST = [(1,0),(0,1),(-1,0),(0,-1)]
    unitVecX = [np.cos(img_rotation), np.sin(img_rotation)]
    unitVecY = [np.sin(img_rotation), np.cos(img_rotation)]

#    Ok, we place a grid point at the brightest blob
    brightestBlobNum = np.argmax(blobMasses)
    brightestBlobLoc = blobCenters[brightestBlobNum]
    middleOfGrid = (int(numGridRows/2), int(numGridRows/2))
    gridUsage[middleOfGrid[0], middleOfGrid[1]] = brightestBlobNum

    TTDlist = [middleOfGrid]
    blobsLeft.remove(brightestBlobNum)
    displacements[brightestBlobNum] = [0,0]
    gridLocsByBlob[brightestBlobNum] = brightestBlobLoc
    # Matches a blob with a given grid location, using some of the current state variables defined above
    # Returns true when there is a match and false otherwise, so we know when to stop the looping
    maxDisplacement = 0.75*gridSpacing
    def blob_grid_interloper(gridPoint):
        (GridX, GridY) = gridPoint
#        print gridPoint
        if ((gridUsage[GridX, GridY] < 0) and blobsLeft):
            gridImgCoordinates = np.add(brightestBlobLoc,
                                 np.add(np.multiply((GridX-int(middleOfGrid[0]))*gridSpacing,unitVecX),
                                 np.multiply((GridY-int(middleOfGrid[1]))*gridSpacing,unitVecY)))
            # draw the grid points
            for k in range(7):
                xCoord = gridImgCoordinates[0]-3+k
                yCoord = gridImgCoordinates[1]
                if (xCoord>0 and xCoord<512 and yCoord>0 and yCoord<512):
                    centers[xCoord, yCoord] = 100
                xCoord = gridImgCoordinates[0]
                yCoord = gridImgCoordinates[1]-3+k
                if (xCoord>0 and xCoord<512 and yCoord>0 and yCoord<512):
                    centers[xCoord, yCoord] = 100
            distances = [np.linalg.norm(vec) for vec in np.subtract([blobCenters[i] for i in blobsLeft],gridImgCoordinates)]
            minIndex = np.argmin(distances)
            # Check to see if the point is close enough to consider
            if (distances[minIndex] < maxDisplacement):
                matchedBlobNum = blobsLeft[minIndex]
                # Update the state variables
                gridUsage[GridX, GridY] = matchedBlobNum
                displacements[matchedBlobNum] = blobCenters[matchedBlobNum] - gridImgCoordinates
                gridLocsByBlob[matchedBlobNum] = gridImgCoordinates
                blobsLeft.remove(matchedBlobNum)
                return True
            else:
                return False
        else:
            return False

    while (blobsLeft and TTDlist):
        (currX, currY) = TTDlist[0]
        TTDlist = TTDlist[1:] # Remove the grid point from the stack
        for (delX, delY) in DIRECTION_LIST:
            newPoint = (np.add(currX,delX), np.add(currY,delY))
            keep_going = blob_grid_interloper(newPoint)
            if (keep_going):
                TTDlist.append(newPoint)
    print("Blobs we didn't use: " + str(blobsLeft))

    '''
    Important: I modified the later code by not including these blobs - watch out for any areas of the code I missed.
    We also need to alter beamMass so that we can effectively get rid of these unmatched blobs.
    '''
    for i in blobsLeft:
        beamMass -= blobMasses[i]
        numBlobs -= 1

#    Step in all directions one grid space over and find the closest unused blob to that grid point
#    That blob must be less than one grid spacing away from the 'proposed' grid point

#    Go until all blobs are matched up or you can't step over one and satisfy the max blob distance

#    Edge cases to watch out for: 1) a blob is disconnected from everything else
#    2) blobs diverge so that at the edges, the spacing is actually more than one grid space
#    3) infinite looping - be careful to remove a blob and a grid point from consideration



    # Draw the displacement vecs for each blob

    for blobNum in range(numBlobs):
        # Don't display the blobs we couldn't place
        if (not (blobNum in blobsLeft)):
            blobX = blobCenters[blobNum][0]
            blobY = blobCenters[blobNum][1]
            dispVecX = displacements[blobNum][0]
            dispVecY = displacements[blobNum][1]
            for i in range(7):
                centers[int(round(blobX-i*dispVecX/7)),int(round(blobY-i*dispVecY/7))] = 30

    if show_dispVec:
        display_img(centers)

    ### Find the average displacement and subtract it from everything, finding the best fit for the grid with these centers
#    avgDispVec = np.average(displacements,0)
#    displacements = np.subtract(displacements, avgDispVec)
#
#    avgBlobCenter = np.average(blobCenters,0)
#    blobCenters = np.subtract(blobCenters, avgBlobCenter)
## 03/23/1991
    '''
    ############# Emittance Calculation #############
    '''
    xxpr = []
    yypr = []

    xprime_max = np.arctan(hole_spacing/2./drift_distance)*1e3  # physical xprime limit, mrad
    yprime_max = np.arctan(hole_spacing/2./drift_distance)*1e3  # physical xprime limit, mrad

    for x in range(numGridRows):
        for y in range(numGridRows):
            # collapse down all blobs with coord x
            blobNum = gridUsage[x,y]
            if (blobNum >= 0):
            #(blobX, blobY) = blobCenters[blobNum]
                disp = displacements[blobNum] # this is in pixels, so we need to convert to mm
                disp = disp*pix_to_mm/drift_distance*1000 # this gives transverse disp over longitudinal disp, or Vx/Vz
                # The factor of 1000 is to convert it to millirad
                xprime = np.dot(disp,unitVecX)
                yprime = np.dot(disp,unitVecY)
                if (abs(xprime) < xprime_max)*(abs(yprime) < yprime_max):  # physical cut to phase space
                    xxpr.append(((x*gridSpacing-beamCenter[0])*pix_to_mm,xprime,int(blobNum)))
                    yypr.append(((y*gridSpacing-beamCenter[1])*pix_to_mm,yprime,int(blobNum)))

    numBlobs = len(xxpr)

    '''
    We have an option above: use x and y as defined by the grid, or by image
    For first, we need to convert the displacement into new basis.
    For second, we convert the coordinates, and don't get nice up and down lines of data in phase space
    (instead they'd be diagonal, which I think would suggest a better set of coordinates).
    '''
    if show_phase_space:

        ### Plot x-x'
        fig = plt.figure()
        ax = fig.add_subplot(111)
        [xVals, xprVals, blobIDs] = list(zip(*xxpr))
        ax.scatter(xVals,xprVals,s=[blobMasses[blobNum]/100 for blobNum in blobIDs]) # plots the dots' areas as proportional to the blob mass
        ax.set_title('x-x\' phase space projection')
        ax.set_xlabel('x (mm)')
        ax.set_ylabel('x\' (millirad)')
        plt.show()

        ### Plt y-y'
        fig = plt.figure()
        ax = fig.add_subplot(111)
        [yVals, yprVals, blobIDs] = list(zip(*yypr))
        ax.scatter(yVals,yprVals,s=[blobMasses[blobNum]/100 for blobNum in blobIDs])
        ax.set_title('y-y\' phase space projection')
        ax.set_xlabel('y (mm)')
        ax.set_ylabel('y\' (millirad)')
        plt.show()

    '''
    To check the two methods, find the angle of convergence by 1) the angle of the best linear fit to the x-x' and y-y'
    phase space, and 2) dotting each displacement with the unit vector in the direction of the center of mass of the whole beam
    and dividing by the radial distance
    '''

    ### Find the slope of the best fit line, using least squares fit stuff
    # first for x-x'
    sumXXpr = sumXsq = sumX = sumXpr = 0
    for i in range(numBlobs):
        mass = blobMasses[xxpr[i][2]]
        x = xxpr[i][0]
        xpr = xxpr[i][1]
        sumXXpr += x*xpr*mass
        sumXsq += x*x*mass
        sumX += x*mass
        sumXpr += xpr*mass
    try:
        slopeX = (sumXXpr*beamMass-sumX*sumXpr)/(sumXsq*beamMass-sumX*sumX)
    except:
        slopeX = 100.0
    xprAvg = sumXpr/beamMass
    xAvg = sumX/beamMass
    intercept = xprAvg - slopeX*xAvg
    print("The best fit line through the x-x' phase space has (slope,intercept)=("+str(slopeX)+","+str(intercept)+").")

    # now y-y'
    sumYYpr = sumYsq = sumY = sumYpr = 0
    for i in range(numBlobs):
        mass = blobMasses[yypr[i][2]]
        y = yypr[i][0]
        ypr = yypr[i][1]
        sumYYpr += y*ypr*mass
        sumYsq += y*y*mass
        sumY += y*mass
        sumYpr += ypr*mass

    try:
        slopeY = (beamMass*sumYYpr-sumY*sumYpr)/(beamMass*sumYsq-sumY*sumY)
    except:
        slopeY = 100.0
    yprAvg = sumYpr/beamMass
    yAvg = sumY/beamMass
    intercept = yprAvg - slopeY*yAvg
    print("The best fit line through the y-y' phase space has (slope,intercept)=("+str(slopeY)+","+str(intercept)+").")

    ### Find the average radial displacement by taking beam center (to define radial direction) and then dotting radial
    ### direction with each displacement vector, then summing
    radialDisp = 0
    for blob in range(numBlobs):
        if (not (blobNum in blobsLeft)):
            disp = displacements[blob]
            loc = gridLocsByBlob[blob]
            radialDir = (loc - beamCenter)/np.linalg.norm(loc-beamCenter) # makes it a unit vector
            radialDisp += np.dot(disp, radialDir)
    radialDisp = radialDisp*pix_to_mm/drift_distance*1000
    print("Average radial displacement, with positive outward and negative inward, is " + str(radialDisp/(numBlobs-len(blobsLeft)))+" mrad.")

    ### Calculate the emittance
    # Based on Steve's formula, em = 4 sqrt(variance of x * variance of x' - expected value of x*x')
    ### Important - this is based on the grid coordinates, not those of the image

    expX = expX2 = expXpr = expXpr2= expXXpr = 0

    for i in range(numBlobs):
        if (not (blobNum in blobsLeft)):
            mass = blobMasses[i]
            x = xxpr[i][0]
            xpr = xxpr[i][1]
            expX += x*mass
            expXpr += xpr*mass
    expX = expX/beamMass
    expXpr = expXpr/beamMass

    for i in range(numBlobs):
        if (not (blobNum in blobsLeft)):
            mass = blobMasses[i]
            x = xxpr[i][0]
            xpr = xxpr[i][1]
            expX2 += mass*(x-expX)**2
            expXpr2 += mass*(xpr-expXpr)**2
            expXXpr += mass*(x-expX)*(xpr-expXpr)

    expX2 = expX2/beamMass
    expXpr2 = expXpr2/beamMass
    expXXpr = expXXpr/beamMass

    varX = expX2
    varXpr = expXpr2

    print("VarX is "+str(varX))
    print("VarXpr is "+str(varXpr))
    print("<XXpr> is "+str(expXXpr))
    emittanceX = 4*np.sqrt(varX*varXpr - expXXpr**2)
    print("The 4rms x-x' emittance is "+str(emittanceX)+ " mm-mrad.")

    ### Now for y-y'
    expY = expY2 = expYpr = expYpr2= expYYpr = 0

    for i in range(numBlobs):
        if (not (blobNum in blobsLeft)):
            mass = blobMasses[i]
            y= yypr[i][0]
            ypr = yypr[i][1]
            expY += y*mass
            expYpr += ypr*mass
    expY = expY/beamMass
    expYpr = expYpr/beamMass

    for i in range(numBlobs):
        if (not (blobNum in blobsLeft)):
            mass = blobMasses[i]
            y = yypr[i][0]
            ypr = yypr[i][1]
            expY2 += mass*(y-expY)**2
            expYpr2 += mass*(ypr-expYpr)**2
            expYYpr += mass*(y-expY)*(ypr-expYpr)

    expY2 = expY2/beamMass
    expYpr2 = expYpr2/beamMass
    expYYpr = expYYpr/beamMass

    varY = expY2
    varYpr = expYpr2

    print("VarY is "+str(varY))
    print("VarYpr is "+str(varYpr))
    print("<YYpr> is "+str(expYYpr))
    emittanceY = 4*np.sqrt(varY*varYpr - expYYpr**2)
    print("The 4rms y-y' emittance is "+str(emittanceY)+ " mm-mrad.")
#    return emittanceX, emittanceY
    return emittanceX, emittanceY, slopeX, slopeY, beamMass, beamCenter[0], beamCenter[1], numBlobs

if (cmd_line):
    crunch(fileLoc, bkgdFileLocs)
