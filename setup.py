from setuptools import setup, Command
from pip._internal.req import parse_requirements
import pip._internal.download
import subprocess
import sys

install_reqs = parse_requirements('requirement.txt', session=pip._internal.download.PipSession())
reqs = [str(ir.req) for ir in install_reqs]

# get git version
try:
    version = subprocess.check_output(['git', 'rev-parse', 'HEAD'])
    version = version.strip().decode('ascii')
    version = "git."+version[0:8]
except FileNotFoundError:
    version = "WARNING: git not available --"
except subprocess.CalledProcessError:
    version = "Warning: git execution error (no git repo?)"

setup(
    name="NDCXII",
    version="0.2+{ver}".format(ver=version),
    packages=['NDCXII'],
    install_requires=reqs,
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    python_requires='>=3.6',
    scripts=['NDCXII-rawdata', 'NDCXII-analyze', 'NDCXII-compare',
             'NDCXII-firstrun', 'NDCXII-shot-monitor', "NDCXII-simulations"],

    author="Arun Persaud",
    author_email="apersaud@lbl.gov",
    description="Scripts to help analyze NDCXII data and automate experiments",
    url="bitbucket.org/berkeleylab/atap-ndcxii-python.git",
)
