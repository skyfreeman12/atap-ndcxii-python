"""envelope.py

Usage:
   envelope [-h|--help] interactive
   envelope [options] scan

Options:
   -h --help                     This text
   --version                     Print current git version
   -p FILE --pickle=FILE         Location of pickle file [results.pkl]
   -q --quiet                    less output
   -c CHARGE --charge=CHARGE     Charge in nC [default: 2.0]
   -n NEUTRAL --neutral=NEUTRAL  Fraction of neutralized beam [default: 1.0]
"""

import numpy as np
import scipy.optimize
from scipy.interpolate import interp1d
from scipy.integrate import odeint
import scipy.constants as SC
from scipy import signal

import pickle
import sys
import docopt

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import matplotlib.patches as mpatches

import NDCXII

plt.ioff()

commands = docopt.docopt(__doc__, version=NDCXII.__version__)
picklefile = commands['--pickle']


def myprint(*args, **kwargs):
    """Only print if --quiet is not set"""
    if not commands['--quiet']:
        print(*args, **kwargs)

# physical parameters
qcharge = SC.e  # proton charge in C
amumass = SC.value('atomic mass constant')  # atomic mass unit in kg
amumc2 = SC.value('atomic mass unit-electron volt relationship')  # atomic mass unit in eV
frpieps0 = 4.*np.pi*SC.epsilon_0  # 4 np.pi eps_0 (in mks units "}, "*, "
clight = SC.c  # speed of light in m/s
clightcgs = SC.c*100  # speed of light in cm/s

# material data
# Aluminum
rhoAl = 2.7  # density of Al g/cm3
soundspeedAl = 500000.  # sound speed in Al cm/s (5000. m/s)
# Gold
rhoAu = 19.3  # density of Au g/cm3
soundspeedAu = 324000.  # sound speed in Au cm/s (3240. m/s)

flagtargetmodel = "Au"  # Al or Au
# beam parameters  #  1.2 MeV Li
ionenergy = 1.2e6  # ion energy in eV
ionmass = 7.0  # ion mass in amu
totalcharge = float(commands['--charge'])*1e-9  # total charge in bunch in C
peakcurrent = 0.10  # peak ion current at end of accelerator in Amp
epsnx = 2.0  # normalized emittance (mm-mrad)
epsnx = 0.5  # normalized emittance (mm-mrad)
# equivalent parabolic pulse duration; total charge = (2/3) peak current*pulseduration
deltavov = -0.087  # head to tail velocity tilt
# equivalent parabolic pulse duration; total charge = (2/3) peak current*pulseduration
deltazp = 0.00184  # rms average of delta v/v before tilt = .5 delta E/E
# lattice parameters

a0 = 2.5
ap0 = 0.0
bsol1 = 0.8  # " T *" "  #, " Lab setting 0.7 *" "
bsol2 = 2.1  # " T *" "  #, " Lab settng 1.9 *"         "
bfinalsol = 6.5  # T    # Lab setting 8.5
rpipe = 4.0  # pipe radius in cm
rangeAl = 2.94e-4  # ion range in Aluminum cm
foilthicknessAl = 2.11e-4  # for 25% energy variation
rangeAu = 1.39e-4  # ion range in Gold cm
foilthicknessAu = 1.0671e-4  # for 25% energy variation
depositedenergyfraction = 5./6.

if flagtargetmodel == "Al":
    targetname = "Aluminum"
elif flagtargetmodel == "Au":
    targetname = "Gold"

myprint("ion mass = ", ionmass)
myprint("target = ", targetname)

# numerical parameters
nslice = 3

# calculated parameters
pulseduration = 1.5*totalcharge/peakcurrent
beta0 = np.sqrt(2.*ionenergy/(ionmass*amumc2))
mylambda = peakcurrent/(beta0*clight)  # line charge density C/m
perv0 = mylambda/(frpieps0*ionenergy)
gfactor = 2.*np.log(rpipe/a0)

# all lengths are in cm
z0 = 0
zsol1start = z0 + 5                 # drift
zsol1end = zsol1start + 20          # solenoid
zsol2start = zsol1end + 10          # drift
zsol2end = zsol2start + 20          # solenoid
znonneutralend = zsol2end + 17      # non-neutral
zdriftend = znonneutralend + 110    # drift
zfinalsolend = zdriftend + 10       # solenoid
zchamberend = zfinalsolend + 50     # target chamber
zfinal = zchamberend

zfinalsol = zfinalsolend - zdriftend
zchamber = zchamberend - zfinalsolend

betatab = np.array([beta0*(1-.5*deltavov), beta0, beta0*(1.+.5*deltavov)])

myprint("beta = ", beta0)
myprint("Delta v / v = ", deltavov)
myprint("Initial perveance = ", perv0)
epsx = epsnx*1e-4/beta0  # cm-rad
myprint("Normalized emittance = ", epsnx, " mm-mrad")
myprint("Unnormalized emittance = ", 1e4*epsx, " mm-mrad")
myprint("gfactor =", gfactor)
myprint("Total charge in bunch = ", totalcharge*1e9, " nC")
myprint("Initial peak current = ", peakcurrent, " A")
myprint("Initial pulse duration = ", 1e9*pulseduration, " ns")
myprint("Initial bunch length = ", pulseduration*beta0*clightcgs, " cm")
myprint("Initial peak line charge density = ", mylambda, " C/m")
z0long = zsol2end
pulsedurationinjector = 500.*1e-9  # Assumed pulse duration of injector.
deltaE = 2*ionenergy*deltazp
kTparallel = 2.*ionenergy*deltazp**2
#     1/2 kTparallel = m deltav**2/2  => kTparallel = m v**2*deltazp**2 = 2 ionenergy*deltazp**2
#                   deltaE=2*ionenergy*deltap/p=2 ionenergy*deltazp
#                => kTparallelinjector=deltaE*pulseduration/pulsedurationinjector
kTperp = ionmass*amumc2*(epsnx*1e-4)**2 / (4*a0**2)            # epsnx = 2 a np.sqrt( kT/mc2)
kTparallelinjector = 2*ionenergy*deltazp*pulseduration/pulsedurationinjector
myprint("delta E (before drift compression) =", deltaE, " eV")
myprint("kTparallel (before drift compression) ~= ", kTparallel, " eV")
myprint("kTperp (before drift compression) ~= ", kTperp, " eV")
myprint("kTparallel (at injector) ~= ", kTparallelinjector, " eV,",
        " assuming dt_injector = ", pulsedurationinjector*1e9, " ns")
l0 = beta0*clightcgs*pulseduration
lp0 = deltavov
epsl = l0*deltazp
# Analytic estimates
niter = 50  # ten
l0iter = l0  # el_0
deltavoviter = deltavov
for iiter in range(niter):
    lnonneutralend = l0iter+deltavoviter*znonneutralend/niter
    deltavovnew = -np.sqrt(deltavoviter**2 - 12.*gfactor*100.*totalcharge /
                           (frpieps0*ionenergy)*(1/lnonneutralend-1/l0iter))
    l0iter = lnonneutralend
    deltavoviter = deltavovnew

myprint("lnonneutralend = ", lnonneutralend, " cm")
deltavovcorr = deltavovnew
deltavovchangenonneut = deltavovcorr-deltavov
ldriftest = -pulseduration*beta0*clightcgs/deltavovcorr
compressionratioestimate = 10.
finalbunchlengthest = epsl*np.sqrt(1-1/compressionratioestimate**2)/(-deltavovcorr)
finalpulsedurationest = finalbunchlengthest/(beta0*clightcgs)
omegacfinal = qcharge*bfinalsol/(ionmass*amumass)  # rad/s
lfocusthinest = (4.*beta0**2*clightcgs**2)/(omegacfinal**2*zfinalsol)
lfocusthickest = (np.pi*beta0*clightcgs)/(2.*omegacfinal/2.)
eta = 0.22  # conversion from tilt to rms spread factor. .22 for parabolic; .29 for flattop
rspotoptest = np.sqrt(-2.*eta*lfocusthickest*deltavovcorr*epsx)
myprint("Rough estimates:")
myprint("Estimated velocity tilt at end of non-neutral =", deltavovcorr)
# myprint("Old Estimated velocity tilt at end of non-neutral =",deltavovcorrold)
myprint("Estimated neutralized drift length = ", ldriftest)
myprint("Estimated neutralized final pulse duration = ", finalpulsedurationest*1e9, " ns")
myprint("Estimated final focal length (thin lens) = ", lfocusthinest, " cm")
myprint("Estimated final focal length (thick lens) = ", lfocusthickest, " cm")
myprint("Estimated optimum spot size =", rspotoptest, " cm")
myprint("Estimated optimum fluence =", 2.*totalcharge*ionenergy/(np.pi*rspotoptest**2), " J/cm**2")
myprint("End rough estimates.")
myprint("length of final focus solenoid = ", zfinalsol, " cm")

zneutralstart = znonneutralend

flagneutralization = 1
if flagneutralization == 1:
    zneutralstart = znonneutralend  # Normal: feps + fcaps = ON
if flagneutralization == 2:
    zneutralstart = zfinal+1.0      # Not normal: feps + fcaps = OFF
if flagneutralization == 3:
    zneutralstart = znonneutralend  # Not Normal: feps = ON; fcaps = OFF


def heavisidetheta(x):
    if x == 0:
        return 0.5
    elif x < 0:
        return 0
    else:
        return 1

heavisidetheta = np.frompyfunc(heavisidetheta, 1, 1)

nfrac = float(commands['--neutral'])
assert nfrac <= 1.0, "Neutralization fraction must be smaller or equal 1.0"
assert nfrac >= 0.0, "Neutralization fraction must be larger or equal 0.0"


def fneut(z):
    return nfrac*heavisidetheta(z-zneutralstart)

if flagneutralization == 3:
    def fneut(z):
        return heavisidetheta(z-zneutralstart)-heavisidetheta(z-zdriftend)

Z = np.linspace(z0, zfinal, 100)

epsx = (1.*1e-4)*epsnx/beta0  # cm-rad # 1e-4 = 1 cm/10 mm * 1 rad/ 1000 mrad
perv = perv0
flagvelocityweight = 2


def find_z_index(Z, pos):
    return np.argmin(np.abs(Z-pos))

# create initial plot with all zeroes
if commands['interactive']:
    a = np.zeros_like(Z)
    ap = np.zeros_like(Z)
    l = np.zeros_like(Z)
    lp = np.zeros_like(Z)

    plota = [None, None, None]
    plotap = [None, None, None]
    plotl = [None, None, None]
    plotlp = [None, None, None]

    bsolenoid = lambda z: (bsol1*(heavisidetheta(z-zsol1start)-heavisidetheta(z-zsol1end)) +
                           bsol2*(heavisidetheta(z-zsol2start)-heavisidetheta(z-zsol2end)) +
                           bfinalsol*(heavisidetheta(z-zdriftend)-heavisidetheta(z-zfinalsolend)))
    ksolenoid = lambda z: qcharge*bsolenoid(z)/(2.*ionmass*amumass*beta0*clightcgs)

    fig, ((ax1, ax2), (ax3, ax4), (dummy, ax6)) = plt.subplots(3, 2)
    fig.subplots_adjust(bottom=0.3)
    plota[0], = ax1.plot(Z, a)
    plota[1], = ax1.plot(Z, a)
    plota[2], = ax1.plot(Z, a)
    plotB, = ax1.plot(Z, bsolenoid(Z))
    ax1.set_xlabel('z')
    ax1.set_ylabel('a')
    plotap[0], = ax3.plot(Z, ap)
    plotap[1], = ax3.plot(Z, ap)
    plotap[2], = ax3.plot(Z, ap)
    ax3.set_xlabel('z')
    ax3.set_ylabel('ap')
    plotl[0], = ax2.plot(Z, l)
    plotl[1], = ax2.plot(Z, l)
    plotl[2], = ax2.plot(Z, l)
    ax2.set_xlabel('z')
    ax2.set_ylabel('Bunch length [cm]')
    plotlp[0], = ax4.plot(Z, lp)
    plotlp[1], = ax4.plot(Z, lp)
    plotlp[2], = ax4.plot(Z, lp)
    ax4.set_xlabel('z')
    ax4.set_ylabel(r'$\Delta$v/v')
    X = np.linspace(0, 10, 100)
    Y = signal.gaussian(100, 10)
    plotgauss, = ax6.plot(X, Y)
    ax6.set_title("beam at target plane")
    plt.tight_layout()
    plt.draw()


def plotupdate():
    global a1, ap1, zafocus1
    global a2, ap2, zafocus2
    global a3, ap3, zafocus3
    global zlfocus, bunchfinal, zlongneut, deltavovnonneutend
    global bfinalsol, bsol2

    gfactor = 2.*np.log(rpipe/a0)

    # recalculate deltavovnonneutend
    def betaovbeta0(z, lnp):
        return 1.

    bsolenoid = lambda z: (bsol1*(heavisidetheta(z-zsol1start)-heavisidetheta(z-zsol1end)) +
                           bsol2*(heavisidetheta(z-zsol2start)-heavisidetheta(z-zsol2end)) +
                           bfinalsol*(heavisidetheta(z-zdriftend)-heavisidetheta(z-zfinalsolend)))
    ksolenoid = lambda z: qcharge*bsolenoid(z)/(2.*ionmass*amumass*beta0*clightcgs)
    if commands['interactive']:
        plotB.set_ydata(bsolenoid(Z))

    sol = odeint(lambda y, z: [y[1],
                               -ksolenoid(z)**2*y[0]/betaovbeta0(z, y[3])**2+(1.-fneut(z))*1.5*totalcharge /
                               (frpieps0*ionenergy*betaovbeta0(z, y[3])**2*y[2]*y[0]/100.)+epsx**2 /
                               (betaovbeta0(z, y[3])**2*y[0]**3),
                               y[3],
                               6.*gfactor*(1.-fneut(z))*1e2*totalcharge /
                               (frpieps0*y[2]**2*ionenergy)+epsl**2/y[2]**3],
                 [a0, ap0, l0, lp0], Z)

    a, ap, l, lp = sol.T

    deltavovnonneutend = lp[find_z_index(Z, zneutralstart)]
    deltavovsol2end = lp[find_z_index(Z, zsol2end)]
    peakcurrentsol2end = 1.5*totalcharge*beta0*clightcgs/(l[find_z_index(Z, zsol2end)]+1e-12)
    pulsedurationsol2end = 1.5*totalcharge/peakcurrentsol2end

    myprint("Calculated deltav/v non-neutral end = ", deltavovnonneutend)
    myprint("Calculated deltav/v sol2 end = ", deltavovsol2end)
    myprint("Calculated peakcurrent sol2 end = ", peakcurrentsol2end, " A")
    myprint("Calculated pulse duration sol2 end = ", pulsedurationsol2end*1e9, " ns")

    bsolenoid = lambda z: (bsol1*(heavisidetheta(z-zsol1start)-heavisidetheta(z-zsol1end)) +
                           bsol2*(heavisidetheta(z-zsol2start)-heavisidetheta(z-zsol2end)) +
                           bfinalsol*(heavisidetheta(z-zdriftend)-heavisidetheta(z-zfinalsolend)))
    ksolenoid = lambda z: qcharge*bsolenoid(z)/(2.*ionmass*amumass*beta0*clightcgs)

    for j in range(nslice):
        i = j+1
        if flagvelocityweight == 1:
            const = 0.5
        elif flagvelocityweight == 2:
            const = 3/8

        if i == 1:
            def betaovbeta0(z, lnp):
                return (1.+const*lnp) if z <= zneutralstart else 1+const*deltavovnonneutend
        if i == 2:
            def betaovbeta0(z, lnp):
                return 1
        if i == 3:
            def betaovbeta0(z, lnp):
                return (1.-const*lnp) if z <= zneutralstart else 1-const*deltavovnonneutend

        myprint("z0 = ", z0, " zfinal = ", zfinal)

        sol = odeint(lambda y, z: [y[1],
                                   -ksolenoid(z)**2*y[0]/betaovbeta0(z, y[3])**2+(1.-fneut(z))*1.5*totalcharge /
                                   (frpieps0*ionenergy*betaovbeta0(z, y[3])**2*y[2]*y[0]/100.)+epsx**2 /
                                   (betaovbeta0(z, y[3])**2 * y[0]**3),
                                   y[3],
                                   6.*gfactor*(1.-fneut(z))*1e2*totalcharge /
                                   (frpieps0*y[2]**2*ionenergy)+epsl**2/y[2]**3],
                     [a0, ap0, l0, lp0], Z)

        a, ap, l, lp = sol.T

        if commands['interactive']:
            plota[j].set_ydata(a)
            plotap[j].set_ydata(ap)
            plotl[j].set_ydata(l)
            plotlp[j].set_ydata(lp)
            for ax in [ax1, ax2, ax3, ax4]:
                ax.relim()
                ax.autoscale_view()

        if i == 2:
            particlecurrent = 1.5*totalcharge*beta0*clightcgs/l
            linechargedensity = 1.5*totalcharge*1e8/l

            if commands['interactive']:
                splinelp = interp1d(Z, lp, kind='cubic')
                splinel = interp1d(Z, l, kind='cubic')
                try:
                    zlfocus = scipy.optimize.brentq(splinelp, zfinal-zchamber, zfinal)
                except:
                    zlfocus = zfinal
                bunchfinal = splinel(zlfocus)
                zlongneut = zlfocus-zneutralstart
                myprint("zfocus = ", zlfocus, " cm")
                myprint("Final bunch duration = ", 1e9*bunchfinal/(beta0*clightcgs), " ns")
                myprint("Final bunch length = ", bunchfinal, " cm")
                myprint("Distance from end of non-neutral to long. focus =", zlongneut, " cm")
            else:
                zlfocus = 1
                bunchfinal = 1
                zlongneut = 1

            # check for beam scraping
            FFsignal = a[(Z < zfinalsolend)*(Z > zdriftend)]
            if FFsignal.max() > 1.9:
                scraping = (1.9/FFsignal.max())**2
            else:
                scraping = 1

        idx = np.ma.array(a, mask=Z < zfinalsolend-0.75*zfinalsol).argmin()
        zafocus = Z[idx]
        aspot = a[idx]
        myprint("i = ", i)
        myprint("beta = ", betatab[j])
        myprint("z(transverse focus) = ", zafocus, " cm")
        myprint("aspot = ", aspot, " cm")
        if i == 1:
            a1 = a
            ap1 = ap
            zafocus1 = zafocus
        if i == 2:
            a2 = a
            ap2 = ap
            zafocus2 = zafocus
        if i == 3:
            a3 = a
            ap3 = ap
            zafocus3 = zafocus
    idx = np.argmin(np.abs(Z-((zfinalsolend+zdriftend)*0.5+18)))
    average_radius = np.sqrt(((5./32.)*a1[idx]**2+(11./16.)*a2[idx]**2+(5./32.)*a3[idx]**2))
    myprint("average radius: ", average_radius, idx, a1[idx], (zfinalsolend+zdriftend)*0.5+18)
    if commands['interactive']:
        Y = signal.gaussian(100, average_radius*10)
        plotgauss.set_ydata(Y)
        plt.draw()
    return scraping, average_radius


def Nupdate(val):
    global nfrac, fneut
    nfrac = val

    def fneut(z):
        return nfrac*heavisidetheta(z-zneutralstart)

    plotupdate()


def EPSupdate(val):
    global epsnx, epsx
    epsnx = val
    epsx = (1.*1e-4)*epsnx/beta0
    plotupdate()


def SRKupdate(val):
    global bsol2
    bsol2 = val
    plotupdate()


def FFupdate(val):
    global bfinalsol
    bfinalsol = val
    plotupdate()

if commands['interactive']:
    Nslider = Slider(plt.axes([0.1, 0.11, 0.4, 0.02]), "neut", 0, 1, valinit=1)
    Nslider.on_changed(Nupdate)
    Epsslider = Slider(plt.axes([0.1, 0.08, 0.4, 0.02]), "EPS", 0, 3, valinit=2)
    Epsslider.on_changed(EPSupdate)
    SRKslider = Slider(plt.axes([0.1, 0.05, 0.4, 0.02]), "SRK", 0, 3, valinit=2)
    SRKslider.on_changed(SRKupdate)
    FFslider = Slider(plt.axes([0.1, 0.02, 0.4, 0.02]), "FF", 0, 10, valinit=8)
    FFslider.on_changed(FFupdate)
    plt.show()

plotupdate()

if commands['scan']:
    try:
        with open(picklefile, 'rb') as f:
            result = pickle.load(f)
    except:
        result = []
    origresults = result
    for a0 in np.linspace(2, 4, 10):
        print("****STEP1 ", a0, "******")
        for ap0 in np.linspace(-20e-3, 20e-3, 3):
            found = False
            for r1, r2, r3 in origresults:
                if np.isclose(r1, a0) and np.isclose(r2, ap0):
                    print("   ****SKIPPING ", a0, ap0, "******")
                    found = True
                    break
            if found:
                continue
            print("   ****STEP2 ", ap0, "******")
            out = []
            print("      ", end='')
            for bsol2 in np.linspace(0.7, 2.4, 50):
                s, r = plotupdate()
                print(".", end="")
                sys.stdout.flush()
                out.append([bsol2, s, r])
            print("")
            out = np.array(out)
            result.append([a0, ap0, out])
            with open(picklefile, 'wb') as f:
                pickle.dump(result, f)

    fig, (ax1, ax2) = plt.subplots(2, 1)

    ax2.set_xlabel("SRK [T]")
    ax1.set_ylabel("fraction of transported beam")
    ax2.set_ylabel("sigma of gaussian at scint.")

    for a, b, c in result:
        lw = 1
        if b < 0:
            color = "r"
        elif b > 0:
            color = "g"
        else:
            color = "b"
        if a == 2:
            color = "k"
            lw = 2
        ax1.plot(c[:, 0], c[:, 1], color=color, lw=lw)
        ax2.plot(c[:, 0], c[:, 2], color=color, lw=lw)

    patch1 = mpatches.Patch(color='r', label="a' =-20 mrad")
    patch2 = mpatches.Patch(color='b', label="a' =  0 mrad")
    patch3 = mpatches.Patch(color='g', label="a' = 20 mrad")
    patch4 = mpatches.Patch(color='k', label="a = 2 cm")

    plt.legend(handles=[patch1, patch2, patch3, patch4], loc='best')

    plt.show()
    plt.savefig("result.png")


abar = np.sqrt(((5./32.)*a1**2+(11./16.)*a2**2+(5./32.)*a3**2))
tmp = np.sqrt((ap1**2+ap2**2+ap3**2)/3.)
apbar = np.where(ap2 < 0, -tmp, tmp)
thetarms = np.sqrt(((1.*1e-8)*epsnx**2/(betatab[0]**2*a1**2)+ap1**2 +
                    (1.*1e-8)*epsnx**2/(betatab[1]**2*a2**2)+ap2**2 +
                    (1.*1e-8)*epsnx**2/(betatab[2]**2*a3**2)+ap3**2)/(4*nslice))
if zafocus3 < zafocus1:
    idx = abar[find_z_index(Z, zafocus3):find_z_index(Z, zafocus1)].argmin()+find_z_index(Z, zafocus3)
elif zafocus1 < zafocus3:
    idx = abar[find_z_index(Z, zafocus1):find_z_index(Z, zafocus3)].argmin()+find_z_index(Z, zafocus1)
else:
    print("strange")
    idx = find_z_index(Z, zafocus3)
zabarfocus = Z[idx]
abarfocus = abar[idx]
zneutral = zabarfocus-zneutralstart
r0est = abar[find_z_index(Z, zdriftend)]

optnumberfluenceest = (4.*(totalcharge/qcharge)*np.arctan(np.pi**2*deltavovnonneutend*r0est**2 /
                                                          (8.*lfocusthickest*epsx)) /
                       (np.pi*lfocusthickest*epsx*deltavovnonneutend))

myprint("2nd estimate of optimum number fluence = ", optnumberfluenceest, " #/cm2")
optfluenceest = optnumberfluenceest*ionenergy*qcharge
myprint("2nd estimate of optimum fluence = ", optfluenceest, " J/cm2")
myprint("z(abar focus) = ", zabarfocus, " cm")
myprint("z(end of final focus solenoid) = ", zfinalsolend, " cm")
myprint("abarfocus = ", abarfocus, " cm")
myprint("distance from end of non-neutral to transverse focus =", zneutral, " cm")
myprint("distance from end of non-neutral to longitudinal focus =", zlfocus-znonneutralend, " cm")
myprint("distance from end of final focus solenoid to transverse focus =",
        zabarfocus-zfinalsolend, " cm")
myprint("***************************************")
myprint("distance from beginning of final focus solenoid to transverse focus =",
        zabarfocus-zdriftend, " cm")
pulsedurationfinal = bunchfinal/(beta0*clightcgs)  # ns
myprint("Final bunch duration = ", pulsedurationfinal*1e9, " ns")
myprint("Final bunch length = ", bunchfinal, " cm")
myprint("Distance from end of non-neutral to long. focus =", zlongneut, " cm")
myprint("distance from end of non-neutral to transverse focus =", zneutral, " cm")
myprint("abarfocus = ", abarfocus, " cm")
myprint("Target heating parameters:")
myprint("Total pulse energy = ", totalcharge*ionenergy, " J")
fluence = 2.*totalcharge*ionenergy/(np.pi*abarfocus**2)
numberfluence = fluence/(ionenergy*qcharge)
myprint("Fluence = ", fluence, " J/cm**2")
myprint("Number fluence = ", numberfluence, " /cm2")

if flagtargetmodel == "Al":
    rhotarget = rhoAl
    soundspeed = soundspeedAl
    foilthickness = foilthicknessAl
    targetmass = 27.0  # target mass in amu Al: 27
    energymelt = 0.277  # eV/atom Al: 0.277
    latentheatofmelt = 0.112  # eV/atom Al: 0.112
    meltingtemp = 0.080  # melting temp eV Al: 0.080

if flagtargetmodel == "Au":
    rhotarget = rhoAu
    soundspeed = soundspeedAu
    foilthickness = foilthicknessAu
    targetmass = 196.9  # target mass in amu Au: 27
    energymelt = 0.402  # eV/atom Al: 0.402
    latentheatofmelt = 0.132  # eV/atom Au
    meltingtemp = 0.115  # melting temp eV Au

energydensitypergram = depositedenergyfraction*fluence/(rhotarget*foilthickness)  # J/g
myprint("Deposited energy density = ", energydensitypergram, " J/g")

kT0 = .025  # initial temp
energytomeltingtemp = 3.*(meltingtemp-kT0)
energydensityperatom = energydensitypergram*targetmass*amumass*1e3/qcharge  # eV
if energydensityperatom > energytomeltingtemp+latentheatofmelt:
    kT = meltingtemp + (energydensityperatom-energytomeltingtemp-latentheatofmelt)/3.
if energydensityperatom > energytomeltingtemp and energydensityperatom < energytomeltingtemp+latentheatofmelt:
    kT = meltingtemp  # estimate temp in eV; no expansion
if energydensityperatom < energymelt:
    kT = kT0 + energydensityperatom/3.  # estimate temp in eV; no expansion
kTwithexpansion = kT/(1+soundspeed*pulsedurationfinal/foilthickness)
myprint("Estimated target temperature without expansion = ", kT, " eV")
myprint("Estimated target temperature with expansion = ", kTwithexpansion, " eV")
