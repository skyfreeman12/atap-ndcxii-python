"""remote control NDCXII via python"""

from .db import Shot, Comments, Setting
from . import db as DB
from . import config
try:
    import ujson as json
except ImportError:
    import json
import hashlib
import zmq
import random
import time

# need functions like
#   setSRKvoltages
#   setCellTiming
#   close valve for bkgd shot
#   read settings (from db or rawdata?)
#
#  also:
#    scan voltage in steps of dV
#    do random sampling over dict of parameters
#
#  also:
#    implement wait to adjust for manual changes, e.g
#       SetCellVoltages -- prints out list of voltages that need to be set manual
#                          also saves these to the db
#       wait_for_completion

context = zmq.Context()

need_new_background = False


def trigger():
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://128.3.58.16:6002")

    socket.send(bytearray('trigger', 'utf-8'))
    message = socket.recv()


def backgroundtrigger():
    global need_new_background
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://128.3.58.16:6002")

    socket.send(bytearray('backgroundtrigger', 'utf-8'))
    message = socket.recv()
    need_new_background = False


def take_shot(timeout=45, testfunc=None):
    """Take a shot, including backgroundshot if needed

    if testfunc is given, use this to test if the shot was good

    """
    got_data = False
    while not got_data:
        if need_new_background:
            print("taking new background shot")
            backgroundtrigger()
            time.sleep(timeout)
        print("taking new shot")
        trigger()
        triggertime = time.time()
        time.sleep(10)
        if testfunc:
            got_data = testfunc()
            if not got_data:
                print("bad shot...repeating")
        else:
            got_data = True
        wait = time.time()-triggertime
        if wait < timeout:
            time.sleep(timeout-wait)


def last_shot():
    # use all shot numbers including background shots
    return DB.parse_shotnumber('Alatest')


def modify_setting(devicename, newsettings):
    """Change the setting of a device

    newsettings depends on the device. Here is a list of devices that can be changes at the moment:

    SRKs:  newsettings is a list of [[SRK#, value], ...]

    """
    if not DB.init_done:
        DB.init()
    if not DB.init_done:
        print("Warning: can't connect to the database. Skipping")
        return

    shotnr = last_shot()
    # add check if we got a valid shotnumber
    print("last shot:", shotnr)
    if devicename.startswith('SRK'):
        s = Shot.objects(lvtimestamp=shotnr, devicename="SRKs").first()
        h = s.settinghash
        setting = Setting.objects(Setting.hash == h).first()
        data = setting.data
        old = setting.data
        data = json.loads(data)
        for settingname, value in newsettings:
            if settingname in data:
                data[settingname] = value
            else:
                print("name unknown:", settingname)
        new = json.dumps(data, sort_keys=True).replace(" ", "")
        m = hashlib.md5()
        m.update(bytearray(new, 'utf-8'))
        hash = m.hexdigest()

        # send to database
        S = Setting.create(hash=hash, data=new, version=1)

        # tell labview to load hash
        socket = context.socket(zmq.REQ)
        socket.connect("tcp://128.3.58.17:6005")

        socket.send(bytearray(hash, 'utf-8'))
        message = socket.recv()
        print("updated SRK setting")
    else:
        print("can't update setting for this device yet")


def modify_timing(devicename, newsetting):
    """Change the timing of a device

    allowed devices: SRK01-28, Diploes, FEPS, FFS, FCAPS, Streak, Cell1-7, Injector, BL1-5
    """
    if not DB.init_done:
        DB.init()
    if not DB.init_done:
        print("Warning: can't connect to the database. Skipping")
        return

    shotnr = last_shot()
    # add check if we got a valid shotnumber
    print("last shot:", shotnr)
    if devicename.startswith('SRK'):
        config = config.config(shotnr, devicename)
        timing = config['timing']
        timingname = timing.device()
        s = Shot.objects(lvtimestamp=shotnr, devicename=timingname).first()
        h = s.settinghash
        setting = Setting.objects(Setting.hash == h).first()
        data = setting.data
        old = setting.data
        data = json.loads(data)
        board = timing.slot
        channel = timing.channel
        data['Trigger Modules'][board]['Channels'][channel]['delay (s)'] = newsetting
        new = json.dumps(data, sort_keys=True).replace(" ", "")
        m = hashlib.md5()
        m.update(bytearray(new, 'utf-8'))
        hash = m.hexdigest()

        # send to database
        S = Setting.create(hash=hash, data=new, version=1)

        # tell labview to load hash
        socket = context.socket(zmq.REQ)
        socket.connect("tcp://128.3.58.51:6000")

        socket.send(bytearray(hash, 'utf-8'))
        message = socket.recv()
        print("updated SRK setting")
    else:
        print("can't update setting for this device yet")
