""" Some helper function to handle data at the NDCXII experiment
"""

import os
import time
import calendar

try:
    import ujson as json
except ImportError:
    import json

import numbers
from datetime import datetime, date

import numpy as np
from scipy.interpolate import interp1d
import scipy.constants as SC


def GetImageFileNames(images, path, name, ext):
    """ allow for short cut in the image names

    expecting file names like:

    ../NDCX-II-data/131029/images/1310290065.SPE

    This function takes a tuple of shortened image names, such as
    ['12, '14', '15-20'] and using `path`, `name`, and `ext`, returns
    a tuple with the full path names


    Parameters
    ----------
    images : a tuple of file names, can be in short hand notation,
             e.g just the last N digits or a range
    path : the path to the images
    name : the file name that short hand notation gets extended to
    ext : the file extension to be used

    Result
    ------
    result : a tuple of expanded file names


    """
    result = []

    Lext = len(ext)+1  # +1 for the "."

    imagenrs = []
    # replace "xx-yy" ranges with numbers:
    for f in images:
        cpath, cname = os.path.split(f)
        if "-" in cname:
            start, end = cname.split("-")
            for k in range(int(start), int(end)+1):
                imagenrs.append(str(k))
        else:
            imagenrs.append(f)

    # replace path
    for f in imagenrs:
        l = len(f)
        if l < len(name):
            result.append(path + "/" + name[:-l-Lext] + f + "." + ext)
        else:
            result.append(f)

    return result


def LVtime2comment(timestamp):
    """Convert a LV timestamp to a YYYYMM and DDHHMM format.
    This format is used for comments in the cassandra database
    """
    deltatime = calendar.timegm(time.strptime("1 Jan 1904", "%d %b %Y"))
    # deltatime < 0
    unix = timestamp+deltatime

    month = int(time.strftime("%Y%m", time.localtime(unix)))
    daytime = int(time.strftime("%d%H%M", time.localtime(unix)))

    return month, daytime


def LVtime2allshots(timestamp):
    """Convert a LV timestamp to a YYYYMM and DD4200 format.
    This format is used for comments in the cassandra database
    """
    month, daytime = LVtime2comment(timestamp)
    daytime = str(daytime)
    daytime = daytime[:-4]+"4200"
    daytime = int(daytime)

    return month, daytime


def lvtime2unix(lvtimestamp):
    """convert labview time to a unix time stamp"""

    # some error checking
    if not isinstance(lvtimestamp, numbers.Integral):
        raise TypeError
    if not 3000000000 < lvtimestamp < 9999999999:
        raise ValueError

    # shift to unix time stamp 1.1.1904 is a negative value in unix time,
    # so just add it to the lv-time
    return lvtimestamp+calendar.timegm(time.strptime("1 Jan 1904", "%d %b %Y"))


def unix2lvtime(unixtime):
    """convert unix time to a labview time stamp"""
    if isinstance(unixtime, datetime):
        unixtime = int(unixtime.strftime("%s"))
    return int(unixtime-calendar.timegm(time.strptime("1 Jan 1904", "%d %b %Y")))


def lvtime2datetime(lvtimestamp):
    """convert labview time to a datetime object"""

    unix = lvtime2unix(lvtimestamp)
    return datetime.fromtimestamp(unix)


def datetime2lvtime(timestamp):
    """convert a datetime object to lvtime"""

    unix = timestamp.timestamp()
    return unix2lvtime(unix)


class Power:
    """Get the filament power at a certain time"""

    def __init__(self, filename):
        self.filename = filename
        data = np.loadtxt(filename)
        dtime = data[:, 0]
        power = data[:, 3]

        # adjust labview time, timestamp is negative, so add it
        dtime += calendar.timegm(time.strptime("1 Jan 1904", "%d %b %Y"))
        self.t = dtime
        self.p = power
        self.spline = interp1d(dtime, power)

    def attime(self, mytime):
        """convert a time string to unix time and
        return the interpolated power of the filament"""
        t = time.mktime(time.strptime(mytime, "%m%d%y%H%M%S"))
        return self.spline(t)


def beta(E, m=4):
    """return the velocity/c for a particle at energy E

    E in eV
    m in amu (default He)
    """

    gamma = 1 + E/(m*SC.physical_constants['atomic mass unit-electron volt relationship'][0])
    return np.sqrt(1 - 1/gamma/gamma)


def velocity(E, m=4):
    """Velocity in m/s"""
    return beta(E, m) * SC.c


def compare_settings(A, B, path=""):
    """Compare two settings

    """
    assert isinstance(A, dict)
    assert isinstance(B, dict)

    old_path = path
    for k in A:
        path = old_path + "[{}]".format(k)
        if not k in B:
            print("Key A[{}] not in B".format(path))
        else:
            if isinstance(A[k], dict) and isinstance(B[k], dict):
                compare_settings(A[k], B[k], path)
            elif isinstance(A[k], list) and isinstance(B[k], list):
                for i, (a, b) in enumerate(zip(A[k], B[k])):
                    if isinstance(a, dict) and isinstance(b, dict):
                        compare_settings(a, b, path=path+"[{}]".format(i))
                    else:
                        if a != b:
                            print("Value of {} differ: A={} B={}".format(path, a, b))
            else:
                if A[k] != B[k]:
                    print("Value of {} differ: A={} B={}\n".format(path, A[k], B[k]))
    for k in B:
        if not k in A:
            print("Key A{} not in B\n".format(path+"[{}]".format(k)))
    return
