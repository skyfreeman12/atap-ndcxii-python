"""Functions and classes that use or manipulate data from the database.

The main tools provided here are:

Classes:
--------
Shotdata  -- access all kind of data from a single shot
             only reads and manipulates the data when it's needed

ContData  -- continuos data



Scope     -- main class representing a single scope, including background
             shot data. Defines several filters that can be used to manipulate
             the shot data, such as background subtraction, slope correction, smoothing, etc.

Vmon      -- specialized class of Scope
Imon      -- specialized class of Scope
SRK       -- specialized class of Scope
BPM       -- specialized class of Scope, handles the 4 quadrants and combining the signal

All of the Scope classes have there data in self.t and self.x. The filters act on these.

"""

from . import db as DB
from . import readSPE
from . import config
import numbers
import numpy as np
from scipy import stats
from scipy.integrate import cumtrapz
from scipy.interpolate import InterpolatedUnivariateSpline as Spline
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.ndimage import gaussian_filter
import datetime
import pytz
import matplotlib.pyplot as plt
import matplotlib.patches
from matplotlib.colors import LogNorm
import scipy.constants as SC
from scipy.interpolate import UnivariateSpline

eps = 1e-15

# some debug flags
import logging
log = logging.getLogger(__name__)


def fft_low_pass_filter_data(data, time, freq=300e6):
    """Filter high frequency components out of the data

       Parameters:
       ----------
       data : numpy array of data values
       time : numpy array of time values in [s]
       freq : optional cut of frequency [default: 300MHz]

    """

    fftsignal = np.fft.rfft(data)
    delta = (time[1:] - time[:-1]).mean()
    frequencies = np.fft.rfftfreq(len(data), d=delta)
    arg = np.abs((frequencies - freq)).argmin()
    fftsignal[arg:] = 0.0

    return np.fft.irfft(fftsignal)


def is_shotnumber(shot):
    """simple test if this is a good shotnumber"""

    if isinstance(shot, numbers.Integral):
        if 0 < shot < 9999999999:
            return True
    return False


class Scope():
    """Class for single channel of a scope

    Ability to apply many filters, also handles background shot data
    """

    def __init__(self, devicename, shotnumber=None, background=None, compressed=True, parent=None):
        """Set the name of the device and also set up some caches that that are specific to each instance"""
        self.name = devicename
        self.parent = parent
        if parent is not None:
            self.shotnumber = parent.shotnumber
            self.backgroundshotnumber = parent.backgroundshotnumber
            self.compressed = parent.compressed
        else:
            self.shotnumber = shotnumber
            self.backgroundshotnumber = background
            self.compressed = compressed
        self.comment = DB.get_shot(self.shotnumber, 'comment')

        # make sure shotnumbers are OK
        assert is_shotnumber(self.shotnumber), "not a valid shotnumber {}".format(self.shotnumber)
        if self.backgroundshotnumber is not None:
            assert is_shotnumber(self.backgroundshotnumber), "not a valid background shotnumber {}".format(
                self.backgroundshotnumber)

        # raw data from the database
        self.rawt = None
        self.rawx = None
        self.rawbackt = None
        self.rawbackx = None
        # data with filters applied
        self._t = None
        self._x = None
        self.backt = None
        self.backx = None
        self.filter = None  # filters to be used
        self.defaultfilter = []  # default filters for this instrument
        self.additional_filters = {}
        self.filterdone = []  # filters actually used
        self.mask = None
        # data window
        self.defaultwindow = None  # from config
        self.window = None
        self.color = None

        # plot information
        self.labelx = "Time [s]"
        self.labely = "Voltage [V]"

        if background:
            self.backgroundshotnumber = background
        else:
            self.backgroundshotnumber = DB.get_shot(self.shotnumber, 'backgroundshot')

        # get config information
        self.config = config.config(self.shotnumber, self.name)
        if self.backgroundshotnumber:
            self.backgroundconfig = config.config(self.backgroundshotnumber, self.name)
        else:
            self.backgroundconfig = None
        self._timing = {}

        # get the settings for the scope
        self.scopesetting = DB.get_setting(self.shotnumber, str(self.config['devices']).split('-')[0])

        self.set_window(self.compressed)

    def __repr__(self):
        position = self.config.get('position', None)
        cell = self.config.get('cell', None)
        out = "{} shotnumber:{} device:{}".format(self.__class__.__name__, self.shotnumber, self.name)
        if position is not None:
            out = out + " position:{}".format(position)
        if cell is not None:
            out = out + " cell:{}".format(cell)

        return "<{}>".format(out)

    @property
    def x(self):
        if self._x is None:
            self.get_data()
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def t(self):
        if self._t is None:
            self.get_data()
        return self._t

    @t.setter
    def t(self, value):
        self._t = value

    @property
    def timing(self):
        if not self._timing:
            # define delay from Blumlein upstairs trigger to actual firing of the
            # acceleration voltage, average delay from comment 201610 251718
            # this comment has 15 shots
            # These are used in the report.py shot-monitor
            if self.name.startswith('BL1'):
                self._timing['delay [us]'] = .4185
            elif self.name.startswith('BL2'):
                self._timing['delay [us]'] = .4964
            elif self.name.startswith('BL3'):
                self._timing['delay [us]'] = .4522
            elif self.name.startswith('BL4'):
                self._timing['delay [us]'] = .4607
            elif self.name.startswith('BL5'):
                self._timing['delay [us]'] = .5273

            if 'timing' in self.config:
                for k, v in self.config['timing'].items():
                    devicename = v.device()
                    try:
                        if k == 'aux timing':
                            self._timing[k] = (DB.get_setting(self.shotnumber, devicename)['Trigger Modules']
                                               [v.slot]['Board Config']['Aux Channels'][v.channel]['delay (s)'])
                        else:
                            self._timing[k] = (DB.get_setting(self.shotnumber, devicename)['Trigger Modules']
                                               [v.slot]['Board Config']['Channels'][v.channel]['delay (s)'])
                    except TypeError:
                        print("Error: Couldn't get timing setting for device {} and shotnumber {}".format(
                            self.name, self.shotnumber))
        return self._timing

    def set_window(self, compressed):
        if compressed is True and 'window-compressed' in self.config:
            self.defaultwindow = self.config['window-compressed']
        elif compressed is False and 'window-uncompressed' in self.config:
            self.defaultwindow = self.config['window-uncompressed']

    def debugplot(self, ax=None, filter=None, **kwargs):
        if filter is None:
            filter = self.defaultfilter

        origax = ax
        for i in range(1, len(filter)):
            if origax is None:
                fig, ax = plt.subplots(1, 1)
            self.plot(ax=ax, filter=filter[:i - 1], color="r",
                      label='w/o {}'.format(filter[i - 1]), **kwargs)
            log.info('calling plot with filter: {}'.format(filter[:i]))
            self.plot(ax=ax, filter=filter[:i], showwindow='box', title=' '.join(
                filter[:i]), label="with {}".format(filter[i - 1]), color="g", **kwargs)
            plt.legend(loc='best')
            if origax is None:
                plt.show()

    def plot(self, ax=None, filter=None, usewindow=False, showwindow=None,
             offset=0, toffset=0, title=None, **kwargs):
        """Create a plot, of data vs time. Create a figure, if no axes is given.

        kwargs get passed to the plot command.

        also adds the following filters as kwargs:

        usewindow:    True or False, only plot data in window
        showwindow:   can be 'box', 'underline', or 'shadow'
        offset:       offsets the plot by that amount in X
        toffset:      offsets the plot by that amount in T
        showfilters: create a list of filters on the side
        """

        log.info('in plot: filter= {}'.format(filter))
        log.debug('         self.filter= {}'.format(self.filter))
        log.debug('         filterdone= {}'.format(self.filterdone))
        log.debug('         self.t? {} filter? {} filterdone=filter? {}'.format(
            self.t is None, filter is None, filter != self.filterdone))

        # do we need to generate the data?
        if filter is None:
            if self.defaultfilter != self.filterdone:
                self.get_data()
        else:
            if filter != self.filterdone:
                self.get_data(filter)

        origax = ax
        if ax is None:
            fig, ax = plt.subplots(1, 1)

        time = self.t
        data = self.x
        Xmin = data[self.mask].min() + offset
        Xmax = data[self.mask].max() + offset
        tmin = time[self.mask].min() + toffset
        tmax = time[self.mask].max() + toffset
        dt = self.t[1] - self.t[0]
        dx = 0.01 * data[self.mask].ptp()

        if usewindow:
            l, = ax.plot(time[self.mask] + toffset,
                         data[self.mask] + offset, **kwargs)
        else:
            l, = ax.plot(time + toffset, data + offset, **kwargs)
        ax.set_xlabel(self.labelx)
        ax.set_ylabel(self.labely)
        if title:
            plt.title(title)
        if self.color is None:
            self.color = l.get_color()

        if showwindow == "box":
            window = matplotlib.patches.Rectangle(
                (tmin, Xmin), (tmax - tmin), (Xmax - Xmin), alpha=0.1)
            ax.add_patch(window)
        elif showwindow == "underline":
            Xmin = 0.5 * (data[self.mask][0] +
                          data[self.mask][-1]) - 10 * dx + offset
            ax.plot([tmin, tmax], [Xmin, Xmin], color=self.color, lw=4)
        elif showwindow == "shadow":
            lw = 2 * kwargs.pop("lw", 2.0)
            kwargs.update({'lw': lw})
            ax.plot(self.t[self.mask] + 2 * dt + toffset, self.x[self.mask] -
                    2 * dx + offset, color=self.color, **kwargs)

        if origax is None:
            plt.show()

    def register_filter(self, name, filterfunc):
        self.additional_filters[name] = filterfunc

    def apply_filter(self, filter=[]):
        """Apply a filter to a self._t, self._x

        The following filters are available:

        setwindow(a, b):  sets self.window and self.mask to the [a,b]
        unsetwindow:      sets self.mask to include all values of self.rawt
        setdefaultwindow: uses set.defaultwindow to set self.mask and self.window
        low_pass(freq):   calcs the FFT, sets everything above freq to zero and converts back from FFT
        normalize:        normalizes to 1
        normalize(a):     normalizes to a
        scale(a):         scales the function by a
        savgol(w):        runs scipy.signal.savgol_filter with window=w, polyorder=2, mode='nearest'
        savgol:           runs the same with a window=15
        backsub:          subtracts self.rawbackx (can only be called as the first filter in a list)
        bias(p):          subtract the average of the first p percent of the data from the whole signal
        bias:             same with 10%
        slope(p):         subtracts a linear fit of the first p percent of the data from the whole signal
        slope:            same with the first 10%
        slopepremask:     same using all data left of the window for the fit
        slopeboth(p):     subtracts a linear fit of the first and lastp percent of the data from the whole signal
        slopeboth:        same with the first 10%
        toff(t):          offsets the time by t
        swapbackground:   swap t, x with backt, backx
        cumsum            calls np.cumsum on the data, usefull for integrated charge on the FFCup for example


        additional filters might be available in additional_filters.

        """

        log.info('applying filters: {}'.format(filter))
        self.filter = filter
        self.filterdone = []
        for i, f in enumerate(filter):
            log.debug('  working on filter: {}'.format(f))
            if f in self.additional_filters:
                self.additional_filters[f]()
            elif f.startswith('setwindow('):
                window = f[10:-1].split(',')
                self.window = [float(window[0]), float(window[1])]
                self.mask = (self._t >= self.window[0]) * (self._t <= self.window[1])
            elif f.startswith('setdefaultwindow'):
                self.window = self.defaultwindow
                self.mask = (self._t >= self.window[0]) * (self._t <= self.window[1])
            elif f.startswith("unsetwindow"):
                self.mask = np.ones_like(self.rawt, dtype=np.bool)
            elif f.startswith("low_pass("):
                freq = float(f[9:-1])
                self._x[self.mask] = fft_low_pass_filter_data(
                    self._x[self.mask], self._t[self.mask], freq=freq)
            elif f == 'fft':
                fftsignal = np.fft.rfft(self._x[self.mask])
                delta = np.diff(self._t).mean()
                frequencies = np.fft.rfftfreq(len(self._x[self.mask]), d=delta)
                self._t = frequencies
                self._x = np.abs(fftsignal)**2
                self.mask = np.ones_like(self._t, dtype=np.bool)
                self.labelx = "Frequency [Hz]"
                self.labely = "Spectral Power"
            elif f.startswith("normalize("):
                # normalize to a given amplitude
                amp = float(f[10:-1])
                self._x[self.mask] /= self._x[self.mask].max() * amp
            elif f.startswith("normalize"):
                # normalize to 1.0
                self._x[self.mask] /= self._x[self.mask].max()
            elif f.startswith("scale("):
                factor = float(f[6:-1])
                self._x[self.mask] *= factor
            elif f.startswith("savgol"):
                if f.startswith("savgol("):
                    window = int(f[7:-1])
                else:
                    window = 15
                self._x[self.mask] = savgol_filter(
                    self._x[self.mask], window_length=window, polyorder=2, mode='nearest')
            elif f == "backsub":
                assert i == 0, "Background subtraction needs to be the first filter"
                if self.rawbackt is None or self.rawbackx is None:
                    raise ValueError("Background data not given")
                else:
                    assert np.array_equal(
                        self.rawbackt, self.rawt), "Timebase for background subtraction not equal"
                    self._x -= self.rawbackx
            elif f.startswith("bias"):
                if f.startswith("bias("):
                    bias = float(f[5:-1])
                    assert bias > 0 and bias < 100, "Bias range must be a 0<bias<100"
                else:
                    bias = 10
                Vtmp = self._x[self.mask]
                i = int(bias / 100 * len(Vtmp))
                vbias = Vtmp[:i].sum() / float(i)
                self._x[self.mask] -= vbias
            elif f.startswith("slope"):
                ttmp, xtmp = self._t[self.mask], self._x[self.mask]

                if f.startswith("slope("):
                    bias = float(f[6:-1])
                    i = int(bias / 100 * len(xtmp))
                elif f.startswith("slopeboth("):
                    bias = float(f[10:-1])
                    i = int(bias / 100 * len(xtmp))
                elif f.startswith("slopepremask"):
                    i = np.argmin(np.abs(self._t - self.window[0]))
                else:
                    bias = 10
                    i = int(bias / 100 * len(xtmp))

                if f.startswith("slopeboth"):
                    slope, intrcpt, r, p, stderr = stats.linregress(
                        np.hstack((ttmp[:i], ttmp[-i:])),
                        np.hstack((xtmp[:i], xtmp[-i:])))
                else:
                    slope, intrcpt, r, p, stderr = stats.linregress(ttmp[:i], xtmp[:i])
                self._x[self.mask] = self._x[self.mask] - intrcpt - slope * self._t[self.mask]
            elif f.startswith("toff("):
                offset = float(f[5:-1])
                self._t -= offset
            elif f.startswith('swapbackground'):
                self._t, self._x, self.backt, self.backx = self.backt, self.backx, self._t, self._x
            elif f.startswith('cumsum'):
                self._x = self.x.cumsum()
            else:
                print("Warning: filter {} is not defined".format(f))
            self.filterdone.append(f)

    def fix_bad_scope(self, t, x):
        """check for a specific bad scope channel

           these scopes have roughly every other data points zero'ed outage
           remove those, and replace with linear interpolation
        """
        mask = x == 0
        if sum(mask) > 0.3*len(mask):
            print("Detected bad scope channel for {} shot {}...fixing it.".format(self.name, self.shotnumber))
            t_tmp = t[~mask]
            x_tmp = x[~mask]
            func = interp1d(t_tmp, x_tmp, bounds_error=False, fill_value=0)
            x = func(t)

        return t, x

    def get_raw(self):
        """Just gets the raw data and some config from the database.  This
           includes the background shot if needed.

        """
        dev = self.config['devices']

        data = DB.get_shot(self.shotnumber, dev.crateslot(), dev.channel)
        assert data is not None and len(
            data) > 0, "ERROR: no data for {} {} {}".format(self.shotnumber, dev.crateslot(), dev.channel)

        # get the waveform
        rate = data['Actual Sample Rate']

        # safe the settinghash
        hash = DB.get_settinghash(self.shotnumber, dev.crateslot())
        self.config['settinghash'] = hash
        setting = DB.get_setting_from_hash(hash)
        refpos = float(setting['reference position']) / 100.0
        conf = {'refpos': refpos}
        self.config['setting'] = conf

        calibration = self.config['calibration']
        attenuator = self.config['attenuator']

        # update plotting units, once we converted using calibration
        if 'unit' in self.config:
            self.labely = self.config['unit']

        # generate a time axis
        Y = np.array(data['wave' + str(dev.channel)])

        if self.parent is not None:
            self.inj_timing = self.parent.Injector.timing['trigger']
        else:
            self.inj_timing = 1.23e-3

        if 'aux timing' in self.timing:
            self.trig_timing = self.timing['aux timing']
        elif 'trigger' in self.timing:
            self.trig_timing = self.timing['trigger']
        else:
            self.trig_timing = 1.23e-3
            # skip warning for picoscope, since there is no trigger available
            if not self.config['devices'].name.startswith('picoscope01'):
                print('No trigger timing for device {} and shot {}!!'.
                      format(self.name, self.shotnumber))

        self.rawt = np.linspace(0, len(Y) / rate, len(Y), endpoint=False)
        self.rawt = (self.rawt -
                     refpos * (self.rawt[-1] - self.rawt[0]) -
                     self.config['tcable'] - self.inj_timing + self.trig_timing)
        self.rawx = Y * calibration * attenuator

        # fix bad scope channels
        self.rawt, self.rawx = self.fix_bad_scope(self.rawt, self.rawx)

        # set the mask to include everything
        self.mask = np.ones_like(self.rawt, dtype=np.bool)

        if self.backgroundshotnumber:
            # some devices might have shot data, but not background shot data
            data = DB.get_shot(self.backgroundshotnumber,
                               dev.crateslot(), dev.channel)
            if data is None:
                print("Warning: no background shot for for device", self.name)
                self.backgroundconfig = None
                self.backgroundshotnumber = None

            else:
                assert len(data) > 0, "ERROR: no data for {} {} {}".format(
                    self.backgroundshotnumber, dev.crateslot(), dev.channel)

                # get the waveform
                rate = data['Actual Sample Rate']

                # safe the settinghash
                hash = DB.get_settinghash(
                    self.backgroundshotnumber, dev.crateslot())
                self.backgroundconfig['settinghash'] = hash
                setting = DB.get_setting_from_hash(hash)
                refpos = float(setting['reference position']) / 100.0
                conf = {'refpos': refpos}
                self.backgroundconfig['setting'] = conf
                calibration = self.backgroundconfig['calibration']
                attenuator = self.backgroundconfig['attenuator']

                # generate a time axis
                Y = np.array(data['wave' + str(dev.channel)])
                self.rawbackt = np.linspace(0, len(Y) / rate, len(Y), endpoint=False)
                self.rawbackt = (self.rawbackt -
                                 refpos * (self.rawbackt[-1] - self.rawbackt[0]) -
                                 self.backgroundconfig['tcable'] - self.inj_timing + self.trig_timing)
                self.rawbackx = Y * calibration * attenuator

        return self.rawt, self.rawx

    def get_data(self, filter=None):
        """Apply filters to data """

        log.info('in get-data: filter={}'.format(filter))
        # check if we already calculated this, if so, skip
        if self._t is not None:
            if filter is None:
                if self.defaultfilter == self.filterdone:
                    return self._t, self._x
            else:
                if filter == self.filterdone:
                    return self._t, self._x

        log.debug('in get-data: need to recalculated')
        if self.rawt is None or self.rawx is None:
            log.debug('in get-data: get raw data')
            self.get_raw()
        # copy from raw data and reapply filters
        self._t = np.copy(self.rawt)
        self._x = np.copy(self.rawx)
        self.backt = np.copy(self.rawbackt)
        self.backx = np.copy(self.rawbackx)
        if filter is None:
            filter = self.defaultfilter
        self.window = self.defaultwindow
        self.apply_filter(filter)
        return self._t, self._x


class BPM(Scope):
    """Data from a beam position monitor.

    current, linecharge density, and position
    The quadrants of the BPM can be accessed using self.A-self.D.
    """

    def __init__(self, devicename, shotnumber=None, background=None, compressed=True, parent=None):
        """Initialize Scopes for each quadrant.

        set the correct device and time offsets for
        """
        super().__init__(devicename, shotnumber, background, compressed, parent)

        # set the scopes used for a BPM
        self.A = Scope(devicename, shotnumber, background, compressed, parent)
        self.A.config['devices'] = self.config['devices'][0]
        self.A.config['tcable'] = self.config['tcable'][0]
        self.A.config['timing'] = self.config['timing'][0]
        if self.A.backgroundshotnumber is not None:
            self.A.backgroundconfig['devices'] = self.backgroundconfig['devices'][0]
            self.A.backgroundconfig['tcable'] = self.backgroundconfig['tcable'][0]

        self.B = Scope(devicename, shotnumber, background, compressed, parent)
        self.B.config['devices'] = self.config['devices'][1]
        self.B.config['tcable'] = self.config['tcable'][1]
        self.B.config['timing'] = self.config['timing'][1]
        if self.B.backgroundshotnumber is not None:
            self.B.backgroundconfig['devices'] = self.backgroundconfig['devices'][1]
            self.B.backgroundconfig['tcable'] = self.backgroundconfig['tcable'][1]

        self.C = Scope(devicename, shotnumber, background, compressed, parent)
        self.C.config['devices'] = self.config['devices'][2]
        self.C.config['tcable'] = self.config['tcable'][2]
        self.C.config['timing'] = self.config['timing'][2]
        if self.C.backgroundshotnumber is not None:
            self.C.backgroundconfig['devices'] = self.backgroundconfig['devices'][2]
            self.C.backgroundconfig['tcable'] = self.backgroundconfig['tcable'][2]

        self.D = Scope(devicename, shotnumber, background, compressed, parent)
        self.D.config['devices'] = self.config['devices'][3]
        self.D.config['tcable'] = self.config['tcable'][3]
        self.D.config['timing'] = self.config['timing'][3]
        if self.D.backgroundshotnumber is not None:
            self.D.backgroundconfig['devices'] = self.backgroundconfig['devices'][3]
            self.D.backgroundconfig['tcable'] = self.backgroundconfig['tcable'][3]

        self.QUADRANTS = [self.A, self.B, self.C, self.D]

        for i in self.QUADRANTS:
            i.defaultfilter = ['backsub']
        self.defaultfilter = ['sumall', 'setdefaultwindow', 'button_charge',
                              'charge', 'linecharge', 'sumall', 'calcI']
        self.linechargefilter = ['sumall', 'setdefaultwindow', 'button_charge',
                                 'charge', 'linecharge', 'sumall', 'calcI']

        self.Imax = None
        self.Imaxtime = None
        self.offsets = None
        self.register_filter('button_charge', self.button_charge)
        self.register_filter('charge', self.charge)
        self.register_filter('linecharge', self.linecharge)
        self.register_filter('sumall', self.sumall)
        self.register_filter('calcI', self.calc_current)

    def button_charge(self):
        """Calculate button charge across a resistor and a cap.

        Isignal = V/R + C*Vdot
        Isignal [A]; Qbutton [C]

        """
        for scope in self.QUADRANTS:
            t = scope.t[self.mask]
            V = scope.x[self.mask]
            SplRep = Spline(t, V)
            Spl1D = SplRep.derivative()
            R = self.config['zterm']
            C = self.config['capacitance']

            Vdot = Spl1D(t)

            scope._x[self.mask] = V / R + C * Vdot
            scope.filterdone.append('button_charge')

        self.labely = "Button Charge [A]"

    def linecharge(self):
        """Return line charge in nC/m."""
        assert 'charge' in self.filterdone, "BPM->linecharge: need to calculate the charge first"

        scale = 1e9 * 4 / self.config['length'] / 2 / np.pi

        for scope in self.QUADRANTS:
            scope._x[self.mask] *= scale
            scope.filterdone.append('linecharge')
        self.labely = "Line Charge [nC/m]"

    def calc_current(self):
        m = np.argmax(self._x[self.mask])
        self.Imax = self._x[m]
        self.Imaxtime = self.t[m]

    def charge(self):
        """Integrate from current to charge"""
        assert 'button_charge' in self.filterdone, "BPM->charge: need to calculate the current first"
        for scope in self.QUADRANTS:
            scope._x[self.mask] = cumtrapz(scope._x[self.mask], scope._t[self.mask], initial=0.0)
            scope.filterdone.append('charge')
        self.labely = "Charge [C]"

    def elec2beam(self, X, Y):
        """Maps the electrical coord. (xe,ye) to beamline coordinates (xb,yb).

        Result of inversion and nonlinear parametric fit of coordinates in
        code BPMdiff2D.py

        xb = elec2beam(xe,ye)
        yb = elec2beam(ye,xe)
        Accurate to (xe**2+ye**2) < 0.7**2

        """
        A = np.array([1.56185609, 0.47527204, 1.18077069, -3.21869839,
                      -1.28844517, 7.07828566, -1.8571103, -0.45848622])
        B = np.array([1.60989274, 0.39026078, 0.44836547, 7.05919419,
                      -0.28318437, 0.03662738, 1.92680652, 0.56780481])

        num = sum(a * X**(2 * i) for i, a in enumerate(A))
        denom = sum(b * Y**(2 * i) for i, b in enumerate(B))

        return X * np.exp(num) / np.exp(denom)

    def getoffset(self, lamA, lamB, lamC, lamD):
        """ dx ~ (A+B)-(C+D)
            dy ~ (A+D)-(B+C)
        """
        assert 'linecharge' in self.filterdone, "need to calculate the linecharge first"

        lamtot = self.x + eps
        dxlste = np.pi / 4. * (self.A.x + self.B.x -
                               self.C.x - self.D.x) / lamtot
        dylste = np.pi / 4. * (self.A.x - self.B.x -
                               self.C.x + self.D.x) / lamtot

        dxlste = dxlste * (np.abs(dxlste) < np.pi / 4.)
        dylste = dylste * (np.abs(dylste) < np.pi / 4.)

        dxlst = self.elec2beam(dxlste, dylste)
        dylst = self.elec2beam(dylste, dxlste)
        dxlst = dxlst * self.config['radius'] * 1e3
        dylst = dylst * self.config['radius'] * 1e3

        self.offsets = [dxlst, dylst]

    def sumall(self):
        self._t = self.A.t
        self._x = self.A.x + self.B.x + self.C.x + self.D.x

    def get_raw(self):
        """needs to be done in the subclasses for each scope"""
        log.debug('In {} get_raw '.format(self.name))

        for i in self.QUADRANTS:
            i.get_data()

        # set the mask to include everything
        self.mask = np.ones_like(self.A.t, dtype=np.bool)

        # 8/12/13 BPM 1 Ch.D is non-functional
        if self.name == "BPM1":
            self.D.x = (self.A.x + self.B.x + self.C.x) / 3
            print('Ch. 1D is non-functional. Averaging over channels A,B,C')

    def get_data(self, filter=None):
        log.debug('In {} get_data '.format(self.name))
        rawt = [i.rawt is None for i in self.QUADRANTS]
        quadfilter = [i.defaultfilter != i.filterdone for i in self.QUADRANTS]
        if any(rawt) or any(quadfilter):
            self.get_raw()
        if filter is None:
            filter = self.defaultfilter
        self.labely = "Voltage [V]"
        self.window = self.defaultwindow
        self.apply_filter(filter)
        return self._t, self._x

    def get_linecharge(self, filter=None):
        if filter is None:
            filter = self.linechargefilter
        self.get_data(filter=filter)
        return self.t, self.x

    def plot_linecharge(self, filter=None, **kwargs):
        if filter is None:
            filter = self.linechargefilter
        self.plot(filter=filter, **kwargs)


class Imon(Scope):
    """An inductive current monitor

    This includes the conversion from voltage to current
    """

    def __init__(self, devicename, shotnumber=None, background=None, compressed=True, parent=None):
        super().__init__(devicename, shotnumber, background, compressed, parent)
        self.defaultfilter = ['backsub', 'savgol', 'slopepremask',
                              'setdefaultwindow', 'droop', 'unsetwindow', 'slopepremask',
                              'setdefaultwindow', 'calc']
        self.register_filter('droop', self.droop)
        self.register_filter('calc', self.calc_values)

    def droop(self):
        # apply droop correction to voltage signals
        # I = V/R + 1/R * 1/tau * Integral(V dt), where tau = L/R
        zterm = self.config['zterm']
        LRconstant = self.config['LRconstant']
        Q = cumtrapz(self.x[self.mask], self.t[self.mask], initial=0.0)
        self.x[self.mask] = self.x[self.mask] / zterm + Q / zterm / LRconstant

    def calc_values(self):
        # calculate/save some derived values on the windowed area
        time, current = self.t[self.mask], self.x[self.mask]
        msk = current > 0
        dt = time[1] - time[0]
        self.Qtot = current[msk].sum() * dt  # nC
        self.Imax = current[msk].max()
        self.tavg = (time[msk] * current[msk]).sum() / current[msk].sum()
        self.tmax = time[current == self.Imax][-1]
        halfmax = 0.5 * self.Imax
        self.tminus = time[(current <= halfmax) * (time <= self.tmax)].max()
        self.tplus = time[(current <= halfmax) * (time >= self.tmax)].min()
        self.tmid = 0.5 * (self.tminus + self.tplus)
        self.Qfwhm = current[(self.tminus <= time) * (time <= self.tplus)].sum() * dt
        self.noise = np.std(current[:len(current) // 10])

    def get_current(self, filter=None):
        self.get_data(filter=filter)
        return [self.t, self.x]

#    def plot_fwhm(self, ax, filter=None, background=True, postfilter=None, usewindow=True):
#        # calc values
#        time, current = self.get_current(filter=filter, background=background,
#                                         postfilter=postfilter, usewindow=usewindow)[0]
#        # fill FWHM portion of the plot
#        if self.color is not None:
#            color = self.color
#        else:
#            color = "k"
#        mask = (self.tminus <= time) * (time <= self.tplus)
#        ax.fill_between(time[mask], 0, current[mask],
#                        facecolor=color, alpha=0.15)


class Vmon(Scope):
    """A simple voltage monitor"""

    def __init__(self, devicename, shotnumber=None, background=None, compressed=True, parent=None):
        super().__init__(devicename, shotnumber, background, compressed, parent)
        self.defaultfilter = []

    def get_voltage(self, filter=None):
        self.get_data(filter=filter)
        return self.t, self.x

    def calc_values(self, p=False):
        if self.x is None:
            self.get_data()
        time, voltage = self.t[self.mask], self.x[self.mask]
        dt = time[1] - time[0]
        N = len(time)

        vbkgd = np.concatenate((voltage[:int(.1 * N)], voltage[int(.9 * N):]))
        tbkgd = np.concatenate((time[:int(.1 * N)], time[int(.9 * N):]))

        fit = np.polyfit(tbkgd, vbkgd, 1)
        p = np.poly1d(fit)

        voltage = voltage - p(time)
        self.xprime = voltage
        vbkgd = np.concatenate((voltage[:int(.1 * N)], voltage[int(.9 * N):]))
        tbkgd = np.concatenate((time[:int(.1 * N)], time[int(.9 * N):]))

        offset = np.mean(vbkgd)
        doffset = np.std(vbkgd)

        self.end = None
        self.start = None
        for i, (t, v) in enumerate(zip(time, voltage)):
            if self.start is None and v > 3 * doffset and v <= voltage[i + 1]:
                self.start = t
            if self.start is not None and v < 2 * doffset and v >= voltage[i + 1]:
                self.end = t
                break
        if self.start is None:
            self.start = time[0]
        if self.end is None:
            self.end = time[-1]

        self.vmax = voltage.max()
        self.tmax = time[voltage == self.vmax][-1]
        halfmax = self.vmax / 2
        self.tminus = time[(voltage <= halfmax) * (time <= self.tmax)].max()
        self.tplus = time[(voltage <= halfmax) * (time >= self.tmax)].min()
        self.fwhm = self.tplus - self.tminus

        # calculate the sum of voltage values inside the FWHM
        self.vfwhm = voltage[(self.tminus <= time) *
                             (time <= self.tplus)].sum() * dt
        self.total = voltage[(time <= self.end) *
                             (time >= self.start)].sum() * dt

        self.vmean = np.mean(voltage)
        # if p:
        #     fig, ax = plt.subplots(1, 1)
        #     ax.plot(time[(self.tminus <= time) * (time <= self.tplus)],
        #             voltage[(self.tminus <= time) * (time <= self.tplus)],
        #             'r.')
        #     ax.plot(time[(self.tminus >= time) | (time >= self.tplus)],
        #             voltage[(self.tminus >= time) | (time >= self.tplus)],
        #             'b.')
        #     ax.axvline(self.start)
        #     ax.axvline(self.end)
        #     plt.show()


class SRK(Scope):
    """B-field monitors for the solenoids."""

    def __init__(self, devicename, shotnumber=None, background=None, compressed=True, parent=None):
        super().__init__(devicename, shotnumber, background, compressed, parent)
        if devicename.startswith('SRK'):
            setting = DB.get_setting(self.shotnumber, 'SRKs')
            if setting:
                self.voltagesetting = setting[devicename]

    def Bmax(self, filter=None):
        # TODO: returning max amplitude, but perhaps returning the amplitude
        # when the beam is passing through is better
        try:
            self.get_data(filter=filter)
        except AssertionError:
            print("Warning: couldn't get data for ", self.name)
            self.x = np.array([0, 0])
        # Checking for broken scopes:
        m = 7.7417e-4  # slope from linear fit w/ voltagesetting
        b = 2.0836e-3  # intercept from linear fit
        try:
            if self.name.startswith('SRK'):
                # Checking for negative values
                if self.x.max() < 0.0:
                    print("{} {} negative value for Bmax, replaced with fit".format(
                        self.shotnumber, self.name))
                    return self.voltagesetting*m+b
                # Checking for large deviations from what we expect
                elif abs(self.x.max() - self.voltagesetting*m+b)/(self.voltagesetting*m+b) > 0.5:
                    print("{} {} large error in Bmax compared to fitted line for solenoids, replaced with fit".format(
                        self.shotnumber, self.name))
                    print("    voltage setting = {}   interpolated Bmax = {}".format(
                        self.voltagesetting, self.voltagesetting*m+b))
                    return self.voltagesetting*m+b
                else:
                    return self.x.max()
            else:  # e.g. FFS
                return self.x.max()
        except:
            print("{} {} hit except statment for Bmax".format(self.shotnumber, self.name))
            return self.x.max()

    def Bz(self, zrange, filter=None):
        """Calculate on-axis field for a solenoid at different z-positions

        The formula used is for a solenoid that extends from -L/2 to L/2
        and consists out of set of single loops.

        L, R and the mid-point of the solenoid are taken from the config data.
        """

        # solenoid fit parameters (model parameter fit from measured data)
        L = self.config['length']
        R = self.config['radius']
        f0 = (L / 2.) / ((L / 2.)**2 + R**2)**0.5
        Zpos = self.config['position']

        # accumulate field from each solenoid
        zmag = zrange - Zpos
        fs = 0.5 * ((L / 2. - zmag) / ((L / 2. - zmag)**2 + R**2)**0.5 +
                    (L / 2. + zmag) / ((L / 2. + zmag)**2 + R**2)**0.5)
        return self.Bmax(filter=filter) * fs / f0


class Scintillator():
    '''handle scintillator images and background images

    Currently we only load SPE files, in future, we should have those in the DB.
    The calibration factor can be set during the __call__ and should be in px/mm.
    '''

    def __init__(self, filename=None, backgroundimagefile=None, calibration=None):
        self.image = None
        self.backgroundimage = None
        self.mask = None
        self.calibration = None
        self.integralX = None
        self.integralY = None
        self.lineoutpx = 10  # number of pixels used for the lineout
        self.lineoutX = None
        self.lineoutY = None
        self.mass = None
        self.FWHMX = None
        self.FWHMXmax = None
        self.FWHMXargmax = None
        self.FWHMY = None
        self.FWHMYmax = None
        self.FWHMYargmax = None
        self.max = None
        self.argmax = None
        self.Nx = None
        self.Ny = None
        if filename is not None:
            self.__call__(filename, backgroundimagefile, calibration)

    def __call__(self, imagefile, backgroundimagefile=None, calibration=None):
        self.image = readSPE.load(imagefile)
        self.Nx, self.Ny = self.image.shape
        self.calibration = calibration   # pixel per mm
        if backgroundimagefile:
            self.backgroundimage = readSPE.load(backgroundimagefile)

    def apply_filter(self, filter, image):
        """Apply a filter to an image

        The filter is given as a string. Optional a second image
        can be given to be used for certain filters (e.g. background
        subtraction)

        """
        if filter is None:
            return image
        image = image.astype(np.float64)
        filters = filter.split(",")
        for f in filters:
            if f.startswith("normalize("):
                # normalize to a given amplitude
                amp = float(f[10:-1])
                image /= image.max() * amp
            elif f.startswith("normalize"):
                # normalize to 1.0
                image /= image.max()
            elif f.startswith("scale("):
                factor = float(f[6:-1])
                image *= factor
            elif f.startswith("sub("):
                bkgd = int(f[4:-1])
                image = np.where(image > bkgd, image - bkgd, 0)
            elif f.startswith("mask"):
                image[~self.mask] = 0
            elif f.startswith("gaussian("):
                sigma = int(f[9:-1])
                image = gaussian_filter(image, sigma=sigma)
            elif f.startswith("gaussian"):
                image = gaussian_filter(image, sigma=5)
            elif f == "backsub":
                if self.backgroundimage is None:
                    raise ValueError("background data not given")
                else:
                    image = image - self.backgroundimage
                    image[image < 0] = 0
        return image.astype(np.uint16)

    def set_mask_rectangle(self, center=(), width=None, height=None, LL=(), RR=()):
        if self.image is None:
            raise ValueError("No image data given")

        if center and width is not None and height is not None:
            assert len(center) == 2, "need a coordinate pair for center"
            cx, cy = center
            assert 0 <= cx and cx <= self.Nx, "Center coordinate outside image"
            assert 0 <= cy and cy <= self.Ny, "Center coordinate outside image"
            mx1 = int(cx - width / 2) if cx - width / 2 >= 0 else 0
            mx2 = int(cx + width / 2) if cx + width / 2 <= self.Nx else self.Nx
            my1 = int(cy - height / 2) if cy - height / 2 >= 0 else 0
            my2 = int(cy + height / 2) if cy + \
                height / 2 <= self.Ny else self.Ny
        elif LL and RR:
            assert len(LL) == 2, "need a coordinate pair for LL"
            assert len(RR) == 2, "need a coordinate pair for RR"
            mx1, my1 = LL
            mx2, my2 = RR
        else:
            raise ValueError("Not enough information to set a rectangle mask")

        x, y = np.ogrid[:self.Ny, :self.Nx]
        maskX = (mx1 < x) * (x < mx2)
        maskY = (my1 < y) * (y < my2)
        self.mask = maskX * maskY

    def set_mask_circle(self, center=(), radius=None):
        if self.image is None:
            raise ValueError("No image data given")

        if center and radius is not None:
            assert len(center) == 2, "need a coordinate pair for center"
            assert radius > 0, "radius need to be > 0"
            cx, cy = center
        else:
            raise ValueError("Not enough information to set a circle mask")

        x, y = np.ogrid[:self.Nx, :self.Ny]
        r2 = (x - cx) * (x - cx) + (y - cy) * (y - cy)
        self.mask = (r2 <= radius * radius)

    def FWHM(self, line):
        m = line.max()
        p = line.argmax()
        l = (line > m / 2).argmax()
        r = (line[p:] > m / 2).argmin() + p - 1
        return r - l, m, p

    def get_image(self, filter=None):
        if filter and 'usebackground' in filter and self.backgroundimage is not None:
            image = self.backgroundimage
            filters = filter.split(',')
            filters = [f for f in filters if f != "usebackground"]
            filter = ",".join(filters)
        else:
            image = self.image

        if filter:
            image = self.apply_filter(filter, image)
        else:
            image = image

        # calculate some moments, etc.
        self.integralX = image.sum(axis=0)
        self.integralY = image.sum(axis=1)
        self.mass = self.integralX.sum()
        self.max = image.max()
        self.argmax = np.unravel_index(image.argmax(), image.shape)

        # calculate lineouts with +- self.lineoutpx/2 pixels in X and Y from
        # max position
        origmask = self.mask
        self.set_mask_rectangle(center=self.argmax,
                                width=self.lineoutpx, height=self.Ny)
        FWHMXimage = self.apply_filter(filter='mask', image=image)
        self.lineoutX = FWHMXimage.sum(axis=0)
        self.set_mask_rectangle(center=self.argmax,
                                width=self.Nx, height=self.lineoutpx)
        FWHMYimage = self.apply_filter(filter='mask', image=image)
        self.lineoutY = FWHMYimage.sum(axis=1)
        self.mask = origmask

        # find FWHM & integral
        self.FWHMX, self.FWHMXmax, self.FWHMXargmax = self.FWHM(self.lineoutX)
        self.FWHMY, self.FWHMYmax, self.FWHMYargmax = self.FWHM(self.lineoutY)

        return image

    def plot(self, filter=None, ax=None, **kwargs):

        origax = ax
        if ax is None:
            fig, ax = plt.subplots(1, 1)

        if filter and 'showmask' in filter:
            showmask = True
            filters = filter.split(',')
            filters = [f for f in filters if f != "showmask"]
            filter = ",".join(filters)
        else:
            showmask = False

        if filter and 'showlineouts' in filter:
            showlineouts = True
            filters = filter.split(',')
            filters = [f for f in filters if f != "showlineoutsx"]
            filter = ",".join(filters)
        else:
            showlineouts = False

        image = self.get_image(filter=filter)
        Nx, Ny = image.shape

        if self.calibration:
            extent = [0, Nx / self.calibration, 0, Ny / self.calibration]
        else:
            extent = None

        ax.imshow(image, extent=extent, **kwargs)
        ax.set_xlim([0, Nx])
        ax.set_ylim([Ny, 0])

        if showmask and self.mask is not None:
            red_cmap = plt.cm.Reds
            red_cmap.set_under(color="white", alpha="0")
            ax.imshow(self.mask, cmap=red_cmap, alpha=0.3)

        if showlineouts:
            ax.plot(range(Nx), self.lineoutX /
                    self.lineoutX.max() * Ny * 0.1, "k")
            ax.plot(self.lineoutY / self.lineoutY.max()
                    * Nx * 0.1, range(Nx), "k")

        if origax is None:
            plt.show()

    def logplot(self, filter=None, ax=None, **kwargs):
        self.plot(filter=filter, ax=ax, norm=LogNorm())

    def debugplot(self, ax=None, filter=None, **kwargs):
        origax = ax
        for i in range(1, len(filter)):
            if origax is None:
                fig, ax = plt.subplots(1, 1)
            log.info('calling plot with filter: {}'.format(filter[:i]))
            self.plot(ax=ax, filter=filter[:i], title=' '.join(filter[:i]),
                      label="with {}".format(filter[i - 1]), **kwargs)
            plt.legend(loc='best')
            if origax is None:
                plt.show()


class Shotdata():
    """The main class to access data from a single shot."""

    def __init__(self, shotnumber, bkgd=None, compressed=True):
        self.compressed = compressed
        self.shotnumber = shotnumber
        self.backgroundshotnumber = bkgd  # set a default here, set to value from DB later

        # test if we have a valid shotnumber
        tmp = DB.get_shot(shotnumber)
        if not tmp:
            print("Warning: no data for shot {}".format(shotnumber))
            return

        # define used BPM
        self.BPM1 = BPM("BPM1", parent=self)
        self.BPM2 = BPM("BPM2", parent=self)
        self.BPM3 = BPM("BPM3", parent=self)
        self.BPM4 = BPM("BPM4", parent=self)
        self.BPM5 = BPM("BPM5", parent=self)
        self.BPM6 = BPM("BPM6", parent=self)
        self.BPM7 = BPM("BPM7", parent=self)
        self.BPM = [self.BPM1, self.BPM2, self.BPM3,
                    self.BPM4, self.BPM5, self.BPM6,
                    self.BPM7]

        # define all voltage monitors
        self.Injector = Vmon("Injector", shotnumber, bkgd, compressed, parent=None)
        self.Cell1 = Vmon("Cell1", parent=self)
        self.Cell2 = Vmon("Cell2", parent=self)
        self.Cell3 = Vmon("Cell3", parent=self)
        self.Cell4 = Vmon("Cell4", parent=self)
        self.Cell5 = Vmon("Cell5", parent=self)
        self.Cell6 = Vmon("Cell6", parent=self)
        self.Cell7 = Vmon("Cell7", parent=self)
        self.BL1 = Vmon("BL1", parent=self)
        self.BL2 = Vmon("BL2", parent=self)
        self.BL3 = Vmon("BL3", parent=self)
        self.BL4 = Vmon("BL4", parent=self)
        self.BL5 = Vmon("BL5", parent=self)
        self.BL1_chargeV = Vmon("BL1-chargeV", parent=self)
        self.BL2_chargeV = Vmon("BL2-chargeV", parent=self)
        self.BL3_chargeV = Vmon("BL3-chargeV", parent=self)
        self.BL4_chargeV = Vmon("BL4-chargeV", parent=self)
        self.BL5_chargeV = Vmon("BL5-chargeV", parent=self)
        self.BL1_chargeI = Vmon("BL1-chargeI", parent=self)
        self.BL2_chargeI = Vmon("BL2-chargeI", parent=self)
        self.BL3_chargeI = Vmon("BL3-chargeI", parent=self)
        self.BL4_chargeI = Vmon("BL4-chargeI", parent=self)
        self.BL5_chargeI = Vmon("BL5-chargeI", parent=self)

        # define current monitors
        self.InjImon = Vmon("InjImon", parent=self)
        self.Imon1 = Imon("Imon1", parent=self)
        self.Imon2 = Imon("Imon2", parent=self)
        self.Imon3 = Imon("Imon3", parent=self)
        self.Imon4 = Imon("Imon4", parent=self)
        self.Imon5 = Imon("Imon5", parent=self)
        self.Imon6 = Imon("Imon6", parent=self)
        self.Imon7 = Imon("Imon7", parent=self)
        self.Imon8 = Imon("Imon8", parent=self)
        self.I = [self.Imon1, self.Imon2, self.Imon3, self.Imon4,
                  self.Imon5, self.Imon6, self.Imon7, self.Imon8]

        # define all SRKs
        self.SRK01 = SRK("SRK01", parent=self)
        self.SRK02 = SRK("SRK02", parent=self)
        self.SRK03 = SRK("SRK03", parent=self)
        self.SRK04 = SRK("SRK04", parent=self)
        self.SRK05 = SRK("SRK05", parent=self)
        self.SRK06 = SRK("SRK06", parent=self)
        self.SRK07 = SRK("SRK07", parent=self)
        self.SRK08 = SRK("SRK08", parent=self)
        self.SRK09 = SRK("SRK09", parent=self)
        self.SRK10 = SRK("SRK10", parent=self)
        self.SRK11 = SRK("SRK11", parent=self)
        self.SRK12 = SRK("SRK12", parent=self)
        self.SRK13 = SRK("SRK13", parent=self)
        self.SRK14 = SRK("SRK14", parent=self)
        self.SRK15 = SRK("SRK15", parent=self)
        self.SRK16 = SRK("SRK16", parent=self)
        self.SRK17 = SRK("SRK17", parent=self)
        self.SRK18 = SRK("SRK18", parent=self)
        self.SRK19 = SRK("SRK19", parent=self)
        self.SRK20 = SRK("SRK20", parent=self)
        self.SRK21 = SRK("SRK21", parent=self)
        self.SRK22 = SRK("SRK22", parent=self)
        self.SRK23 = SRK("SRK23", parent=self)
        self.SRK24 = SRK("SRK24", parent=self)
        self.SRK25 = SRK("SRK25", parent=self)
        self.SRK26 = SRK("SRK26", parent=self)
        self.SRK27 = SRK("SRK27", parent=self)
        self.SRK28 = SRK("SRK28", parent=self)
        self.SRK = [self.SRK01, self.SRK02, self.SRK03, self.SRK04, self.SRK05,
                    self.SRK06, self.SRK07, self.SRK08, self.SRK09, self.SRK10,
                    self.SRK11, self.SRK12, self.SRK13, self.SRK14, self.SRK15,
                    self.SRK16, self.SRK17, self.SRK18, self.SRK19, self.SRK20,
                    self.SRK21, self.SRK22, self.SRK23, self.SRK24, self.SRK25,
                    self.SRK26, self.SRK27, self.SRK28]

        self.CAPSA = Vmon('CAPSA', parent=self)
        self.CAPSB = Vmon('CAPSB', parent=self)
        self.CAPSC = Vmon('CAPSC', parent=self)
        self.CAPSD = Vmon('CAPSD', parent=self)
        self.CAPS = [self.CAPSA, self.CAPSB, self.CAPSC, self.CAPSD]

        self.FFS = SRK("FFS", parent=self)

        self.Scintillator = Scintillator()

        # beamline scope specific devices that include correct z-pos
        self.cameratrigger = Vmon("camera-trigger", parent=self)
        self.scintillatormesh = Vmon("scintillator-mesh", parent=self)
        self.FFCupcollector = Vmon("FFCup-collector", parent=self)
        self.FFCupsuppressor = Vmon("FFCup-suppressor", parent=self)
        self.LargeFCupcollector = Vmon("LargeFCup-collector", parent=self)
        self.LargeFCupsuppressor = Vmon("LargeFCup-suppressor", parent=self)

        # beamline scope general devices
        self.beamlinescope01A = Vmon('beamlinescope01A', parent=self)
        self.beamlinescope01B = Vmon('beamlinescope01B', parent=self)
        self.beamlinescope01C = Vmon('beamlinescope01C', parent=self)
        self.beamlinescope01D = Vmon('beamlinescope01D', parent=self)

        # source picoscope
        self.sourceextractionI = Vmon("source-extraction-current", parent=self)
        self.sourcearcI = Vmon("source-arc-current", parent=self)
        self.sourceextractionV = Vmon("source-extraction-voltage", parent=self)
        self.sourcegrid = Vmon("source-grid-voltage", parent=self)

        # group them together
        self.Cells = [self.Cell1, self.Cell2, self.Cell3,
                      self.Cell4, self.Cell5, self.Cell6,
                      self.Cell7]
        self.BL = [self.BL1, self.BL2, self.BL3, self.BL4, self.BL5]
        self.BL_chargeV = [self.BL1_chargeV, self.BL2_chargeV,
                           self.BL3_chargeV, self.BL4_chargeV, self.BL5_chargeV]
        self.BL_chargeI = [self.BL1_chargeI, self.BL2_chargeI,
                           self.BL3_chargeI, self.BL4_chargeI, self.BL5_chargeI]
        self.V = [self.Injector] + self.Cells + self.BL

        # get the background shotnumber from a device
        self.backgroundshotnumber = self.Injector.backgroundshotnumber

    def __repr__(self):
        return "<{} shotnumber:{} bkgdshot:{}>".format(self.__class__.__name__,
                                                       self.shotnumber,
                                                       self.backgroundshotnumber)

    def beamenergy(self, A=None, B=None, plot=False, ax=None):
        """Calculate the beam energy by using time of flight(TOF) between A and B

        Use two current monitors to calculate the beam energy
        using. Since the beam current sometimes can have several small
        peaks, we find the FWHM values and use the medium time of the
        FWHM as the beam arrival time.
        """

        # set default, need self for this, so we can't do it in the function definition
        if A is None:
            A = self.BPM7
        if B is None:
            B = self.scintillatormesh

        if B.config['position'] < A.config['position']:
            print('Warning: wrong order for time of flight calculation. Fixing it!')
            A, B = B, A

        # get the position of the maxima
        n1 = np.argmax(A.x)
        Amax = A.x[n1]

        n2 = np.argmax(B.x)
        Bmax = B.x[n2]

        # find left and right indices of the FWHM
        try:
            l1 = np.argmin(np.abs(A.x[:n1]-Amax/2))
        except ValueError:
            l1 = 0
        try:
            r1 = np.argmin(np.abs(A.x[n1:]-Amax/2)) + n1
        except ValueError:
            r1 = -1

        try:
            l2 = np.argmin(np.abs(B.x[:n2]-Bmax/2))
        except ValueError:
            l2 = 0
        try:
            r2 = np.argmin(np.abs(B.x[n2:]-Bmax/2)) + n2
        except ValueError:
            r2 = -1

        # calculate flight time by using median arrival time for A and B
        dt = 0.5*(B.t[l2]+B.t[r2]) - 0.5*(A.t[l1]+A.t[r1])

        # distance between A and B
        dx = B.config['position'] - A.config['position']

        v = dx/dt
        m = config.mass(self.shotnumber)

        E = 0.5 * m * v*v / SC.e
        self.Ekin = E
        self.energycalcinstruments = [A, B]

        if plot:
            origax = ax
            if ax is None:
                fig, ax = plt.subplots(1, 1)

            ax.fill_between(A.t[l1:r1]*1e9, A.x[l1:r1]/A.x.max(), alpha=0.5, color='blue', zorder=1)
            ax.plot(A.t*1e9, A.x/A.x.max(), label=A.name, color='blue', zorder=1)
            ax.fill_between(B.t[l2:r2]*1e9, B.x[l2:r2]/B.x.max(), alpha=0.5, color='orange', zorder=2)
            Bname = B.name
            if Bname in ["scintillator-mesh"]:
                Bname = "Target"
            ax.plot(B.t*1e9, B.x/B.x.max(), label=Bname, color='orange', zorder=2)
            ax.plot([A.t[n1]*1e9], [A.x[n1]/A.x.max()], "s", color="blue")
            ax.plot([B.t[n2]*1e9], [B.x[n2]/B.x.max()], "s", color="orange")
            ax.legend()
            ax.set_title('{} TOF: {:.2f} ns, E: {:.3f} MeV'.format(self.shotnumber, dt*1e9, E/1e6))
            ax.set_xlabel("Time [ns]")
            ax.set_ylabel("Current [a.u.]")
            t1 = 0.5*(A.t[l1]+A.t[r1])
            t2 = 0.5*(B.t[l2]+B.t[r2])
            ax.axvline(x=t1*1e9, color='r')
            ax.axvline(x=t2*1e9, color='r')
            ax.set_xlim([t1*1e9-800, t2*1e9+800])

            if origax is None:
                plt.show()

        return E

    def calc_energyloss(self, plot=True, target='false'):
        def subtract_background(instrument, toffset=0, FWHM=0, bpm=0):
            t, y = instrument.get_data()
            m, b = np.polyfit(t, y, 1)
            ynew = y - (m*t+b)
            t = t * 1e3 - toffset
            ynew = ynew / ynew.max()

            zerocrossing = 0
            if bpm == 0:
                # zerocrossing calculation for BPM7
                interval = [np.argmax(ynew), np.argmin(ynew)]
                for i in range(np.argmax(ynew), np.argmin(ynew)):
                    if ynew[i] <= 0:
                        zerocrossing = (t[i-1]-(t[i]-t[i-1])/(ynew[i]-ynew[i-1])*ynew[i-1])
                        break
            tmaxamp = 0
            if FWHM == 0:
                # FWHM calculation
                FWHM = []
                spl = UnivariateSpline(t, ynew-.5*ynew.max(), s=.01)
                if len(spl.roots()) > 2:
                    FWHM.append(spl.roots()[0])
                    for i in range(len(spl.roots())):
                        if (spl.roots()[i]-spl.roots())[0] > 2:
                            FWHM.append(spl.roots()[i])
                            break
                        else:
                            print('FWHM was calculated wrong!')
                else:
                    FWHM = spl.roots()

                # time of max amplitude
                tmaxamp = t[np.argmax(ynew)]

            return t, ynew, zerocrossing, FWHM, tmaxamp

        DIFF_INJECTOR_BLS = 3600  # time difference between injector trigger and beamlinescopetrigger
        DIFF_BPM7_FFC = 52  # cable difference between BPM7 and FFC suppressor in ns
        DIFF_SUP_COL = 25.5  # cable difference between FFC suppressor and collector when used for big FFC in ns
        DIFF_PXI_BLS = 18.8  # time difference between PXI trigger and beamlinescope trigger
        POSITION_BPM7 = 1.5  # position in inch
        POSITION_TARGET = 63.637  # position in inch
        POSITION_FFC = 81.692  # position in inch
        scale = 0.0254  # inch in meter

        zerocrossingcorrect = []

        x, ynew, zero, FWHMtarget, tmaxamptarget = subtract_background(
            self.FFCupcollector, toffset=DIFF_SUP_COL, bpm=1)
        x1, ynew1, zero1, FWHMFFC, tmaxampFFC = subtract_background(self.FFCupsuppressor, bpm=1)
        x2, ynew2, zero2, FWHM, tmaxamp = subtract_background(self.BPM7.A, toffset=DIFF_INJECTOR_BLS-(
            DIFF_PXI_BLS+DIFF_BPM7_FFC)+self.BPM7.config['tcable'][0]-self.BPM7.config['tcable'][0], FWHM=1)
        zerocrossingcorrect.append(zero2)
        x3, ynew3, zero3, FWHM, tmaxamp = subtract_background(self.BPM7.B, toffset=DIFF_INJECTOR_BLS-(
            DIFF_PXI_BLS+DIFF_BPM7_FFC)+self.BPM7.config['tcable'][1]-self.BPM7.config['tcable'][0], FWHM=1)
        zerocrossingcorrect.append(zero3)
        x4, ynew4, zero4, FWHM, tmaxamp = subtract_background(self.BPM7.C, toffset=DIFF_INJECTOR_BLS-(
            DIFF_PXI_BLS+DIFF_BPM7_FFC)+self.BPM7.config['tcable'][2]-self.BPM7.config['tcable'][0], FWHM=1)
        zerocrossingcorrect.append(zero4)
        x5, ynew5, zero5, FWHM, tmaxamp = subtract_background(self.BPM7.D, toffset=DIFF_INJECTOR_BLS-(
            DIFF_PXI_BLS+DIFF_BPM7_FFC)+self.BPM7.config['tcable'][3]-self.BPM7.config['tcable'][0], FWHM=1)
        zerocrossingcorrect.append(zero5)

        # print('BPM7')
        # print(zerocrossingcorrect)
        avzero = np.sum(zerocrossingcorrect)/len(zerocrossingcorrect)
        print('averaged zero crossing BPM7 [ns] = {:.2f}'.format(avzero))

        # Find FWHM for target and FFC signal

        # print('TARGET')
        # print('    roots')
        # print(FWHMtarget)
        print('FWHM of target signal[ns] = {:.2f}'.format(np.abs(FWHMtarget[1]-FWHMtarget[0])))
        # print('time of max amplitude of target signal[ns] = {:.2f}'.format(tmaxamptarget))

        # print('FFC')
        # print('    roots')
        # print(FWHMFFC)
        print('FWHM of big FFC signal[ns] = {:.2f}'.format(np.abs(FWHMFFC[1]-FWHMFFC[0])))
        # print('    max amplitude = {:.2f}'.format(ynew1.max()))
        # sprint('time of max amplitude of big FFC signal[ns] = {:.2f}'.format(tmaxampFFC))

        # Calculate time differences
        # Time difference BPM7 target signal
        difftarget = tmaxamptarget - avzero
        timedisttarget = FWHMtarget - avzero
        print('time difference BPM7 target[ns] = {:.2f}'.format(difftarget))
        # print(timedisttarget)

        # Time difference BPM7 FFC signal
        diffFFC = tmaxampFFC - avzero
        timedistFFC = FWHMFFC - avzero
        print('time difference BPM7 FFC[ns] = {:.2f}'.format(diffFFC))
        # print(timedistFFC)

        # Time difference target signal to FFC
        difftargetFFC = tmaxampFFC - tmaxamptarget
        timedisttargetFFC = FWHMFFC - tmaxamptarget
        print('time difference target FFC[ns] = {:.2f}'.format(difftargetFFC))
        # print(timedisttargetFFC)

        # Distances
        disttargetFFC = (POSITION_FFC-POSITION_TARGET)*scale
        disttargetBPM7 = (POSITION_TARGET-POSITION_BPM7)*scale
        print('distance target to FFC[m] = {:.2f}'.format(disttargetFFC))
        print('distance BPM7 to target[m] = {:.2f}'.format(disttargetBPM7))

        # Formular for energy
        mass = config.mass(self.shotnumber)

        def energycalculation(time, distance):
            kin = mass*SC.c**2*((1/(1-(distance/(time*1e-9))**2/SC.c**2)**(1/2))-1)/SC.e/1e3
            return kin

        # Calculate energy range at target plane

        # IF NO TARGET IN THE WAY THE ENERGY IS CALCULATED WITH TIME OF FLIGHT FROM BPM7 TO BIG FFC
        if target == 'false':
            energyrangeFFC = [energycalculation(t, disttargetBPM7+disttargetFFC)
                              for t in [timedistFFC[0], diffFFC, timedistFFC[1]]]
            print('energy range at big FFC without target')
            print(energyrangeFFC)
            print('conversion energy in nanoseconds 1ns = {:.2f}'.format(
                energycalculation(diffFFC, disttargetBPM7+disttargetFFC)/diffFFC))

        if target == 'true':
            energyrangetarget = [energycalculation(t, disttargetBPM7) for t in [
                timedisttarget[0], difftarget, timedisttarget[1]]]
            print('energy range at target position')
            print(energyrangetarget)
            # print('conversion energy in nanoseconds 1ns =
            # {:.2f}keV'.format(energycalculation(difftarget,disttargetBPM7)/difftarget))
            energyrangeFFCwithtarget = [energycalculation(t, disttargetFFC) for t in [timedisttargetFFC[
                0], difftargetFFC, timedisttargetFFC[1]]]
            print('energy range at big FFC with target')
            print(energyrangeFFCwithtarget)
            # print('conversion energy in nanoseconds 1ns =
            # {:.2f}keV'.format(energycalculation(difftargetFFC,disttargetFFC)/difftargetFFC))

        plt.plot(x, ynew, label='target signal')
        plt.plot(x1, ynew1, label='FFC signal')
        plt.plot(x2, ynew2, label='BPM7 signal channel A')
        plt.plot(x3, ynew3, label='BPM7 signal channel B')
        plt.plot(x4, ynew4, label='BPM7 signal channel C')
        plt.plot(x5, ynew5, label='BPM7 signal channel D')
        plt.xlabel('time in ns')
        plt.ylabel('amplitude')
        plt.axis([-300, 300.0, -1.2, 1.1])
        ax = plt.gca()
        ax.set_autoscale_on(False)
        plt.legend(loc='best')
        plt.show()

        # overlay target and FFC signals
        plt.plot(x, ynew, label='target signal')
        plt.plot((x1-difftargetFFC), ynew1, label='FFC signal')
        plt.xlabel('time in ns')
        plt.ylabel('amplitude')
        plt.axis([-50, 100.0, -0.3, 1.1])
        ax = plt.gca()
        ax.set_autoscale_on(False)
        plt.legend(loc='best')
        plt.show()

        def timecalc(kin, s):
            t = s*1e9/(np.sqrt(1-(mass*(SC.c)**2/(kin*SC.e*1e3+mass*(SC.c)**2))**2)*SC.c)
            return t
        timeresoldistFFC = []
        for i in range(1, 1205):
            timeresoldistFFC.append(timecalc(i, disttargetFFC))
        plt.plot(timeresoldistFFC, label='BPM7 to target')
        timeresoldisttarget = []
        for i in range(1, 1205):
            timeresoldisttarget.append(timecalc(i, disttargetBPM7))
        plt.plot(timeresoldisttarget, label='target to FFC')
        plt.xlabel('energy in keV')
        plt.ylabel('time in ns')
        plt.yscale('log')
        plt.xscale('log')
        plt.legend(loc='best')
        plt.show()

        energies = mp.arange(600, 1201, 50)
        timeresoldistFFC = np.array(timeresoldistFFC)

        def differentiate(vector):
            differential = []
            for i in range(600, 1201, 50):
                if i == 0:
                    differential.append(0)
                else:
                    difference = vector[i]-vector[i-50]
                    differential.append(difference)
            return differential

        gradientFFC = differentiate(timeresoldistFFC)
        gradientFFC = np.array(gradientFFC)
        # print(energies)
        # print(gradientFFC)
        plt.plot(energies, gradientFFC, label='FFC')
        gradienttarget = differentiate(timeresoldisttarget)
        gradienttarget = np.array(gradienttarget)
        print(gradienttarget)
        plt.plot(energies, gradienttarget, label='target')
        plt.title('differential of time of flight per 50keV versus energy of particles')
        plt.xlabel('energy in keV')
        plt.ylabel('dt/dE in ns/50keV')
        plt.legend(loc='best')
        # plt.show()


class ContData():
    """Access to continous data aquisition"""

    def __init__(self, name):
        self.name = name
        self.tz = pytz.timezone('US/Pacific-New')
        self.utc = pytz.timezone('UTC')
        self.limit = 10000
        self.every = 1

    def raw(self, tstart, tstop=None, every=None):
        """Get the raw data from the database

        use a start date and an end date, also only use every
        ``every`` datapoint and skip the ones inbetween.
        """
        self.every = 1 if every is None else every

        if tstop is None:
            # use tz.localize to get correct timezone
            # now().replace() doesn't work as it will use
            # the first timezone defintion it finds, which for US/Pacific
            # will be for 1893 or so and will have a 7:53 offset!
            tstop = self.tz.localize(datetime.datetime.now())

        # set timezone, if not set (assume pacific time zone where
        # experiment is based)
        if tstart.tzinfo is None:
            tstart = self.tz.localize(tstart)

        if tstop.tzinfo is None:
            tstop = self.tz.localize(tstop)

        time = []
        value = []

        more_data = True
        while more_data:
            if len(time) > 1e7:
                del time
                del value
                raise MemoryError(
                    "Time span in data query too long; too many values (>1e7)")

            # recalculate starting month, this way we can go across month
            # boundaries
            monthstart = int("{}{:02d}".format(tstart.year, tstart.month))
            data = DB.get_data(self.name, monthstart,
                               tstart, tstop, limit=self.limit)
            loctime = []
            locvalue = []
            for d in data:
                # the time we get back is saved in UTC, but no timezone is
                # set so we need to first set it, and then convert to the
                # local time zone of the experiment
                loctime.append(d.eventtime.replace(
                    tzinfo=self.utc).astimezone(self.tz))
                locvalue.append(d.value)
            if len(loctime) < self.limit:
                more_data = False
            else:
                tstart = loctime[-1]
            time = time + loctime[::every]
            value = value + locvalue[::every]
        return time, value


def ShotsFromComment(month, daytime, limit=None, skip=None):
    """Get a list of shots from a comment, can skip bad shots via skip

    limit only returns only returns a certain number of shots. This
          is mostly just for testing
    """
    shots = DB.shots_from_comment(month, daytime)
    ret = []
    for s in shots:
        if skip:
            if any([str(s).endswith(test) for test in skip]):
                continue
        ret.append(Shotdata(s))
        if limit and len(ret) >= limit:
            break
    return ret
