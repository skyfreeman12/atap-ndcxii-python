from __future__ import unicode_literals
import numpy as np
import matplotlib.pyplot as plt
import time
import sys
import os
import random
import matplotlib
# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from PyQt5 import QtCore, QtWidgets, QtGui

from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

progname = os.path.basename(sys.argv[0])
progversion = "0.1"

from NDCXII import data
import zmq

c = zmq.Context()
s = c.socket(zmq.SUB)
s.connect('tcp://128.3.58.16:6001')
s.setsockopt_string(zmq.SUBSCRIBE, '')

poller = zmq.Poller()
poller.register(s, zmq.POLLIN)


def get_loc(b):
    '''Finds the location halfway up the rising slope of the voltage pulse'''
    b.get_data()
    j = np.argmax(b.x)
    m = b.x[j]
    tmin = np.argmin(np.abs(b.t - (b.t[j] - 2)))
    j = np.argmin(np.abs(b.x[tmin:j] - m / 2)) + tmin
    return b.t[j]


class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig, self.axes = plt.subplots(1, 5, sharey=True)
        for ax in self.axes:
            ax.hold(False)

        self.compute_initial_figure()

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass


class MyStaticMplCanvas(MyMplCanvas):
    """Simple canvas with a sine plot."""

    def compute_initial_figure(self):
        t = arange(0.0, 3.0, 0.01)
        s = sin(2 * pi * t)
        for ax in self.axes:
            ax.plot(t, s)


class MyDynamicMplCanvas(MyMplCanvas):
    """A canvas that updates itself every second with a new plot."""

    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)

    def compute_initial_figure(self):
        for j, ax in enumerate(self.axes):
            plt.sca(ax)
            plt.cla()
            ax.plot([], [])
            ax.yaxis.set_ticklabels([])
            ax.xaxis.set_ticklabels([])
        self.fig.subplots_adjust(wspace=0)

    def update_figure(self):
        # Build a list of 4 random integers between 0 and 10 (both inclusive)

        socks = dict(poller.poll(0))
        if s in socks and socks[s] == zmq.POLLIN:
            sn = int(s.recv_string())
            time.sleep(3)
            shot = data.Shotdata(sn)
            BLs = shot.BL

            for j, (B, ax) in enumerate(zip(BLs, self.axes)):
                plt.sca(ax)
                plt.cla()
                B.get_data()
                actualloc = get_loc(B)
                trigger = (B.timing['upstairs'] - .00123) * 10**6
                desiredloc = trigger + B.timing['delay [us]']
                if np.abs(actualloc - desiredloc) >= .0075:
                    bgcolor = 'r'

                elif np.abs(actualloc - desiredloc) > .005 and np.abs(actualloc - desiredloc) < .0075:
                    bgcolor = 'y'
                else:
                    bgcolor = 'w'
                ax.set_axis_bgcolor(bgcolor)
                x = B.x[(B.t >= desiredloc - .3) * (B.t <= desiredloc + .3)]
                t = B.t[(B.t >= desiredloc - .3) * (B.t <= desiredloc + .3)]
                ax.plot(t, x)

                if j != 0:
                    ax.yaxis.set_ticklabels([])
                ax.relim()
                ax.autoscale_view(True, True, True)
                ax.axvline(actualloc, c='g', ls='--')
                ax.axvline(desiredloc, c='k')
            self.draw()


class ApplicationWindow(QtWidgets.QMainWindow):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("application main window")

        self.file_menu = QtWidgets.QMenu('&File', self)
        # self.file_menu.addAction('&Load
        # shot',self.loadshot,QtCore.Qt.CTRL+QtCore.QT.Key_S)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtWidgets.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtWidgets.QWidget(self)

        l = QtWidgets.QVBoxLayout(self.main_widget)

        dc = MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100)

        l.addWidget(dc)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.statusBar().showMessage("All hail matplotlib!", 2000)

    # def loadshot(self):

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtWidgets.QMessageBox.about(self, "About",
                                    """Shotmonitor GUI for NDCXII"""
                                    )


def listen():
    qApp = QtWidgets.QApplication(sys.argv)

    aw = ApplicationWindow()
    aw.setWindowTitle('NDCX-II Shot Monitor')
    iconpath = os.path.join(os.curdir, 'Icon', 'NDCXIILogo.jpeg')
    print(iconpath)
    aw.setWindowIcon(QtGui.QIcon(iconpath))
    aw.show()
    sys.exit(qApp.exec_())

listen()
