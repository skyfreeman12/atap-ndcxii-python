import NDCXII
import numpy as np
import matplotlib.pyplot as plt
import os
import configparser
from json import JSONDecoder, JSONEncoder


def checktiming(shotnumber, vb=False):
    """Checks the timing of the cells in the shot indicated by shotnumber."""

    shot = NDCXII.data.Shotdata(shotnumber)
    if vb:
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlabel('time $[\mu \mathrm{s}]$')
        ax.set_ylabel('voltage [V]')
        ax.set_xlim([0, 4])
        ax.set_ylim([0, 200000])
        colors = [plt.cm.plasma(i, alpha=0.3) for i in np.linspace(0, 0.9, 6)]
    offset = 0

    for i in range(1, len(shot.Cells)):
        cell = shot.Cells[i]
        trigger = (cell.timing['trigger'] - 0.00123) * 10**6
        # 0.0012295 trigger offset seems to get it exactly
        t0, v = cell.get_data()
        v_os = v + offset
        m = max(v)
        if vb:
            ax.plot(t0, v_os, c=colors[i - 1])
            ax.axvline(x=trigger + 0.5, ymin=offset / 200000,
                       ymax=(m + offset) / 200000,
                       color=colors[i - 1], ls='--')
            ax.axvspan(trigger + .5, trigger + 1.5, ymin=offset / 200000,
                       ymax=(m + offset) / 200000,
                       color=colors[i - 1])
            ax.axvline(x=trigger + 0.5 + 1, ymin=offset / 200000,
                       ymax=(m + offset) / 200000,
                       color=colors[i - 1], ls='--')
        offset = offset + m

        maxtime = t0[np.argmax(v)]
        if maxtime >= trigger + 1.5 or maxtime <= trigger + 0.5:
            print('trigger error on ' + cell.name)
            print('\ttrigger was {0:.3f} microseconds'.format(trigger + 0.5))
            print(
                '\tmax voltage occured at {0:.3f} microseconds'.format(maxtime))

    if vb:
        plt.show()


def add_data(data):
    """
    Takes an array of voltages and fields and adds them to the file
    containing data from previous shots
    """

    with open('./SRKFILES/FieldMapdata.txt', 'ab') as f_handle:
        np.savetxt(f_handle, data)

    with open('./SRKFILES/FieldMapData.txt', 'r') as fieldmap:
        alldata = np.loadtxt(fieldmap)
    return alldata


def get_field(V):
    """gets the expected field value for a given voltage, V, using the
    model saved in the FieldMap text file."""
    config = configparser.ConfigParser()
    config.read('./warnings_config.ini')
    model = JSONDecoder().decode(config['MODEL']['Bz'])
    return model[0] * V + model[1]


def get_voltage(Bz):
    config = configparser.ConfigParser()
    config.read('./warnings_config.ini')
    model = JSONDecoder().decode(config['MODEL']['Bz'])
    return (Bz - model[1]) / model[0]


def make_fit(voltages, fields):
    """
    Takes data in the form of a numpy array of voltage, field pairs and
    fits to a 1d polynomial. Saves the model to
    the FieldMap text file.
    """

    fit = np.polyfit(voltages, fields, 1)
    return fit


def check_shot(shotnumber, vb=False):
    """
    Takes a shot number and checks to see if the magnetic fields on the
    SRKs match the values that they were set to
    """

    shot = NDCXII.data.Shotdata(shotnumber)

    try:
        shot.SRK
    except:
        print('No SRK data')
        return

    configin = configparser.ConfigParser()
    configin.read('./warnings_config.ini')
    # print(configin.sections())

    rawvoltages = []
    rawfields = []
    names = []
    for s in shot.SRK:
        names.append(s.name)
        rawvoltages.append(s.voltagesetting)
        rawfields.append(s.Bmax())
    thisshotmodel = make_fit(rawvoltages, rawfields)

    if not configin.sections():
        used_shots = []
        fields = []
        voltages = []
        prevmodel = make_fit(rawvoltages, rawfields)
    else:
        used_shots = JSONDecoder().decode(configin['USED']['shots'])
        prevmodel = JSONDecoder().decode(configin['MODEL']['Bz'])
        fields = JSONDecoder().decode(configin['DATA']['fields'])
        voltages = JSONDecoder().decode(configin['DATA']['voltages'])

    if shot.shotnumber not in used_shots:

        used_shots.append(shot.shotnumber)
        if abs(prevmodel[1] - thisshotmodel[1]) > 0.0005 and abs(
                prevmodel[0] - thisshotmodel[0]) > 0.1:
            if vb:
                print('current model no good, using previous model')
                print(np.poly1d(thisshotmodel))
            model = np.poly1d(prevmodel)
        else:
            if vb:
                print('Generating model')
            fields = fields + rawfields
            voltages = voltages + rawvoltages
            model = np.poly1d(make_fit(voltages, fields))
    else:
        model = np.poly1d(prevmodel)

    # Make the plot and print the model if vb is true
    if vb:
        fig, ax = plt.subplots(1, 1)
        ax.plot(voltages, fields, '.', voltages, model(voltages), '-')
        ax.set_xlabel('set voltage [V]')
        ax.set_ylabel('maximum field [T]')
        plt.show()
        print(model)

    config = configparser.ConfigParser()
    config['MODEL'] = {'Bz': JSONEncoder().encode(list(model.coeffs))}
    config['DATA'] = {'voltages': JSONEncoder().encode(voltages),
                      'fields': JSONEncoder().encode(fields)}
    config['USED'] = {'shots': JSONEncoder().encode(used_shots)}
    with open('warnings_config.ini', 'w') as f:
        config.write(f)

    # Compare field values to expected values, only print if there is a
    # discrepancy

    for (voltage, field, name) in zip(rawvoltages, rawfields, names):
        valueset = model(voltage)
        if abs(field - valueset) > 0.05:
            print('discrepancy in ' + name)
            print('\t Value read  is: {0:5.2f}'.format(field))
            print('\t Value set   is: {0:5.2f}'.format(valueset))


check_shot(3550264813)
